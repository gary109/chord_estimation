import os
from data.base_dataset import BaseDataset, get_transform
from data.image_folder import make_dataset
from PIL import Image
import random

import cv2
import numpy as np


class UnalignedDataset(BaseDataset):
    """
    This dataset class can load unaligned/unpaired datasets.
    It requires two directories to host training images from domain A '/path/to/data/trainA'
    and from domain B '/path/to/data/trainB' respectively.
    You can train the model with the dataset flag '--dataroot /path/to/data'.
    Similarly, you need to prepare two directories:
    '/path/to/data/testA' and '/path/to/data/testB' during test time.
    """

    def __init__(self, opt):
        """Initialize this dataset class.
        Parameters:
            opt (Option class) -- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseDataset.__init__(self, opt)
        self.dir_A = os.path.join(opt.dataroot, opt.phase + 'A')  # create a path '/path/to/data/trainA'
        self.dir_B = os.path.join(opt.dataroot, opt.phase + 'B')  # create a path '/path/to/data/trainB'
        print('[GARY1]',self.dir_A,self.dir_B)

        self.A_paths = sorted(make_dataset(self.dir_A, opt.max_dataset_size))   # load images from '/path/to/data/trainA'
        self.B_paths = sorted(make_dataset(self.dir_B, opt.max_dataset_size))    # load images from '/path/to/data/trainB'
        self.A_size = len(self.A_paths)  # get the size of dataset A
        self.B_size = len(self.B_paths)  # get the size of dataset B
        btoA = self.opt.direction == 'BtoA'
        input_nc = self.opt.output_nc if btoA else self.opt.input_nc       # get the number of channels of input image
        output_nc = self.opt.input_nc if btoA else self.opt.output_nc      # get the number of channels of output image
        self.transform_A = get_transform(self.opt, grayscale=False,convert=False)
        self.transform_B = get_transform(self.opt, grayscale=False)

    def __getitem__(self, index):
        """Return a data point and its metadata information.
        Parameters:
            index (int)      -- a random integer for data indexing
        Returns a dictionary that contains A, B, A_paths and B_paths
            A (tensor)       -- an image in the input domain
            B (tensor)       -- its corresponding image in the target domain
            A_paths (str)    -- image paths
            B_paths (str)    -- image paths
        """
        print('[GARY2]',self.opt.serial_batches,index)

        # A_path = self.A_paths[index % self.A_size]  # make sure index is within then range
        # if self.opt.serial_batches:   # make sure index is within then range
        #     index_B = index % self.B_size
        # else:   # randomize the index for domain B to avoid fixed pairs.
        #     index_B = random.randint(0, self.B_size - 1)
        # B_path = self.B_paths[index_B]

        A_path = self.A_paths[index % self.A_size]  # make sure index is within then range
        B_path = self.B_paths[index % self.B_size]
        
        # A_img = cv2.imread(A_path)
        # A_img = cv2.cvtColor(A_img, cv2.COLOR_BGR2RGB)
        # w, h, c = A_img.shape
        # print('[GARY3]',h, w, c)

        # b, g, r = cv2.split(A_img)
        # a1 = np.ones(b.shape, dtype=b.dtype) * 25 #creating a dummy alpha channel image.
        # a2 = np.ones(b.shape, dtype=b.dtype) * 50 #creating a dummy alpha channel image.
        # a3 = np.ones(b.shape, dtype=b.dtype) * 100 #creating a dummy alpha channel image.
        # # img_BGRA = cv2.merge((b, g, r, a1,a2,a3))
        # # cv2.imwrite('AAA.png', img_BGRA)

        # """Create a blank image that has 5 channels 
        # and the same number of pixels as your original input"""
        # needed_multi_channel_img = np.zeros((b.shape[0], b.shape[1], 6))

        # """Add the channels to the needed image one by one"""
        # needed_multi_channel_img [:,:,0] = b
        # needed_multi_channel_img [:,:,1] = g
        # needed_multi_channel_img [:,:,2] = r
        # needed_multi_channel_img [:,:,3] = a1
        # needed_multi_channel_img [:,:,4] = a2
        # needed_multi_channel_img [:,:,5] = a3
        # A_img = Image.fromarray(needed_multi_channel_img)

        A_img = np.load(A_path) 
        B_img = np.load(B_path) 

        
        # apply image transformation
        A = self.transform_A(A_img)
        B = self.transform_B(B_img)

        return {'A': A, 'B': B, 'A_paths': A_path, 'B_paths': B_path}

    def __len__(self):
        """Return the total number of images in the dataset.
        As we have two datasets with potentially different number of images,
        we take a maximum of
        """
        return max(self.A_size, self.B_size)