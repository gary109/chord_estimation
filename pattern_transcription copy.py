import copy

# *_*_*  => M_L_XL

# For Test
# XL_LIST = [
#     './pattern_8/2_0_0.png',                        
# ]

# L_LIST = [
#     './pattern_8/2_0_0.png',                        
# ]

# M_LIST = [
#     './pattern_8/2_0_0.png',                                                       
# ]

# S_LIST = [
#     './pattern_8/2_0_0.png',     
# ]

# XS_LIST = [
#     './pattern_8/2_0_0.png',       
# ]

XL_LIST = [
    './pattern_1/0_*_*.png',                
    './pattern_2/0_*_*.png',            
    './pattern_3/0_0_*.png',
    './pattern_4/0_*_*.png',
    './pattern_5/*_*_*.png',                
    './pattern_6/0_*_*.png',   
    './pattern_7/*_*_*.png',
    './pattern_8/0_0_*.png',
]

L_LIST = [
    './pattern_1/*_*_0.png',                
    './pattern_2/*_*_*.png',               
    './pattern_3/0_*_*.png',
    './pattern_4/*_*_*.png',
    './pattern_5/*_*_*.png',                
    './pattern_6/*_*_0.png',   
    './pattern_7/*_*_*.png',
    './pattern_8/0_*_*.png',        
]

M_LIST = [
    './pattern_1/*_0_0.png',                
    './pattern_2/*_*_0.png',                
    './pattern_3/0_*_*.png',
    './pattern_4/*_*_0.png',
    './pattern_5/*_*_0.png',                
    './pattern_6/*_*_0.png',   
    './pattern_7/*_*_*.png',
    './pattern_8/*_0_0.png',                                                 
]

S_LIST = [
    './pattern/S0C_E0C_M.png',
    './pattern/S0C_E0C_M.png',
    './pattern/S12_E12_M.png',
    './pattern/S12_E12_M.png',
    './pattern/S33_E33_M.png',
    './pattern/S33_E33_M.png',
    './pattern/S21_E21_M.png',
    './pattern/S21_E21_M.png',
]

XS_LIST = [
    './pattern/S0C_E0C_M.png',
    './pattern/S0C_E0C_M.png',
    './pattern/S12_E12_M.png',
    './pattern/S12_E12_M.png',
    './pattern/S33_E33_M.png',
    './pattern/S33_E33_M.png',
    './pattern/S21_E21_M.png',
    './pattern/S21_E21_M.png',
]

pattern_dict = {
    'chorus_symbol_nokey':{
        'size':{
            'XL':1.5,
            'L' :.9,
            'M' :.5,
            'S' :.3,
            'XS':.0,
        },
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },
        'filter':{
            'XL': [{'mode':'bulr-erode','gsize':(49,1),'esize':(3,1)},],
            'M':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            'XS': [{'mode':'dilate','dsize':(3,1)},],
        },
    }, 'pre-chorus_symbol_nokey':{
        'size':{
            'XL':1.5,
            'L' :.9,
            'M' :.5,
            'S' :.3,
            'XS':.0},
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },
        'filter':{
            'XL': [{'mode':'lap48'},],
            'L':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            'M':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            'XS': [{'mode':'dilate','dsize':(3,1)},],
        },
    }, 'pre-chorus-and-chorus_symbol_nokey':{
        'size':{
            'XL':1.5,
            'L' :.9,
            'M' :.5,
            'S' :.3,
            'XS':.0},
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },
        'filter':{
            'XS': [{'mode':'dilate','dsize':(3,1)},],
            'M':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
        },
    # }, 'verse_symbol_nokey':{
    #     'size':{
    #         'XL':1.5,
    #         'L' :.9,
    #         'M' :.5,
    #         'S' :.3,
    #         'XS':.0},
    #     'pattern':{
    #         'XL': copy.deepcopy(XL_LIST),
    #         'L' : copy.deepcopy(L_LIST),
    #         'M' : copy.deepcopy(M_LIST),
    #         'S' : copy.deepcopy(S_LIST),
    #         'XS': copy.deepcopy(XS_LIST),
    #     },
    #     'rotate':{
    #         'XL':'YES',
    #         'L':'YES',
    #         'M':'YES',
    #         'S':'YES',
    #         'XS':'YES',
    #     },
    #     'filter':{
    #         'XL':  [{'mode':'bulr-erode','gsize':(49,1),'esize':(3,1)},],
    #         'M':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
    #         'XS': [{'mode':'dilate','dsize':(3,1)},],
    #     }
    }, 'verse_symbol_nokey':{
        'size':{
            'XL':1.65,
            'L' :1.025,
            'M' :0.75,
            'S' :0.5,
            'XS':.0},
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },
        'filter':{
            # 'XL':  [{'mode':'bulr-erode','gsize':(49,1),'esize':(3,1)},],
            # 'M':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            # 'XS': [{'mode':'dilate','dsize':(3,1)},],
        }
    }, 'intro_symbol_nokey':{
        'size':{
            'XL':1.5,
            'L' :.9,
            'M' :.5,
            'S' :.3,
            'XS':.0},
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },
        'filter':{
            'XL': [{'mode':'bulr-erode','gsize':(49,1),'esize':(3,1)},],
            'M':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            'XS': [{'mode':'dilate','dsize':(3,1)},],
        },
    }, 'intro-and-verse_symbol_nokey':{
        'size':{
            'XL':1.5,
            'L' :.9,
            'M' :.5,
            'S' :.3,
            'XS':.0},
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
       'filter':{
            'XL': [{'mode':'lap48'}],
            'M':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            'XS': [{'mode':'dilate','dsize':(3,1)},],
        },
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },
    }, 'bridge_symbol_nokey':{
        'size':{
            'XL':1.5,
            'L' :.9,
            'M' :.5,
            'S' :.3,
            'XS':.0},
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },
        'filter':{
            'XL': [{'mode':'lap48',},],
            'L': [{'mode':'lap48',},],
            'M': [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            'XS': [{'mode':'dilate','dsize':(3,1)},],
            'XS': [{'mode':'bulr-erode','gsize':(9,1),'esize':(3,1)},],
            'S':  [{'mode':'bulr-erode','gsize':(13,1),'esize':(3,1)},],
        }
    },'instrumental_symbol_nokey':{
        'size':{
            'XL':1.5,
            'L' :.9,
            'M' :.5,
            'S' :.3,
            'XS':.0},
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
        'filter':{
            'XL': [{'mode':'lap48'},],
            'L': [{'mode':'lap48',},],
            'M': [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            'XS': [{'mode':'bulr-erode','gsize':(9,1),'esize':(3,1)},],
            'S':  [{'mode':'bulr-erode','gsize':(13,1),'esize':(3,1)},],

        },
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },
    }, 'verse-and-pre-chorus_symbol_nokey':{
        'size':{
            'XL':1.5,
            'L' :.9,
            'M' :.5,
            'S' :.3,
            'XS':.0},
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
        'filter':{
            # 'L':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            # 'M':  [{'mode':'bulr-erode','gsize':(13,1),'esize':(3,1)},],

            'M':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            'XS': [{'mode':'dilate','dsize':(3,1)},],
        },                
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },    
    },'outro_symbol_nokey':{
        'size':{
            'XL':1.5,
            'L' :.9,
            'M' :.5,
            'S' :.3,
            'XS':.0},
        'pattern':{
            'XL': copy.deepcopy(XL_LIST),
            'L' : copy.deepcopy(L_LIST),
            'M' : copy.deepcopy(M_LIST),
            'S' : copy.deepcopy(S_LIST),
            'XS': copy.deepcopy(XS_LIST),
        },
        'rotate':{
            'XL':'YES',
            'L':'YES',
            'M':'YES',
            'S':'YES',
            'XS':'YES',
        },
        'filter':{
            'XL': [{'mode':'bulr-erode','gsize':(49,1),'esize':(3,1)},],
            'M':  [{'mode':'bulr-erode','gsize':(21,1),'esize':(3,1)},],
            'XS': [{'mode':'dilate','dsize':(3,1)},],
        },
    },                                                                                                     
}