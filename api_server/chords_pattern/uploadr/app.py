from flask import Flask, request, redirect, url_for, render_template, Response, flash,session
from flask import make_response
from flask import jsonify
import os
import json
import glob
import time
from uuid import uuid4
import copy
import random
import threading
from model.api import API
from model.box import Box
from os import path
import cv2
import pandas as pd
import PIL
import numpy as np

# from flask_socketio import SocketIO, emit
# from time import sleep
import base64
# from pubsub import pub


api = API()
class ExportingThread(threading.Thread):
    def __init__(self):
        super().__init__()
        self.infodict = {}
        self.infodict['progress'] = 0
       
    def run(self):
        #############################
        # FOR predict chords/melody
        #############################
        # api.song2img4one(self.infodict)
        # api.predict(self.infodict)
        # api.result2note(self.infodict)
        # api.note2light(self.infodict)
        # api.sendProjectToCloud(self.infodict)
        # root = "uploadr/static/uploads/{}".format(self.infodict['uuid'])
        # if os.path.isdir(root):
        #     with open('%s/infodict.json'%(root), 'w') as outfile:
        #         json.dump(self.infodict, outfile)
        
        ###########
        # FOR BOX
        ###########
        
        #---------------------------------------------------------------------------
        npz_filePath=genNPZ(filePath=self.infodict['png_filePath'],ch_n=self.infodict['ch_n'],ch_h=self.infodict['ch_h'])
        #---------------------------------------------------------------------------                                                 
        self.infodict['box'].sendProjectToCloud(ProjectName=self.infodict['projectName'], 
                               songPath=self.infodict['mp3_filePath'], 
                               showPath=npz_filePath)
        
        print('[%s] '%(self.infodict['sessionKey']), self.infodict)
        pass

exporting_threads = {}

# app = Flask(__name__, template_folder='./')
app = Flask(__name__)
app.threaded=False
app.processes=10
app.secret_key = '%d'%(random.randint(0, 10000))
# app.config['SECRET_KEY'] = 'secret!'     

# turn the flask app into a socketio app
# socketio = SocketIO(app, cors_allowed_origins="*",SameSite=None,)

################################################################################################
@app.route("/")
@app.route('/<name>')
def index(name=None):
    session.clear()
    print('[index][ExportingThread]',exporting_threads)
    # return render_template("index2.html")
    # return render_template("index_original.html")

    if name == 'chords_pattern':
        songs = []
        gtw = []
        chords = []
        onsets = []
        offsets = []
        for _ in glob.glob("uploadr/static/HW1/*/*.mp3"):            
            songs.append(os.path.basename(_))       
            
        for _ in songs:
            song_id = os.path.basename(_).split('.')[0]            
            gt_img = cv2.imread("uploadr/static/HW1/%s/%s_ground_truth.png"%(song_id,song_id))
            height, width, channels = gt_img.shape
            gtw.append([width,height])
            
            # chords
            df_chord = pd.read_csv("uploadr/static/HW1/%s/%s_chord.csv"%(song_id,song_id))
            chords.append(list(df_chord['chord']))
            onsets.append(list(df_chord['onset']))
            offsets.append(list(df_chord['offset']))

        return render_template("index.html", songs=songs, chords=chords,onsets=onsets,offsets=offsets)


#####################################################################################################
@app.route("/getResult", methods=["POST"])
def getResult():
    form = request.form
    res_dict = {}
    try:
        root = "uploadr/static/uploads/{}/temp.json".format(form['sessionKey'])
        if path.exists(root):
            res_dict['msg'] = "Success"
            res_dict['code'] = 0  
            temp_file = open (root)
            res_dict['result'] = json.load(temp_file)   
        else:
            res_dict['msg'] = "sessionKey not found"
            res_dict['code'] = -9008     
    except:
        res_dict['msg'] = "sessionKey format error"
        res_dict['code'] = -9009  
    return ajax_response(res_dict)

@app.route("/upload", methods=["POST"])
def upload():
    """Handle the upload of a file."""
    global exporting_threads

    form = request.form
    if form['account'] == '':
        res_dict = {}
        res_dict['code'] = -9000
        res_dict['msg'] = "Account Format error!!!"
        res_dict['status'] = "error"
        return ajax_response(res_dict)

    if form['pwd'] == '':
        res_dict = {}
        res_dict['msg'] = "Password Format error!!!"
        res_dict['code'] = -9001
        res_dict['status'] = "error"
        return ajax_response(res_dict)

    # Create a unique "session ID" for this particular batch of uploads.
    upload_key = str(uuid4())

    # Is the upload using Ajax, or a direct POST by the form?
    is_ajax = False
    if form.get("__ajax", None) == "true":
        is_ajax = True

    # Target folder for these uploads.
    target = "uploadr/static/uploads/{}".format(upload_key)
    try:
        os.makedirs(target, exist_ok=True) 
    except:
        if is_ajax:
            res_dict = {}
            res_dict['msg'] = "Couldn't create upload directory: {}".format(target)
            res_dict['code'] = -9002
            res_dict['status'] = "error"
            return ajax_response(res_dict)
        else:
            return "Couldn't create upload directory: {}".format(target)

    print("=== Form Data ===")
    session[upload_key] = {} 
    for key, value in list(form.items()):
        print(key, "=>", value)
        session[upload_key][key] = value

    session[upload_key]['message'] = ''
    session[upload_key]['uuid'] = upload_key
    session[upload_key]['target'] = target
    session[upload_key]['files'] = []
    if request.files.getlist("file") != []:
        for upload in request.files.getlist("file"):
            filename = upload.filename.rsplit("/")[0]
            destination = "/".join([target, filename])
            if filename == '':
                res_dict = {}
                res_dict['msg'] = 'No Files'
                res_dict['code'] = -9006
                res_dict['status'] = "error"
                session.pop('uuid', None)
                return ajax_response(res_dict)
            print("Accept incoming file:", filename)
            print("Save it to:", destination)
            upload.save(destination)
            session[upload_key]['files'].append(destination)
    # elif session[upload_key]['youtube'] != "":
    #     # session[upload_key]['files'].append(destination)
    #     exporting_threads[upload_key] = ExportingThread()
    #     exporting_threads[upload_key].infodict = copy.deepcopy(session[upload_key])
    #     exporting_threads[upload_key].start()
    #     session.pop('uuid', None)
    #     res_dict = {}
    #     res_dict['msg'] = "Success"
    #     res_dict['sessionKey'] = upload_key
    #     res_dict['code'] = 0
    #     res_dict['status'] = "ok"
    #     return ajax_response(res_dict)
    else:
        res_dict = {}
        res_dict['msg'] = 'No Files or Youtube Link'
        res_dict['code'] = -9006
        res_dict['status'] = "error"
        session.pop('uuid', None)
        return ajax_response(res_dict)


    print('is_ajax:',is_ajax)

    if is_ajax:
        # return ajax_response(status=True, msg=upload_key, code=0)
        res_dict = {}
        res_dict['msg'] = upload_key
        res_dict['code'] = 0
        res_dict['status'] = "ok"
        return ajax_response(res_dict)
    else:   
        return redirect(url_for("upload_complete", uuid=upload_key))

        # exporting_threads[upload_key] = ExportingThread()
        # exporting_threads[upload_key].infodict = copy.deepcopy(session[upload_key])
        # exporting_threads[upload_key].start()
        # session.pop('uuid', None)
        

        # res_dict = {}
        # res_dict['msg'] = "Success"
        # res_dict['sessionKey'] = upload_key
        # res_dict['code'] = 0
        # res_dict['status'] = "ok"
        # return ajax_response(res_dict)
       
@app.route('/progress1/<uuid>')
def progress1(uuid=None):
    global exporting_threads
    print('[progress1]',uuid)
    def generate():
        num = exporting_threads[uuid].infodict['progress']
        while exporting_threads[uuid].infodict['progress'] <= 100:
            if num < exporting_threads[uuid].infodict['progress']:
                num = exporting_threads[uuid].infodict['progress']
            
            yield "data:"+'$'+str(num)+'$'+exporting_threads[uuid].infodict['message']+'$'+"\n\n"
            time.sleep(0.5)
       
            if num-1 < (num//20+1)*20:
                num += 1
    return Response(generate(), mimetype='text/event-stream')

@app.route("/files/<uuid>")
def upload_complete(uuid):
    """The location we send them to at the end of the upload."""
    # Get their files.
    root = "uploadr/static/uploads/{}".format(uuid)
    if not os.path.isdir(root):
        return redirect(url_for("index"))

    for _ in glob.glob("{}/infodict.json".format(root)):
        return redirect(url_for("index"))

    for _ in glob.glob("{}/*.npz".format(root)):
        return redirect(url_for("index"))

    files = []
    for file in glob.glob("{}/*.*".format(root)):
        fname = file.split(os.sep)[-1]
        files.append(fname)
    
    if files:
        global exporting_threads
        exporting_threads[uuid] = ExportingThread()
        exporting_threads[uuid].infodict = copy.deepcopy(session[uuid])
        exporting_threads[uuid].start()
        session.pop('uuid', None)
        print(exporting_threads[uuid].infodict['account'],exporting_threads[uuid].infodict['pwd'])
        return render_template("index2.html", uuid=uuid, songs=files, 
            account=exporting_threads[uuid].infodict['account'],
            pwd=exporting_threads[uuid].infodict['pwd'])


    return redirect(url_for("index"))
    # return render_template("index2.html")
    # res_dict = {}
    # res_dict['msg'] = 'No Files'
    # res_dict['code'] = -9006
    # res_dict['status'] = "error"
    # account = session[uuid]['account']
    # pwd = session[uuid]['pwd']
    # session.pop('uuid', None)
    # return render_template("index2.html", account=account, pwd=pwd)    
    # # session.pop('uuid', None)
    # # return ajax_response(res_dict)

def ajax_response(res):
    return json.dumps(dict(res))
    # return json.dumps(res)

# Function to convert a CSV to JSON 
# Takes the file paths as arguments 
def make_json(csvFilePath, jsonFilePath): 
      
    # create a dictionary 
    data = {} 
      
    # Open a csv reader called DictReader 
    with open(csvFilePath, encoding='utf-8') as csvf: 
        csvReader = csv.DictReader(csvf) 
          
        # Convert each row into a dictionary  
        # and add it to data 
        for rows in csvReader: 
              
            # Assuming a column named 'No' to 
            # be the primary key 
            key = rows['No'] 
            data[key] = rows 
  
    # Open a json writer, and use the json.dumps()  
    # function to dump data 
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf: 
        jsonf.write(json.dumps(data, indent=4)) 
################################################################################################################
@app.route('/login', methods=['GET', 'POST'])
def login():
   message = None
   if request.method == 'POST':
        # datafromjs = request.form['mydata']
        print("[RESP]\n",request.form)
        result = "return this"
        resp = make_response('{"response": '+result+'}')
        resp.headers['Content-Type'] = "application/json"
        return resp
        # return render_template('login.html', message='')        

################################################################################################################
@app.route('/upload_lightshow', methods=['GET', 'POST'])
def upload_lightshow():
   res_dict={}
   res_dict['code']=0
   res_dict['msg']=''
   if request.method == 'POST':
        b64_string = request.form['image']
        convert_and_save(b64_string=b64_string,
                         songname=os.path.basename(request.form['songname']).split('.')[0],
                         filename=request.form['filename'])
        res_dict['msg'] = "SUCCESS"
        res_dict['code']=1
        return ajax_response(res_dict)

################################################################################################################
@app.route('/upload_lightshow2taptap', methods=['POST'])
def upload_lightshow2taptap():
    global exporting_threads
    res_dict={}
    res_dict['code']=0
    res_dict['msg']=''
    if request.method == 'POST':
        songname=os.path.basename(request.form['songname']).split('.')[0]
        projectName=request.form['projname']
        b64_string = request.form['image']
        password=request.form['password']
        accountname=request.form['accountname']
        sessionKey=str(uuid4())
        #---------------------------------------------------------------------------
        # Target folder for these uploads.
        target = "uploadr/static/uploads/{}".format(sessionKey)
        try:
            os.makedirs(target, exist_ok=True) 
        except:
            res_dict['msg'] = "Couldn't create upload directory"
            res_dict['code']= 0
            return ajax_response(res_dict)
        #---------------------------------------------------------------------------
        png_filePath='%s/%s.png'%(target,songname)
        mp3_filePath="./uploadr/static/HW1/%s/%s.mp3"%(songname,songname)
        with open(png_filePath, "wb") as fh:
            fh.write(base64.decodebytes(b64_string.encode()))
        #---------------------------------------------------------------------------        
        session[sessionKey] = {} 
        session[sessionKey]['sessionKey'] = sessionKey
        session[sessionKey]['songname'] = songname
        session[sessionKey]['projectName'] = projectName
        session[sessionKey]['password'] = password
        session[sessionKey]['accountname'] = accountname
        session[sessionKey]['target'] = target
        session[sessionKey]['png_filePath'] = png_filePath
        session[sessionKey]['mp3_filePath'] = mp3_filePath
        session[sessionKey]['ch_n'] = int(request.form['ch_n'])
        session[sessionKey]['ch_h'] = int(request.form['ch_h'])   

        box = Box(account=accountname, 
                  pwd=password, 
                  dataroot=os.path.dirname(png_filePath), 
                  projectName=projectName)

        #---------------------------------------------------------------------------
        exporting_threads[sessionKey] = ExportingThread()
        exporting_threads[sessionKey].infodict = copy.deepcopy(session[sessionKey])
        exporting_threads[sessionKey].infodict['box'] = box
        exporting_threads[sessionKey].start()
        session.pop(sessionKey, None)
        print('[GARY]',exporting_threads)

        res_dict['msg'] = "SUCCESS"
        res_dict['code']= 1
        res_dict['sessionKey']= sessionKey
        return ajax_response(res_dict)
 
################################################################################################################
def genNPZ(filePath,ch_n,ch_h):  
    dirname = os.path.dirname(filePath)
    basename = os.path.basename(filePath)
    filename = basename.split('.')[0]   
    npz_filePath = '%s/%s.npz'%(dirname,filename)

    an_image = PIL.Image.open(filePath).convert('L')
    a = np.asarray(an_image)

    channels = dict()
    for ch in range(ch_n):
        channels['ch%d'%(ch+1)] = [int(round(x/25.5)) for x in a[int(ch*ch_h)+ int(ch_h/2)]]     
    np.savez(npz_filePath , **channels) 
    return npz_filePath

################################################################################################################
def convert_and_save(b64_string,songname,filename):    
    timestr = time.strftime("%Y%m%d%H%M%S")
    filename_dir = "./uploadr/static/uploads/%s_%s_%s.png"%(filename,songname,timestr)

    with open(filename_dir, "wb") as fh:
        fh.write(base64.decodebytes(b64_string.encode()))
    return filename_dir
################################################################################################
@app.route("/getStatus", methods=["POST"])
def getStatus():
    form = request.form
    sessionKey = form['sessionKey']
    res_dict = {}
    global exporting_threads
    try:
        root = "uploadr/static/uploads/{}".format(sessionKey)
        if os.path.isdir(root):            
            res_dict['msg'] = exporting_threads[sessionKey].infodict['box'].msg
            res_dict['progress'] = exporting_threads[sessionKey].infodict['box'].progress
            res_dict['code'] = exporting_threads[sessionKey].infodict['box'].code           

            if res_dict['progress'] == 100:
                exporting_threads.pop(sessionKey, None)
                print('[GARY1]',exporting_threads)
        else:
            res_dict['msg'] = "sessionKey not found"
            res_dict['code'] = -9003   
    except:
        res_dict['msg'] = "sessionKey format error"
        res_dict['code'] = -9007   
    return ajax_response(res_dict)
################################################################################################################
@app.route('/loadSongInfo', methods=['GET', 'POST'])
def loadSongInfo():
    songs = []
    gtw = []
    chords = []
    onsets = []
    offsets = []
    patterns = []
    for _ in glob.glob("uploadr/static/HW1/*/*.mp3"):            
        songs.append(os.path.basename(_))     
    songs.sort()

    a, b = songs.index('109.mp3'), songs.index('101.mp3')
    songs[b], songs[a] = songs[a], songs[b]
    
    for _ in glob.glob("uploadr/static/patterns/*.png"):  
        patterns.append(os.path.basename(_))
    patterns.sort()
            
    for _ in songs:
        song_id = os.path.basename(_).split('.')[0]            
        gt_img = cv2.imread("uploadr/static/HW1/%s/%s_ground_truth.png"%(song_id,song_id))
        height, width, channels = gt_img.shape
        gtw.append([width,height])
        
        # chords
        df_chord = pd.read_csv("uploadr/static/HW1/%s/%s_chord.csv"%(song_id,song_id))
        chords.append(list(df_chord['chord']))
        onsets.append(list(df_chord['onset']))
        offsets.append(list(df_chord['offset']))

    d = {
        'gtw':gtw,
        'songs':songs,
        'chords':chords,
        'onsets':onsets,
        'offsets':offsets,
        'patterns':patterns,
    }

    
    resp = make_response(jsonify(d))
    resp.headers['Content-Type'] = "application/json"
    return resp
    