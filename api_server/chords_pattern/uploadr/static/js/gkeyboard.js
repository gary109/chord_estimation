const ORDER_0 = 0;
const ORDER_1 = 1;
const ORDER_2 = 2;
let KeyCodeInfo = {
    'code':0x0,
    ///////////////
    // Channel 1 //
    ///////////////
    49:{ 
        'name':'ch1',
        'code': 0x01,
        'rcode':0xFE,
    },
    ///////////////
    // Channel 2 //
    ///////////////
    50:{
        'name':'ch2',
        'code': 0x02,
        'rcode':0xFD,
    },
    ///////////////
    // Channel 3 //
    ///////////////
    51:{
        'name':'ch3',
        'code': 0x04,
        'rcode':0xFB,
    },
    ///////////////
    // Channel 4 //
    ///////////////
    56:{
        'name':'ch4',
        'code': 0x08,
        'rcode':0xF7,
    },
    ///////////////
    // Channel 5 //
    ///////////////
    57:{
        'name':'ch5',
        'code': 0x10,
        'rcode':0xEF,
    },
    ///////////////
    // Channel 6 //
    ///////////////
    48:{
        'name':'ch6',
        'code': 0x20,
        'rcode':0xDF,
    },
    //////////
    // Ctrl //
    //////////
    17:{
        'name':'ctrl',
        'code': 0x40,
        'rcode':0xBF,
    },
    ///////
    // c //
    ///////
    67:{
        'name':'c',
        'code': 0x80,
        'rcode':0x7F,
    },
    ///////
    // v //
    ///////
    86:{
        'name':'v',
        'code': 0x100,
        'rcode':0xEFF,
    },
    ///////
    // x //
    ///////
    88:{
        'name':'x',
        'code': 0x200,
        'rcode':0xDFF,
    },   
    ///////
    // z //
    ///////
    90:{
        'name':'z',
        'code': 0x400,
        'rcode':0xBFF,
    },  
    /////////
    // del //
    /////////
    46:{
        'name':'del',
        'code': 0x800,
        'rcode':0x7FF,
    }, 
    ///////////
    // shift //
    ///////////
    16:{
        'name':'shift',
        'code': 0x1000,
        'rcode':0xEFFF,
        'order':ORDER_0,
        'x1':0,
        'y1':0,
        'penWidth':0,
        'fixed':'N',
    }, 
    ///////
    // y //
    ///////
    89:{
        'name':'y',
        'code': 0x2000,
        'rcode':0xDFFF,
    },  
    /////////
    // ESC //
    /////////
    27:{
        'name':'ESC',
        'code': 0x4000,
        'rcode':0xBFFF,
    },  
    ///////////
    // Enter //
    ///////////
    13:{
        'name':'Enter',
        'code': 0x8000,
        'rcode':0x7FFF,
    },  
};

function keyAction(){
    // Copy (ctrl+c)
    if(192 == KeyCodeInfo['code']) copy();
    // Paste (ctrl+v)
    else if(320 == KeyCodeInfo['code']) paste();
    // Cut (ctrl+x)
    else if(576 == KeyCodeInfo['code']) cut();
    // Undo (ctrl+z)
    else if(1088 == KeyCodeInfo['code']) undo();
    // Redo (ctrl+y)
    else if(8256 == KeyCodeInfo['code']) redo();
    // Del
    else if(2048 == KeyCodeInfo['code']) del();
    // ESC
    else if(16384 == KeyCodeInfo['code']) esc();
    // Enter
    else if(32768 == KeyCodeInfo['code']) enter();
    // Shift
    else if(4096 == KeyCodeInfo['code']){      
        if(ORDER_0 == KeyCodeInfo[16]['order']) {
            KeyCodeInfo[16]['order'] = ORDER_1;
            KeyCodeInfo[16]['x1'] = x1;
            KeyCodeInfo[16]['y1'] = y1;
            KeyCodeInfo[16]['penWidth'] = getPenWidth();                          
        }
    }
}

document.addEventListener("keydown", function (e) {   
    var key= (e || window.event).keyCode;
    if (!(key in KeyCodeInfo)) 
        return true;        
    KeyCodeInfo['code'] |= KeyCodeInfo[key]['code'];   
    keyboardInfoText = '[Keyboard]\n'+
        'keyCode : ' + key + "\n" +  
        'name    : ' + KeyCodeInfo[key]['name']+'\n'+
        'code    : ' + KeyCodeInfo['code'] + "\n";
    keyAction();   
    return false;
} , false);

document.addEventListener("keyup", function (e) {        
    var key= (e || window.event).keyCode;
    if (!(key in KeyCodeInfo)) 
        return true;
    KeyCodeInfo['code'] &= (KeyCodeInfo[key]['rcode']); 
    keyboardInfoText = '[Keyboard]\n'+
        'keyCode : ' + key + "\n" +  
        'name    : ' + KeyCodeInfo[key]['name']+'\n'+
        'code    : ' + KeyCodeInfo['code'] + "\n";
    if(KeyCodeInfo[key]['name'] == 'shift') {
        KeyCodeInfo[16]['order'] = ORDER_0;
        KeyCodeInfo[16]['fixed'] = 'N';
        KeyCodeInfo[16]['x1'] = 0;
        KeyCodeInfo[16]['y1'] = 0;
        KeyCodeInfo[16]['penWidth'] = getPenWidth();
    }
    return false;
}, false);


// When window is unfocused we may not get key events. To prevent this
// causing a key to 'get stuck down', cancel all held keys
window.onblur= function() {
    KeyCodeInfo['code'] = 0; 
    keyboardInfoText = '[Keyboard] onblur\n';
};