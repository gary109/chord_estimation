function play() {
    if (bPlay) {
        bPlay = false;
        light_ctx.drawImage(cursorRightCanvas, 0,0);
        startAnimation('UpdateRCursor');
        document.getElementById("track").pause();
        document.getElementById("play_img").src = '/static/img/play.png?t='+Math.random();        
    } else {
        document.getElementById("track").play();
        document.getElementById("play_img").src = '/static/img/pause.png?t='+Math.random();
        bPlay = true;
        drawCursorRight(false);
        cursorRight_ctx.drawImage(lightCanvas, 0, 0);
        startAnimation('UpdateCanvas');
    }
}

function player_stop() {
    bPlay = false;
    light_ctx.drawImage(cursorRightCanvas, 0,0);
    startAnimation('UpdateRCursor');
    document.getElementById("track").stop();
    document.getElementById("play_img").src = '/static/img/play.png?t='+Math.random();
}

function pause() {
    bPlay = false;
    light_ctx.drawImage(cursorRightCanvas, 0,0);
    startAnimation('UpdateRCursor');
    document.getElementById("track").pause();
    document.getElementById("play_img").src = '/static/img/play.png?t='+Math.random();
}

function prev(t) {
    var vid = document.getElementById("track");
    if (vid.currentTime < 5)
        vid.currentTime = 0;
    else
        vid.currentTime -= 5;
}

function after(t) {
    document.getElementById("track").currentTime += t;
}

function go_start() {
    document.getElementById("track").currentTime = 0;
}

function go_end() {
    document.getElementById("track").currentTime = 999;
}