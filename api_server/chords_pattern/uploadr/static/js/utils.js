function drag_start(event) {
    var style = window.getComputedStyle(event.target, null);
    var str = (parseInt(style.getPropertyValue("left")) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top")) - event.clientY) + ',' + event.target.id;
    event.dataTransfer.setData("Text", str);
    var offset = event.dataTransfer.getData("Text").split(',');
    var dm = document.getElementById(offset[2]);
    dm.style.zIndex = toolbox_name_list.length;  
}
function drag_stop(event) {
    event.preventDefault();
    event.stopPropagation();
}
function drop(event) {
    var offset = event.dataTransfer.getData("Text").split(',');
    var dm = document.getElementById(offset[2]);
    dm.style.left = (event.clientX + parseInt(offset[0], 10)) + 'px';
    dm.style.top = (event.clientY + parseInt(offset[1], 10)) + 'px';            
    event.preventDefault();
    return false;
}
function drag_over(event) {
    event.preventDefault();
    return false;
}
function updateInfoText(){
    document.getElementById("info_textarea").innerHTML = cursorInfoText+'\n'+audioInfoText+'\n'+ keyboardInfoText+'\n';  
}
function runPyScript(method, input){
    if("test" == method){
        var jqXHR = $.ajax({
            type: "POST",
            url: "/login",
            async: false,
            data: {
                mydata: input,
                song:selected_song,
            }
        });
        return jqXHR.responseText;
    } else if("upload_lightshow" == method) {
        var jqXHR = $.ajax({
            type: "POST",
            url: "/upload_lightshow",
            async: false,
            data: {
                image: input,
                songname:selected_song,
                filename:document.getElementById('lightshow_filename').value,
            }
        });
        return jqXHR.responseText;
    } else if("getStatus" == method) {
        var jqXHR = $.ajax({
            type: "POST",
            url: "/getStatus",
            async: false,
            data: {
                sessionKey:input
            }
        });
        return jqXHR.responseText;
    } else if("upload_lightshow2taptap" == method) {
        var jqXHR = $.ajax({
            type: "POST",
            url: "/upload_lightshow2taptap",
            async: false,
            data: {
                ch_n:Channels_N,
                ch_h:Channels_H,
                accountname:document.getElementById('accountname').value,
                password:document.getElementById('password').value,
                projname:document.getElementById('projname').value,
                image: input,
                songname:selected_song,
            }
        });
        return jqXHR.responseText;
    } else if("loadSongInfo" == method){
        axios.post('/loadSongInfo', {})
          .then(function (response) {
            gtwVar = response['data']['gtw'];
            songsVar = response['data']['songs'];
            chordsVar = response['data']['chords'];
            onsetsVar = response['data']['onsets'];
            offsetsVar = response['data']['offsets'];
            patternsVar = response['data']['patterns'];
            initAllPanel();
          })
          .catch(function (error) {
            console.log(error);
          });
    }
    
}

