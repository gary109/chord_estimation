let toolbox_name_list = [
    {
        'id':'box_file',
        'name':'File',
        'content':'box_file_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':220,
        'h+':350,
        'class':'File',
    },{
        'id':'box_hotkey',
        'name':'Hotkey',
        'content':'box_hotkey_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':150,
        'h+':620,
        'class':'Edit',
    },{
        'id':'box_pen',
        'name':'Pen',
        'content':'box_pen_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':280,
        'h+':350,
        'class':'Edit',
    },{
        'id':'box_color',
        'name':'Color',
        'content':'box_color_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':380,
        'h+':220,
        'class':'Edit',
    },{
        'id':'box_filter',
        'name':'Effect',
        'content':'box_filter_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':200,
        'h+':550,       
        'class':'Edit', 
    },{
        'id':'box_clipboard',
        'name':'Clipboard',
        'content':'box_clipboard_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':200,
        'h+':480,
        'class':'Edit',
    },{
        'id':'box_patterns',
        'name':'Pattern',
        'content':'box_pattern_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':380,
        'h+':480,
        'class':'Edit',
    },{
        'id':'box_channel_view',
        'name':'Channel View',
        'content':'box_channel_view_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':250,
        'h+':200,
        'class':'View',
    },{
        'id':'box_photo_view',
        'name':'Photo View',
        'content':'box_photo_view_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':500,
        'h+':600,
        'class':'View',
    },{
        'id':'box_audio',
        'name':'Audio',
        'content':'box_audio_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':380,
        'h+':130,
        'class':'Control',
    },{
        'id':'box_taptap',
        'name':'TapTap',
        'content':'box_taptap_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':280,
        'h+':200,
        'class':'Control',
    },{
        'id':'box_info',
        'name':'Information',
        'content':'box_info_div',
        'l-':100,
        'w-':150,
        'h-':30,
        'w+':200,   
        'h+':510,
        'class':'Info',
    }
];



function initToolboxPanel() {
    document.getElementById("toolbox_panel").style.width = document.documentElement.clientWidth + "px"
    document.getElementById("toolbox_panel").style.height = document.documentElement.clientHeight + "px"
}

function initToolBox() {
    initToolboxPanel()
    var track_rect = document.getElementById("track").getBoundingClientRect();
    var start_x = 10;
    var start_y = track_rect.top + track_rect.height;
    var className = toolbox_name_list[0].class;
    for (var i = 0; i < toolbox_name_list.length; i++) {
        if (className != toolbox_name_list[i].class) {
            start_y += 50;
            start_x = 10;
            className = toolbox_name_list[i].class;
        }

        const element = document.querySelector('.' + toolbox_name_list[i].id);
        const style = getComputedStyle(element);
        document.getElementById(toolbox_name_list[i].id).style.left = start_x + "px"
        document.getElementById(toolbox_name_list[i].id).style.top = start_y + "px"
        document.getElementById(toolbox_name_list[i].id).style.zIndex = i; 
        start_x += 120;
        

        element.addEventListener(downEvent, function (e) {
            for (var ii = 0; ii < toolbox_name_list.length; ii++) 
                document.getElementById(toolbox_name_list[ii].id).style.zIndex = ii;       
            document.getElementById(this.id).style.zIndex = toolbox_name_list.length;
        });
    }
    initChordPanel()
    document.getElementById('box_navigation').style.left = clientWidth - 250 + "px";
    document.getElementById('box_navigation').style.top = clientHeight - 200 + "px" 
}

function labelBtn(t){
    for (var i = 0; i < toolbox_name_list.length; i++) {
        if(toolbox_name_list[i].name == t.innerHTML) {
            var _ = document.getElementById(toolbox_name_list[i].content);
            if(_.style.visibility == 'hidden'){
                document.getElementById(toolbox_name_list[i].id).style.height = toolbox_name_list[i]['h+']+'px';
                document.getElementById(toolbox_name_list[i].id).style.width = toolbox_name_list[i]['w+']+'px';
                _.style.visibility = "visible";
                if(toolbox_name_list[i].id == 'box_photo_view') all_photoview_canvas_visible(true);
            }
            else {
                document.getElementById(toolbox_name_list[i].id).style.height = toolbox_name_list[i]['h-']+'px';
                document.getElementById(toolbox_name_list[i].id).style.width = toolbox_name_list[i]['w-']+'px';
                _.style.visibility = "hidden";
                if(toolbox_name_list[i].id == 'box_photo_view') all_photoview_canvas_visible(false);
            }
            break;
        }
    }
  
}

