
const loadImageToCanvas = (url, cavansId, callback) => {
    let canvas = document.getElementById(cavansId);
    let ctx = canvas.getContext("2d");
    let img = new Image();
    img.crossOrigin = "anonymous";
    img.onload = () => {        
      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage(img, 0, 0, img.width, img.height);
      callback && callback();
    };
    img.src = url;
  };

function initPatternsView(){
    for (p of patternsVar) {
        var basename = p.split('.')[0];
        // document.getElementById('patterns_div').innerHTML += `<img src="/static/patterns/${p}" 
        //     alt="image name" style="cursor: pointer;float: left; width: 50px; height: 50px; margin: 2px 2px;" /></img>`;
        document.getElementById('patterns_div').innerHTML += `<img src="/static/patterns/${p}" 
           id="pattern_${basename}" alt="${basename}" class="pattern_btn normal-zoom" /></img>`;
        // __i++;
    }

    $('.pattern_active').click(function() {
        $(this).toggleClass('pattern_active');
    });

    // pattern_btn in/out clothing img
    $('.pattern_btn').click(function() {        
        $(this).toggleClass('normal-zoom zoom-in');                  
        // $('.pattern_btn').not(this).removeClass('pattern_active');
        removePatternActive();
        $(this).toggleClass('pattern_active')
        copy_pattern($(this)[0].src)
    });



    $('.pattern_btn').on('mousemove', function(event) {
        // // This gives you the position of the image on the page
        // var bbox = event.target.getBoundingClientRect();
      
        // // Then we measure how far into the image the mouse is in both x and y directions
        // var mouseX = event.clientX - bbox.left;
        // var mouseY = event.clientY - bbox.top;
      
        // // Then work out how far through the image as a percentage the mouse is
        // var xPercent = (mouseX / bbox.width) * 100;
        // var yPercent = (mouseY / bbox.height) * 100;
          
        // // Then we change the `transform-origin` css property on the image to center the zoom effect on the mouse position
        // //event.target.style.transformOrigin = xPercent + '% ' + yPercent + '%';
        // // It's a bit clearer in jQuery:
        // $(this).css('transform-origin', (xPercent+'% ' + yPercent+ '%') );
        // // We add the '%' units to make sure the string looks exactly like the css declaration it becomes.   
  
    });

    // If you want it to automatically trigger on hover
    $('.pattern_btn').on('mouseenter', function() {
        $(this).addClass('zoom-in');
        $(this).removeClass('normal-zoom');
    });

    // and stop when not hovering
    $('.pattern_btn').on('mouseleave', function() {
        $(this).addClass('normal-zoom');
        $(this).removeClass('zoom-in');
    });
}

function removePatternActive(){
    $('.pattern_btn').not(this).removeClass('pattern_active');
}

function copy_pattern(src){
    loadImageToCanvas(src, "canvasOutput", () => {});
    clipboard_active = true;
    x1_right = x2_right;
    y1_right = y2_right;
}


  
  


 

  