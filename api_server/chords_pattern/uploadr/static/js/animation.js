function startAnimation(animation_name) {
    if (animation_name == 'UpdateCanvas')       requestAnimationFrame(animation_UpdateCanvas);
    else if (animation_name == 'UpdateRCursor') requestAnimationFrame(animation_UpdateRCursor);
    else if (animation_name == 'UpdateCursor')  requestAnimationFrame(animation_UpdateCursor);
    else if (animation_name == 'UpdateViews')   requestAnimationFrame(animation_UpdateViews);
    else if (animation_name == 'UpdateInfo')    requestAnimationFrame(animation_UpdateInfo);
}
function animation_UpdateCanvas() {
    UpdateCanvas();
    if(bPlay) requestAnimationFrame(animation_UpdateCanvas);
    // else console.log('done UpdateCanvas.');
}
function animation_UpdateRCursor(t) {
    var start = null;
    var limit = 500;
    if(start === null) start = t;    
    requestAnimationFrame(animation);
    function animation(t) {
        if(t-start > limit) {
            cursorRightOn ^= 0x1; 
            start = t;
        }
        if(!bPlay) {               
            drawCursorRight(false);
            drawCursorRight(true);                    
            requestAnimationFrame(animation);
        }
        // else console.log('done UpdateRCursor.');
    }    
}
function animation_UpdateCursor(t) {
    var start = null;
    var limit = 250;
    if(start === null) start = t;    
    requestAnimationFrame(animation);
    function animation(t) {
        if(t-start > limit) {
            cursorOn ^= 0x1 
            start = t;
        }
        if(bPlay) {
            setChordIndex();
            drawCursor(false);
            drawCursor(true);
        } else {        
            if(mouseBtn == 2 && isMouseActive) drawCursor(false);
            else drawCursor(cursorOn); 
        }
        requestAnimationFrame(animation);
    }    
}
function animation_UpdateViews(t) {
    var start = null;
    var limit = 10;
    if(start === null) start = t;    
    requestAnimationFrame(animation);
    function animation(t) {
        if(t-start > limit) {
            start = t;
            for (var i = 0; i < Channels_N; i++) {
                ch_string = "ch" + String(i + 1);
                var p = light_ctx.getImageData(parseInt(currentTimeMs / pixelTimeMs), parseInt(Channels_H * i + Channels_H / 2), 1, 1).data;        
                document.getElementById(ch_string).style.backgroundColor = 'rgb(' + p[0] + ',' + p[1] + ',' + p[2] + ')';        
                if(bPlay) runPhotoView(i+1,p[0],p[1],p[2]);            
                // else {
                //     try {
                //         recoverChannelColor();
                //     }catch(err) {
                //         console.log(err);
                //     }
                // }        
            }          
        }        
        requestAnimationFrame(animation);
    }    
}

function animation_UpdateInfo(t) {
    var start = null;
    var limit = 300;
    if(start === null) start = t;    
    requestAnimationFrame(animation);
    function animation(t) {
        if(t-start > limit) {
            start = t;
            updateInfoText();       
        }        
        requestAnimationFrame(animation);
    }      
}