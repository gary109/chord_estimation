
let edgeMode = "Both"

function drawColorGradient(ns,ne,y,nc){
    var grd = light_ctx.createLinearGradient(ns,y,ns+ne-ns, y);
    grd.addColorStop(0, nc);
    grd.addColorStop(1, "black");

    var old_fillStyle = light_ctx.fillStyle;
    light_ctx.fillStyle = grd;
    light_ctx.fillRect(ns,y,ne, Channels_H);
    light_ctx.fillStyle = old_fillStyle;
}

function getColorIndex(xs,xe,y) {
    var counts = 0
    var info = [];
    var ns='None',ne='None',nc='None';
    var rule = 0;
    var canvasColor;
    for(var i=xs;i<xe;i++){
        canvasColor = light_ctx.getImageData(i, y+Channels_H/2, 1, 1).data; // rgba e [0,255] 
        if(rule == 0){
            if (((canvasColor[0]+canvasColor[1]+canvasColor[2]) > 0)) {
                var r = canvasColor[0].toString(16);
                var g = canvasColor[1].toString(16);
                var b = canvasColor[2].toString(16);
                nc="#"+r+g+b
                rule = 1;
                ns = i;
                
            }
        }
        else if(rule == 1){
            if (((canvasColor[0]+canvasColor[1]+canvasColor[2]) == 0)) {
                ne = i;
                // drawColorGradient(parseInt(ns),parseInt(ne),parseInt(y),nc);
                rule = 0;

                info[counts] = {
                    'ns':ns,
                    'ne':ne,
                    'nc':nc,
                    'id':counts,
                    'y':y
                };
                counts++;
            }
        }
    }
    return info;
}

function drawGradientColoring(mode,ctx,x1,y1,x2,y2){
    for (var yy=y1;yy<y2;yy+=Channels_H) {
        var grd = ctx.createLinearGradient(x1,yy,x2, yy);

        if(0==mode) {
            grd.addColorStop(0, "white");
            grd.addColorStop(1, "black");
        }
        else if(1==mode) {
            grd.addColorStop(1, "white");
            grd.addColorStop(0, "black");
        }
        else if(2==mode) {
            grd.addColorStop(1, "white");
            grd.addColorStop(0.5, "black");
            grd.addColorStop(0, "white");
        }
        else if(3==mode) {
            grd.addColorStop(1, "black");
            grd.addColorStop(0.5, "white");
            grd.addColorStop(0, "black");
        }
    
        ctx.fillStyle = grd;
        ctx.fillRect(x1,yy,x2-x1, Channels_H);
    }
}

function GradientColor(mode){
    clipboard_active = true;
    if(x2_right == x1_right || y2_right == y1_right) {
        var _Canvas = document.getElementById('canvasOutput');  
        var _ctx = _Canvas.getContext('2d');
        drawGradientColoring(mode,_ctx,0,0,_Canvas.width,_Canvas.height);
        return;
    }
    
    var lightCanvas = document.getElementById('lightCanvas');
    var light_ctx = lightCanvas.getContext('2d');
    drawGradientColoring(mode,light_ctx,x1_right,y1_right,x2_right,y2_right)
    let src = cv.imread(document.getElementById('lightCanvas'));
    let dst = new cv.Mat();
    w=Math.abs(x2_right-x1_right);
    h=Math.abs(y2_right-y1_right);
    let rect = new cv.Rect(x1_right, y1_right, w, h);
    dst = src.roi(rect);
    cv.imshow('canvasOutput', dst);
    src.delete(); dst.delete();
    cPush();  
    x1_right = x2_right;
    y1_right = y2_right;
}

function GBlur() {
    if(x2_right == x1_right || y2_right == y1_right) {
        // GBlur ...
        let src = cv.imread(document.getElementById('canvasOutput'));
        let dst = new cv.Mat();
        let ksize = new cv.Size(21, 21);
        // You can try more different parameters
        cv.GaussianBlur(src, dst, ksize, 0, 0, cv.BORDER_DEFAULT);
        cv.imshow('canvasOutput', dst);
        src.delete(); dst.delete();
        return;
    }
    clipboard_active = true;
    let src = cv.imread(document.getElementById('lightCanvas'));
    let dst = new cv.Mat();
    w=Math.abs(x2_right-x1_right);
    h=Math.abs(y2_right-y1_right);
    let rect = new cv.Rect(x1_right, y1_right, w, h);
    dst = src.roi(rect);
    let p1  = new cv.Point(0, 0);
    let p2  = new cv.Point(0, h);
    let p3  = new cv.Point(w, 0);
    let p4  = new cv.Point(w, h);
    cv.line(dst, p1, p2, [0, 0, 0, 255], 1)
    cv.line(dst, p3, p4, [0, 0, 0, 255], 1)

    // GBlur ...
    let dst1 = new cv.Mat();
    let ksize = new cv.Size(21, 21);
    // You can try more different parameters
    cv.GaussianBlur(dst, dst1, ksize, 0, 0, cv.BORDER_DEFAULT);
    cv.imshow('canvasOutput', dst1);
    src.delete(); dst.delete();dst1.delete();
    pasteEffect();
    cPush();  
    x1_right = x2_right;
    y1_right = y2_right;
}

function mirrorImage(x = 0, y = 0, horizontal = false, vertical = false){ 
    var _Canvas = document.getElementById('canvasOutput');  
    var _ctx = _Canvas.getContext('2d');

    var ctx = _ctx;
    var image = _Canvas;

    ctx.save(); // save the current canvas state 
    ctx.setTransform(
     horizontal ? -1 : 1, 0, // set the direction of x axis 
     0, vertical ? -1 : 1, // set the direction of y axis 
     x + horizontal ? image.width : 0, // set the x origin 
     y + vertical ? image.height : 0 // set the y origin 
    ); 
    ctx.drawImage(image,0,0); 
    ctx.restore(); // restore the state as it was when this function was called 
} 

function Mirror(horizontal, vertical) {
    if(x2_right == x1_right || y2_right == y1_right) {
        mirrorImage(0,0,horizontal, vertical);
        return;
    }
    clipboard_active = true;
    let src = cv.imread(document.getElementById('lightCanvas'));
    let dst = new cv.Mat();
    w=Math.abs(x2_right-x1_right);
    h=Math.abs(y2_right-y1_right);
    let rect = new cv.Rect(x1_right, y1_right, w, h);
    dst = src.roi(rect);
    cv.imshow('canvasOutput', dst);
    src.delete(); dst.delete();
    mirrorImage(0,0,horizontal, vertical);
    pasteEffect();
    cPush();
    x1_right = x2_right;
    y1_right = y2_right;
}

function edgeDetection(src){
    // let p1  = new cv.Point(0, 0);
    // let p2  = new cv.Point(0, h);
    // let p3  = new cv.Point(w, 0);
    // let p4  = new cv.Point(w, h);
    // cv.line(dst, p1, p2, [0, 0, 0, 255], 1)
    // cv.line(dst, p3, p4, [0, 0, 0, 255], 1)
    cv.cvtColor(src, src, cv.COLOR_RGB2GRAY, 0);

    let dst1 = new cv.Mat();
    let dst2 = new cv.Mat();
    let dst3 = new cv.Mat();
    // cv.Laplacian(src, dst1, cv.CV_8U, 3, 1, 0, cv.BORDER_DEFAULT);
    // cv.Canny(src, dst1, 50, 100, 3, false);

    if (edgeMode == "Both") {
        // Right-Side
        cv.Sobel(src, dst1, cv.CV_8U, 1, 0, 1, -1, 0, cv.BORDER_DEFAULT);
        
        // Left-Side
        cv.Sobel(src, dst2, cv.CV_8U, 1, 0, 1, 1, 0, cv.BORDER_DEFAULT);

        cv.addWeighted(dst1,1.0,dst2,1.0,0,dst3);
    }
    else if(edgeMode == "Left") {       
        // Left-Side
        cv.Sobel(src, dst3, cv.CV_8U, 1, 0, 1, 1, 0, cv.BORDER_DEFAULT);
    }
    else if(edgeMode == "Right"){
        // Right-Side
        cv.Sobel(src, dst3, cv.CV_8U, 1, 0, 1, -1, 0, cv.BORDER_DEFAULT);
    }

    cv.imshow('canvasOutput', dst3);
    src.delete();dst1.delete(),dst2.delete(),dst3.delete();

}

function Edge(){
    if(x2_right == x1_right || y2_right == y1_right) {
        let src = cv.imread(document.getElementById('canvasOutput'));
        edgeDetection(src);
        return;
    }
    clipboard_active = true;
    let src = cv.imread(document.getElementById('lightCanvas'));
    
    w=Math.abs(x2_right-x1_right);
    h=Math.abs(y2_right-y1_right);
    let rect = new cv.Rect(x1_right, y1_right, w, h);
    let dst = new cv.Mat();
    dst = src.roi(rect);
    edgeDetection(dst);
    pasteEffect();
    cPush();  
    x1_right = x2_right;
    y1_right = y2_right;
}

function pasteEffect(){
    cPush();
    let src = cv.imread(document.getElementById('canvasOutput'));
    let dst = cv.imread(document.getElementById('lightCanvas'));
    w=src.cols;
    h=src.rows;
    if((y1_right+src.rows)>dst.rows){
        h=dst.rows-y1_right;
        let rect1 = new cv.Rect(0, 0, w, h);
        src = src.roi(rect1)
    }
    let rect = new cv.Rect(x1_right, y1_right, w, h);
    src.copyTo(dst.roi(rect))
    cv.imshow(document.getElementById('lightCanvas'), dst);    
    src.delete();
    dst.delete();
}

function initEdgeForm() {
    var rad = document.edgeForm.edge;
    var prev = null;
    for (var i = 0; i < rad.length; i++) {
        rad[i].addEventListener('change', function() {
            if (this !== prev) {
                prev = this;
            }
            edgeMode = this.value;
            initGridCanvas();
        });
    }
}