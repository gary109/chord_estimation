let chordSeg = [];
let chordSegWidth = [];
let chordSegX=[];
let chordSegX_Offset=[];

const lightBeats = 1;
let chordEndTimeS = 0;

let gt_x_offset = 0;
// let timer_counts = 0;
let cursorOn = true;
let cursorRightOn = true;
const cursorFlashTime = 100; // ms

// let timerUpdateInfo = [];

let clipboard_active = false;


let cursorTime = 0;
let chordsIndex = 0;

const pixelTimeMs=25;
const pixelTimeS=0.025;

const gridCanvas = document.getElementById('gridCanvas');
const grid_ctx = gridCanvas.getContext('2d')

const cursorCanvas = document.getElementById('cursorCanvas');  
const cursor_ctx = cursorCanvas.getContext('2d')

const cursorRightCanvas = document.getElementById('cursorRightCanvas');  
const cursorRight_ctx = cursorRightCanvas.getContext('2d');

let lightCanvas = document.getElementById('lightCanvas');
let light_ctx = lightCanvas.getContext('2d');

const gtCanvas = document.getElementById('gtCanvas');
const gt_ctx = gtCanvas.getContext('2d');

const box_panel = document.getElementById('box_panel');
const canvas_wrapper = document.getElementById("canvas-wrapper");

let clientWidth = document.documentElement.clientWidth;
let clientHeight = document.documentElement.clientHeight;
let gt_x_start = clientWidth / 2;
let gt_y_start = 100;
let currentTimeMs = 0;
let GT_W = 0;
let GT_H = 256;
let gtCanvas_H = 20;
let Channels_N = 6;
let Channels_H = 60;
let Light_W = 0;
let Light_H = Channels_H * Channels_N;
let beats = 120; // 一分鐘六十拍
let beat_W = (60 / beats) / pixelTimeS;
let penWidth = beat_W;

//起始點座標
let x1 = 0;
let y1 = 0;
let x1_offset = 0;

// 終點座標
let x2 = 0;
let y2 = 0;

//起始點座標
let x1_right = 0;
let y1_right = 0;

// 終點座標
let x2_right = 0;
let y2_right = 0;

// 宣告 isMouseActive 為滑鼠點擊的狀態，因為我們需要滑鼠在 mousedown 的狀態時，才會監聽 mousemove 的狀態
let mouseBtn = "None";
let prevMouseBtn = "None";
let isMouseActive = false

// 宣告一個 hasTouchEvent 變數，來檢查是否有 touch 的事件存在
const hasTouchEvent = 'ontouchstart' in window ? true : false;

// 透過上方的 hasTouchEvent 來決定要監聽的是 mouse 還是 touch 的事件
const downEvent = hasTouchEvent ? 'touchstart' : 'mousedown'
const moveEvent = hasTouchEvent ? 'touchmove' : 'mousemove'
const upEvent = hasTouchEvent ? 'touchend' : 'mouseup'
const leaveEvent = 'mouseleave'
const rightKeyEvent = 'contextmenu'

gridCanvas.onwheel = zoom;
gtCanvas.onwheel = zoom;
div_imgGT.onwheel = zoom;

// function startUpdateInfo(interval=200) {
//     window.clearInterval(timerUpdateInfo);
//     timerUpdateInfo = setInterval("updateInfoText();",interval); 
// }

function initGTCanvas() {
    var div_imgGT = document.getElementById("div_imgGT")
    var imgGT = document.getElementById("imgGT")
    var filename = selected_song.split('.')[0]
    var extname = '_ground_truth.png'
    imgGT.src = root_songs + '/' + filename + '/' + filename + extname + '?t='+Math.random();
    setGTWH();
    div_imgGT.style.width = GT_W + gt_x_start+"px"    
    gt_ctx.clearRect(0, 0, GT_W + gt_x_start, gtCanvas_H);  
    gtCanvas.width = GT_W + gt_x_start
    gtCanvas.height = gtCanvas_H
    drawStartMark(gtCanvas);
}

function initLightCanvas() {
    Light_W = GT_W
    lightCanvas.width = Light_W
    lightCanvas.height = Light_H
    light_ctx.clearRect(0, 0, Light_W+4096, Light_H);
    light_ctx.fillStyle = "black";
    light_ctx.fillRect(0, 0, Light_W, Light_H);
    cPush();
}

function initCursorCanvas() {
    Light_W = GT_W
    cursorCanvas.width = Light_W
    cursorCanvas.height = Light_H
    // cursor_ctx.strokeStyle = "gray";
    cursor_ctx.clearRect(0, 0, Light_W+4096, Light_H);
    cursor_ctx.fillStyle = 'rgba(' + '0' + ',' + '0' + ',' + '0' + ','+'.0' + ')'; 
    cursor_ctx.fillRect(0, 0, Light_W, Light_H);
}

function initCursorRightCanvas() {
    Light_W = GT_W
    cursorRightCanvas.width = Light_W
    cursorRightCanvas.height = Light_H
    // cursor_ctx.strokeStyle = "gray";
    cursorRight_ctx.clearRect(0, 0, Light_W+4096, Light_H);
    cursorRight_ctx.fillStyle = 'rgba(' + '0' + ',' + '0' + ',' + '0' + ','+'.0' + ')'; 
    cursorRight_ctx.fillRect(0, 0, Light_W, Light_H);
}

function ClearAllCanvas() {
    light_ctx.beginPath()
    light_ctx.fillStyle = 'black';
    light_ctx.fillRect(0, 0, Light_W, Light_H);
}

function setGTWH() {
    GT_W = gtwVar[songs_index][0];
    GT_H = gtwVar[songs_index][1];           
}

function moveGT() {
    var im = document.getElementById('imgGT');
    gt_x_offset = gt_x_start - currentTimeMs / pixelTimeMs;
    im.style.marginLeft = gt_x_offset + "px";
}

function moveLight() {
    gt_x_offset = gt_x_start - currentTimeMs / pixelTimeMs
    lightCanvas.style.marginLeft = gt_x_offset + "px";
    gridCanvas.style.marginLeft = gt_x_offset + "px";
    cursorCanvas.style.marginLeft = gt_x_offset + "px";
    cursorRightCanvas.style.marginLeft = gt_x_offset + "px";
    
    for (var i = 0; i < Channels_N; i++) {
        // For Keyboard ch1~6
        if (bPlay && (KeyCodeInfo['code'] != 0x0) && (KeyCodeInfo['code']<0x40)) drawKeyboardChannel(i);
        // For Mouse
        else if (isMouseActive) {             
            if (mouseBtn == 0) {
                light_ctx.fillStyle = 'rgba(' + curr_r + ',' + curr_g + ',' + curr_b + ','+1.0 + ')';        
                if (bPlay) light_ctx.fillRect(parseInt(currentTimeMs/pixelTimeMs), y1, 2, Channels_H);
                else {
                    if (clipboard_active){}
                    else{
                        light_ctx.fillRect(x1, y1, getPenWidth(), Channels_H);                   
                    }
                }
            }   
        }     
    }
}

window.addEventListener(rightKeyEvent, function (e) {
    e.preventDefault();
    // alert("rightKeyEvent")
}, false);

gridCanvas.addEventListener(rightKeyEvent, function (e) {
    e.preventDefault();
    // alert("rightKeyEvent")
}, false);

gridCanvas.addEventListener(leaveEvent, function (e) {
    // e.preventDefault();
    isMouseActive = false
    if (mouseBtn == 0){
        cPush();
        mouseBtn = "None";
    }        
})

gridCanvas.addEventListener(upEvent, function (e) {
    // e.preventDefault();
    isMouseActive = false;
    if (mouseBtn == 0) cPush();    
    else if (mouseBtn == 2) actionClipboard('upEvent');
})

function initGridCanvas() {
    Light_W = GT_W
    gridCanvas.width = Light_W
    gridCanvas.height = Light_H
    grid_ctx.strokeStyle = "gray";
    grid_ctx.lineWidth = 1
    grid_ctx.clearRect(0, 0, Light_W, Light_H);
    grid_ctx.fillStyle = 'rgba(' + '0' + ',' + '0' + ',' + '0' + ','+'.0' + ')'; 
    grid_ctx.fillRect(0, 0, Light_W, Light_H);

    if (gridMode ==  "YES") {
        if(alignMode == "YES") {
            grid_ctx.beginPath();
            for (var i=0;i<chordSegX.length;i++){                
                for(var j=0;j<chordSeg[i];j++){
                    var _ = parseFloat(chordSegX[i].split(',')[j]);
                    grid_ctx.moveTo(Math.round(_), 0);
                    grid_ctx.lineTo(Math.round(_), Channels_H * Channels_N);                    
                }   
            }      
            grid_ctx.lineWidth = 1;
            grid_ctx.stroke();      
        }
        else {
            for (var i = 0; i < gridCanvas.width; i += beat_W) {
                grid_ctx.strokeRect(i, 0, beat_W, Channels_H * Channels_N);
            }
        }        
    }
    drawChannelLine(); 
}

function getPenWidth(){
    if(4096 == KeyCodeInfo['code'] && KeyCodeInfo[16]['fixed']=='X') return KeyCodeInfo[16]['penWidth'];
    else if(fillMode == "YES" && alignMode == "YES") {
        var obj_onset_X = onsetsVar[songs_index][chordsIndex]/pixelTimeS;
        var obj_offset_X = offsetsVar[songs_index][chordsIndex]/pixelTimeS;
        var XS = parseFloat(chordSegX[chordsIndex].split(',')[chordSegX_Offset[chordsIndex]])
        var XE = 0;
        if (chordSegX_Offset[chordsIndex]+1 >= chordSeg[chordsIndex]) XE = obj_offset_X;
        else XE = obj_onset_X+(chordSegX_Offset[chordsIndex]+1)*chordSegWidth[chordsIndex]
        return Math.round(XE-Math.round(XS));
    }
    else return penWidth;
}

function transX(h,e){
    var X = 0;    
    if(h == "mousedown" || h == "mousemove") {      
        // #### Draw Width By Grid ####          
        if(fillMode == "YES") { 
            if (alignMode == "YES") {
                var obj_onset = onsetsVar[songs_index]
                var obj_offset = offsetsVar[songs_index]    
                cursorTime = e.offsetX*pixelTimeS;
                for (var i = 0; i < obj_onset.length; i++) {
                    if((parseFloat(obj_onset[i]) <= cursorTime) && (parseFloat(obj_offset[i]) > cursorTime)) {
                        chordsIndex=i;
                        break;
                    }
                }                
                for (var i=chordSeg[chordsIndex];i>=0;i--) {
                    X = parseFloat(chordSegX[chordsIndex].split(',')[i]);                        
                    if(X <= e.offsetX) {
                        chordSegX_Offset[chordsIndex] = i;
                        X = Math.round(X);
                        break;
                    }
                }
            } else {
                var pw = getPenWidth();
                X = parseInt(e.offsetX / pw) * pw;
            }
        // #### Draw Width By pixel ####
        } else X = parseInt(e.offsetX);
    } else {
        var pw = getPenWidth();
        X = parseInt((e.targetTouches[0].pageX-gt_x_offset) / pw) * pw;
    }
    if(4096 == KeyCodeInfo['code']) {
        if(KeyCodeInfo[16]['fixed']=='X'){
            X = KeyCodeInfo[16]['x1'];
        }
        else if (KeyCodeInfo[16]['x1'] != X && KeyCodeInfo[16]['fixed']=='N'){
            KeyCodeInfo[16]['fixed'] = 'Y';
        }
    }
    return X;
}

function transY(h,e){
    var Y = 0;
    // #### Draw Width By Grid ####
    if(h == "mousedown" || h == "mousemove") Y = parseInt(e.offsetY / Channels_H) * Channels_H;
    // #### Draw Width By pixel ####
    else Y = parseInt((e.targetTouches[0].pageY-70) / Channels_H) * Channels_H;
    if (Y>=Channels_H*Channels_N) Y = Channels_H*(Channels_N-1);
    if(4096 == KeyCodeInfo['code']) {
        if(KeyCodeInfo[16]['fixed']=='Y'){
            Y= KeyCodeInfo[16]['y1'];
        }
        else if ((alignMode == "YES") && KeyCodeInfo[16]['y1']!=Y && KeyCodeInfo[16]['fixed']=='N'){
            KeyCodeInfo[16]['fixed'] = 'X';
        }
        else if ((alignMode == "NO") && (Math.abs(KeyCodeInfo[16]['y1']-e.offsetY)>= 2) && KeyCodeInfo[16]['fixed']=='N'){
            KeyCodeInfo[16]['fixed'] = 'X';
        }
    }
    return Y;
}

gridCanvas.addEventListener(downEvent, function (e) {
    if(bPlay) return;
    if(e.offsetX > chordEndTimeS/pixelTimeS) return; // 限制滑鼠X座標不得超過GT
    if(downEvent == "mousedown") mouseBtn = e.button;
    else mouseBtn = 0;

    // 左鍵 
    if (mouseBtn == 0) {
        isMouseActive = true;
        x1 = transX(downEvent,e);
        y1 = transY(downEvent,e);
        actionClipboard('downEvent');
    } 
    // 右鍵
    else if(mouseBtn == 2) {
        isMouseActive = true;
        var pw = getPenWidth();
        if (fillMode == "YES") {            
            if(alignMode == "YES"){
                x1_right=transX(downEvent,e);
                y1_right=transY(downEvent,e);
                x2_right=parseInt(x1_right + pw);
                y2_right=y1_right+Channels_H;
            } else {
                x1_right=transX(downEvent,e);
                y1_right=transY(downEvent,e);
                x2_right=parseInt(x1_right / pw+1) * pw;
                y2_right=parseInt(y1_right / Channels_H+1) * Channels_H;
            }            
        } else {
            x1_right=transX(downEvent,e);
            y1_right=transY(downEvent,e);
            x2_right=parseInt(x1_right);
            y2_right=parseInt(y1_right / Channels_H+1) * Channels_H;
        }
    }

    UpdateCanvas();
})

gridCanvas.addEventListener(moveEvent, function (e) {    
    if(bPlay) return;
    if(e.offsetX > chordEndTimeS/pixelTimeS) return;// 限制滑鼠X座標不得超過GT
    e.preventDefault();
    x1=x2=transX(moveEvent,e);
    y1=y2=transY(moveEvent,e);
    if(mouseBtn == 2 && isMouseActive){}
    else {
        drawCursor(false);
        drawCursor(true);
    }    
    if (!isMouseActive) return;

    // 左鍵 
    if (mouseBtn == 0) actionClipboard('moveEvent')
    // 右鍵 
    else if (mouseBtn == 2) { 
        x2_right = transX(moveEvent,e);
        y2_right = transY(moveEvent,e);
        var x_min = Math.min(x1_right,x2_right);
        var x_max = Math.max(x1_right,x2_right);
        var y_min = Math.min(y1_right,y2_right);
        var y_max = Math.max(y1_right,y2_right);
        var pw = getPenWidth();
        if (fillMode == "YES") {
            if(alignMode == "YES") {
                x1_right = parseInt(x_min);
                y1_right = parseInt(y_min / Channels_H) * Channels_H;
                x2_right = parseInt(x_max)+pw;
                y2_right = parseInt(y_max / Channels_H+1) * Channels_H;
            } else {
                x1_right = parseInt(x_min / pw) * pw;
                y1_right = parseInt(y_min / Channels_H) * Channels_H;
                x2_right = parseInt(x_max / pw+1) * pw;
                y2_right = parseInt(y_max / Channels_H+1) * Channels_H;
            }            
        } else {
            x1_right = parseInt(x_min);
            y1_right = parseInt(y_min / Channels_H) * Channels_H;
            x2_right = parseInt(x_max);
            y2_right = parseInt(y_max / Channels_H+1) * Channels_H;
        }
    }
    UpdateCanvas();
})

function zoom(event) {            
    event.preventDefault();
    if(alignMode == "YES" && fillMode == "YES")
    {
        updateChordBPM(event.deltaY * -0.01)
    }
    else
    {
        bpmInput.value = parseInt(bpmInput.value) - (event.deltaY * -0.01);
        updateTextInput(bpmInput.value);
    }    
}



function drawChannelLine(){
    grid_ctx.fillStyle = "gray";
    for (i = 1; i < Channels_N; i++) {
        grid_ctx.fillRect(0, i * Channels_H, gridCanvas.width, 1);
    }
}

function drawStartMark(canvas1) {
    var ctx = canvas1.getContext("2d");
    ctx.clearRect(0, 0, canvas1.width, canvas1.height);
    //###############################################
    ctx.fillStyle = "blue";
    ctx.fillRect(0, 0, canvas1.width, canvas1.height);
    // ctx.beginPath();
    // ctx.moveTo(gt_x_start, 0);
    // ctx.lineTo(gt_x_start, gtCanvas_H);
    // ctx.strokeStyle = '#ff0000';
    // ctx.lineWidth = 1;
    // ctx.stroke();            
    //###############################################
    ctx.beginPath();
    ctx.strokeStyle = "white";
    ctx.lineWidth = 1;
    for (i = 20; i < canvas1.width; i += 20) {
        if (i % 40 == 0) {
            ctx.moveTo(gt_x_start + i, 0);
            ctx.lineTo(gt_x_start + i, gtCanvas_H);
        } else {
            ctx.moveTo(gt_x_start + i, gtCanvas_H / 2);
            ctx.lineTo(gt_x_start + i, gtCanvas_H);
        }

    }
    for (i = gt_x_start; i > 0; i -= 20) {
        if ((gt_x_start - i) % 40 == 0) {
            ctx.moveTo(i, 0);
            ctx.lineTo(i, gtCanvas_H);
        }
        else {
            ctx.moveTo(i, gtCanvas_H / 2);
            ctx.lineTo(i, gtCanvas_H);
        }

    }
    ctx.stroke();
    //###############################################
    ctx.beginPath();
    ctx.fillStyle = "red";
    ctx.moveTo(gt_x_start - 10, 0);
    ctx.lineTo(gt_x_start, gtCanvas_H);
    ctx.lineTo(gt_x_start + 10, 0);
    ctx.fill();
}

function drawKeyboardChannel(ch){
    if ((KeyCodeInfo['code']>>ch & 0x1) == 0x1){
        cursorRight_ctx.fillStyle = 'rgba(' + curr_r + ',' + curr_g + ',' + curr_b + ','+1.0 + ')';  
        cursorRight_ctx.fillRect(parseInt(currentTimeMs/pixelTimeMs), ch*Channels_H, 1, Channels_H);
        runPhotoView(ch+1,255,255,255);  
        ch_string = "ch" + String(ch + 1);
        document.getElementById(ch_string).style.backgroundColor = 'rgb(255,255,255)';         
    }
}

function UpdateCanvas() {
    currentTimeMs = document.getElementById("track").currentTime * 1000
    audioInfoText = '[Audio]\n' + "Current Time: " + Math.round(currentTimeMs) / 1000  + "s\n";      
    moveGT();
    moveLight();      
}

function drawCursorChordArea() {
    var obj_onset = onsetsVar[songs_index]
    var obj_offset = offsetsVar[songs_index]      

    var onsetPixel = Math.round(obj_onset[chordsIndex]/pixelTimeS);
    var offsetPixel = Math.round(obj_offset[chordsIndex]/pixelTimeS);

    initGridCanvas();

    grid_ctx.strokeStyle = "orange";
    grid_ctx.setLineDash([4]); 
    grid_ctx.lineWidth = 1;
    grid_ctx.strokeRect(onsetPixel, 0, offsetPixel-onsetPixel, Channels_H * Channels_N);
}

function initChordBPM() {
    chordSeg = [];
    chordSegWidth = [];
    chordSegX=[];
    var obj_onset = onsetsVar[songs_index];
    var obj_offset = offsetsVar[songs_index];
    var obj_chords = chordsVar[songs_index];

    for (var i = 0; i < obj_chords.length; i++) {
        chordSeg.push(lightBeats);        
        chordSegWidth.push((obj_offset[i]-obj_onset[i])/lightBeats/pixelTimeS);
        chordSegX.push(String(obj_onset[i]/lightBeats/pixelTimeS)+',');
        chordSegX_Offset.push(0); 
    }
    chordEndTimeS = obj_offset[obj_chords.length-1];
}

function updateChordBPM(val) {
    var obj_onset = onsetsVar[songs_index]
    var obj_offset = offsetsVar[songs_index]        
    if (val < 0) val = -1;
    else val = 1;
    var nn = parseInt(chordSeg[chordsIndex]) - val;
    if (nn < 1) nn = 1;
    chordSeg[chordsIndex] = nn;
    chordSegWidth[chordsIndex] = ((obj_offset[chordsIndex]-obj_onset[chordsIndex])/chordSeg[chordsIndex]/pixelTimeS);
    chordSegX[chordsIndex] = '';
    var obj_onset_X = obj_onset[chordsIndex]/pixelTimeS;
    for(var i=0;i<nn;i++) {chordSegX[chordsIndex] += (obj_onset_X+chordSegWidth[chordsIndex]*i+',');}
    updateCursorInfo();
    initGridCanvas();
}

function clearRightCursor() {
    clipboard_active = false;
    mouseBtn = 'None';
    x1_right = x2_right;
    y1_right = y2_right;

    var _Canvas = document.getElementById('canvasOutput');  
    var _ctx = _Canvas.getContext('2d');

    _ctx.clearRect(0, 0, _Canvas.width, _Canvas.height);
}

function updateCursorInfo(){
    try {
        if (alignMode == "YES") {
            cursorInfoText = '[Cursor]\n'+
                'width: ' + parseFloat(chordSegWidth[chordsIndex].toFixed(2)) + "px\n" +  
                'time : ' + parseFloat((chordSegWidth[chordsIndex]*pixelTimeS).toFixed(2)) + "s\n"+
                'BPM  : ' + parseFloat((60/(chordSegWidth[chordsIndex]*pixelTimeS)).toFixed(2)) +"\n"+
                'Seg  : ' + chordSeg[chordsIndex]+'\n'+
                'chord: ' + chordsVar[songs_index][chordsIndex] + '\n'+
                'onset: ' + onsetsVar[songs_index][chordsIndex].toFixed(2) + '\n'+
                'offset: ' + offsetsVar[songs_index][chordsIndex].toFixed(2) + '\n';
        }
        else {
            cursorInfoText = '[Cursor]\n'+
                'width: ' + Math.round(penWidth * 1000) / 1000 + " px\n" +  
                'time : ' + Math.round(penWidth*pixelTimeS * 1000) / 1000 + "s\n"+
                'BPM  : ' + beats+"\n";
                'chord: ' + chordsVar[songs_index][chordsIndex] + '\n';  
        }
    }catch(err) {
        console.log(err);
    }     
}
function drawCursorRight(on){
    if(on) {                 
        // 右鍵
        if(mouseBtn == 2) {
            cursorRight_ctx.strokeStyle = "red";
            if (cursorRightOn) cursorRight_ctx.setLineDash([4]); 
            else cursorRight_ctx.setLineDash([9]);             
            cursorRight_ctx.lineWidth = 3;

            var newPos = compute_clipboard_newWH(); 
            var _Canvas = document.getElementById('canvasOutput');  
            var _ctx = _Canvas.getContext('2d');            
            cursorRight_ctx.drawImage(_Canvas, newPos['x_min'], newPos['y_min'], newPos['ww'], newPos['hh']);
            cursorRight_ctx.fillStyle = "rgba(255, 128, 255, 0.2)";
            cursorRight_ctx.fillRect(newPos['x_min'], newPos['y_min'], newPos['ww'], newPos['hh']);
            cursorRight_ctx.strokeRect(newPos['x_min'], newPos['y_min'], newPos['ww'], newPos['hh']);            
        }
                
    } else cursorRight_ctx.clearRect(0, 0, Light_W+4096, Light_H);
}

function drawCursor(on){
    if(on) {                 
        if (bPlay) {
            // Compute X
            x1 = parseInt(currentTimeMs/pixelTimeMs);
            x2 = parseInt(currentTimeMs/pixelTimeMs);

            // Compute Y
            if (KeyCodeInfo['code'] != 0) {
                cursor_ctx.strokeStyle = "orange";
                cursor_ctx.setLineDash([4]); 
                cursor_ctx.lineWidth = 3;
                for (var i=0;i<6;i++) {
                    if ((KeyCodeInfo['code']>>i & 0x1) == 0x1){      
                        cursor_ctx.strokeRect(x2, i*Channels_H, 1, Channels_H);
                    }
                }
            }else{
                cursor_ctx.strokeStyle = "orange";
                // cursor_ctx.setLineDash([4]); 
                cursor_ctx.lineWidth = 3;
                cursor_ctx.strokeRect(x2, 0, 1, 6*Channels_H);
            }            
        } else {
            cursor_ctx.strokeStyle = "yellow";
            cursor_ctx.setLineDash([4]); 
            cursor_ctx.lineWidth = 3;
            actionClipboard('drawCursor');
        }        
    } else cursor_ctx.clearRect(0, 0, Light_W+4096, Light_H);  
    updateCursorInfo();
}

function compute_clipboard_newWH(){
    var x_min = Math.min(x1_right,x2_right);
    var x_max = Math.max(x1_right,x2_right);
    var y_min = Math.min(y1_right,y2_right);
    var y_max = Math.max(y1_right,y2_right);

    var ww = Math.abs(x_max-x_min);
    var hh = Math.abs(y_max-y_min);

    return {'ww':ww,'hh':hh,'x_min':x_min,'x_max':x_max,'y_min':y_min,'y_max':y_max};
}

function actionClipboard(action) {
    if('upEvent' == action) {
        if(clipboard_active){
            var newPos = compute_clipboard_newWH();             
            var _Canvas = document.getElementById('canvasOutput');           
            light_ctx.drawImage(_Canvas, newPos['x_min'], newPos['y_min'], newPos['ww'], newPos['hh']);
            mouseBtn = 'None';
            x1_right = x2_right;
            y1_right = y2_right;
        }
    } else if('drawCursor' == action) {
        if(clipboard_active){
            var _Canvas = document.getElementById('canvasOutput'); 
            var newPos = compute_clipboard_newWH();               

            if(0 == newPos['hh']) newPos['hh'] = _Canvas.height;
            if('Chord' == pasteMode) newPos['ww'] = getPenWidth(); 
            else if('Original' == pasteMode){
                 newPos['ww'] = _Canvas.width;       
                 newPos['hh'] = _Canvas.height;
            }
            
            cursor_ctx.drawImage(_Canvas, x1, y1, newPos['ww'], newPos['hh']);
            cursor_ctx.fillStyle = "rgba(255, 128, 255, 0.2)";
            cursor_ctx.fillRect(x1, y1, newPos['ww'], newPos['hh']);
            cursor_ctx.strokeRect(x1, y1, newPos['ww'], newPos['hh']);
        } else cursor_ctx.strokeRect(x1, y1, getPenWidth(), Channels_H);
    } else if('downEvent' == action) {
        if(clipboard_active) {
            var newPos = compute_clipboard_newWH();             
            var _Canvas = document.getElementById('canvasOutput');   

            if(0 == newPos['hh']) newPos['hh'] = _Canvas.height; 
            if('Chord' == pasteMode) newPos['ww'] = getPenWidth(); 
            else if('Original' == pasteMode) {
                newPos['ww'] = _Canvas.width;            
                newPos['hh'] = _Canvas.height;
            }
            light_ctx.drawImage(_Canvas, x1, y1, newPos['ww'], newPos['hh']);
        } else light_ctx.fillRect(x1, y1, getPenWidth(), Channels_H); 
    } else if('moveEvent' == action) {
        if(clipboard_active) {}
        else light_ctx.fillRect(x1, y1, getPenWidth(), Channels_H); 
    }
}