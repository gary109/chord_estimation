let songs_obj = 0;
let root_songs = "/static/HW1";
let selected_song = 0;
let songs_index = 0;
let upload_lightshow = document.getElementById('upload_lightshow');

upload_lightshow.addEventListener('change', function (e) {
    if (e.target.files) {
        // document.getElementById('label_upload').innerHTML="上傳 Light Show: [" + e.target.files[0].value + "]" ;
        let imageFile = e.target.files[0]; //here we get the image file
        var reader = new FileReader();
        reader.readAsDataURL(imageFile);
        reader.onloadend = function (e) {
            var myImage = new Image(); // Creates image object
            myImage.src = e.target.result; // Assigns converted image to image object
            myImage.onload = function (ev) {
                if (GT_W != myImage.width) {
                    alert("燈光秀檔案錯誤(寬度不符)" + "[" + GT_W + "," + myImage.width + "]")
                    upload_lightshow.value = ''
                    return;
                }
                lightCanvas=document.getElementById('lightCanvas');
                light_ctx=lightCanvas.getContext('2d')
                light_ctx.clearRect(0, 0, myImage.width, Channels_N*Channels_H);
                light_ctx.drawImage(myImage, 0, 0,myImage.width,Channels_N*Channels_H); // Draws the image on canvas                  
                document.getElementById('label_filename').innerHTML=upload_lightshow.value.replace(/^.*[\\\/]/, '');
                upload_lightshow.value = ''
            }
        }
    }
});

function DownloadCanvasAsImage() {
    if (document.getElementById('lightshow_filename').value == '')
        alert("檔名錯誤，請重新輸入檔名!!!")
    else {
        let downloadLink = document.createElement('a');
        downloadLink.setAttribute('download', document.getElementById('lightshow_filename').value + '.png');
        let canvas = document.getElementById('lightCanvas');
        let dataURL = canvas.toDataURL('image/png');
        let url = dataURL.replace(/^data:image\/png/, 'data:application/octet-stream');
        downloadLink.setAttribute('href', url);
        downloadLink.click();
    }
}

function onchange_songs() {
    selected_song = songs_obj[document.getElementById('select_songs').selectedIndex]
    songs_index = parseInt(document.getElementById('select_songs').selectedIndex)
    initChordBPM();
    initAudioController();
    initGTCanvas();
    initLightCanvas();
    initCursorCanvas();
    initCursorRightCanvas();
    initGridCanvas();
    pause();
    upload_lightshow.value = '';
    initChordPanel();
    clearRightCursor();
}

function initSelectSongs() {
    var obj = songsVar
    var elm = document.getElementById('select_songs'), // get the select
        df = document.createDocumentFragment(); // create a document fragment to hold the options while we create them
    for (var i = 0; i < obj.length; i++) {
        var option = document.createElement('option'); // create the option element
        option.value = obj[i]; // set the value property
        option.appendChild(document.createTextNode(obj[i])); // set the textContent in a safe way.
        df.appendChild(option); // append the option to the document fragment
    }
    elm.appendChild(df); // append the document fragment to the DOM. this is the better way rather than setting innerHTML a bunch of times (or even once with a long string)    
    elm.selectedIndex = "0";
    selected_song = obj[0];
    songs_obj = obj
    songs_index = parseInt(elm.selectedIndex)
    onchange_songs()            
}

function getBase64Image(img) {
    // var canvas = document.createElement("canvas");
    // canvas.width = img.width;
    // canvas.height = img.height;
    // var ctx = canvas.getContext("2d");
    // ctx.drawImage(img, 0, 0);
    var dataURL = img.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

// $('#submitbutton').click(function(){
//     datatosend = '[Gary] Test Test Test';
//     result = runPyScript("test", datatosend);
//     document.getElementById("label_test").innerHTML = 'Got back ' + result;  
// });

// $('#submitLightShowBtn').click(function(){
//     var img = getBase64Image(lightCanvas)
//     result = runPyScript("upload_lightshow",img);
//     document.getElementById("label_test").innerHTML = 'Got back ' + result;  
// });

function UploadCanvasAsImage() {
    document.getElementById('label_UploadLightShow').innerHTML='';
    if (document.getElementById('lightshow_filename').value == '')
        alert("檔名錯誤，請重新輸入檔名!!!")
    else {
        var img = getBase64Image(lightCanvas)
        result = runPyScript("upload_lightshow",img);
        var responseText = jQuery.parseJSON(result);
        document.getElementById('label_UploadLightShow').innerHTML=responseText['msg'];
    }
}