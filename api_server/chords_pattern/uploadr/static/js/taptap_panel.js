let timerCheckStatus = [];
let sessionKey=[];
let timeout_count = 0;

function UploadLightShow2TapTap() {
    document.getElementById('label_UploadLightShow2TapTap').innerHTML='';
    document.getElementById('progressUploadLightShow').value = 0;
    if (document.getElementById('accountname').value == '')
        alert("帳號錯誤，請重新輸入帳號!!!")
    else if(document.getElementById('password').value == '')
        alert("密碼錯誤，請重新輸入密碼!!!")
    else if(document.getElementById('projname').value == '')
        alert("專案名稱錯誤，請重新輸入專案名稱!!!")
    else {   
        var responseText = jQuery.parseJSON(runPyScript("upload_lightshow2taptap",getBase64Image(lightCanvas)));
        // console.log(responseText);
        if (responseText['code'] == 1){
            startCheckStatus();
            sessionKey=responseText['sessionKey'] 
        }
    }
}


function startCheckStatus(interval=1500) {
    timeout_count=0
    window.clearInterval(timerCheckStatus);
    timerCheckStatus = setInterval("CheckStatus();",interval);
}

function CheckStatus() {    
    timeout_count++;
    result = runPyScript("getStatus",sessionKey);  
    var responseText = jQuery.parseJSON(result);
    document.getElementById('label_UploadLightShow2TapTap').innerHTML=responseText['msg'];
    document.getElementById('progressUploadLightShow').value = responseText['progress'];
    if (responseText['progress'] == 100 || timeout_count>10 || responseText['code'] < 0)
        window.clearInterval(timerCheckStatus);
}