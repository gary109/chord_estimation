var curr_r = 255;
var curr_g = 255;
var curr_b = 255;
function set_color() {
    curr_r = document.getElementById("red").value;
    curr_g = document.getElementById("green").value;
    curr_b = document.getElementById("blue").value;
    document.getElementById("current_color").style.backgroundColor = 'rgb(' + curr_r + ',' + curr_g + ',' + curr_b + ')';

    document.getElementById("red_label").innerText = curr_r;
    document.getElementById("green_label").innerText = curr_g;
    document.getElementById("blue_label").innerText = curr_b;
};

function set_level_color(c) {
    curr_r = c
    curr_g = c
    curr_b = c

    document.getElementById("red").value = curr_r;
    document.getElementById("green").value = curr_g;
    document.getElementById("blue").value = curr_b;

    document.getElementById("red_label").innerText = curr_r;
    document.getElementById("green_label").innerText = curr_g;
    document.getElementById("blue_label").innerText = curr_b;
    document.getElementById("current_color").style.backgroundColor = 'rgb(' + c + ',' + c + ',' + c + ')';
};