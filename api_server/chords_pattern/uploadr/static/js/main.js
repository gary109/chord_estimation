const productName = 'The Chords Pattern Maker'
const VERSION = 'v1.0';
let cursorInfoText = '[Cursor]\n';
let audioInfoText = '[Audio]\n';
let keyboardInfoText = '[Keyboard]\n';
let gtwVar = 0;
let songsVar = 0;
let bPlay = false;

window.onresize = onresize;
window.onload = onload;
window.onOpenCvReady = function () {}
function onresize() {}
function onload() {} 

function init() {  
    document.getElementById('label_version').innerHTML= productName+' '+VERSION;
    clientWidth = document.documentElement.clientWidth;
    clientHeight = document.documentElement.clientHeight;
    gt_x_start = clientWidth / 2;
    gt_y_start = 100;
    initLoadingPanel();
    visibleLoadingPanel(); 
    runPyScript("loadSongInfo",'')
}

function initAllPanel(){
    initChordBPM();
    initSelectSongs();
    initGTCanvas();
    initphotoViewCanvas();
    initLightCanvas();
    initToolBox();            
    initAlignForm();
    initFillForm();
    initGridForm();
    initEdgeForm();
    initPasteForm();
    initCursorCanvas();
    initCursorRightCanvas();
    initPatternsView();
    initHotKeyChannel();      
    // startUpdateInfo(200);  
    startAnimation('UpdateCanvas');
    startAnimation('UpdateCursor');
    startAnimation('UpdateRCursor');
    startAnimation('UpdateViews');
    startAnimation('UpdateInfo');
    hiddenLoadingPanel();
}