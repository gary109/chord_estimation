const PVBG_WIDTH = 480;
const PVBG_HEIGHT = 360;

let pvInfo;
let upload_pv_bg = document.getElementById('upload_pv_bg');

upload_pv_bg.addEventListener('change', function (e) {
    if (e.target.files) {
        // alert(e.target.files[0])
        // document.getElementById('label_upload').innerHTML="上傳 Light Show: [" + e.target.files[0].value + "]" ;
        let imageFile = e.target.files[0]; //here we get the image file
        var reader = new FileReader();
        reader.readAsDataURL(imageFile);
        reader.onloadend = function (e) {
            var myImage = new Image(); // Creates image object
            myImage.src = e.target.result; // Assigns converted image to image object
            myImage.onload = function (ev) {
                pvInfo.bgCanvas = document.getElementById('photoView_BGCanvas');
                pvInfo.bgCanvas_ctx = pvInfo.bgCanvas.getContext('2d');        
                pvInfo.bgCanvas_ctx.clearRect(0,0,PVBG_WIDTH,PVBG_HEIGHT);
                pvInfo.bgCanvas_ctx.drawImage(myImage,0,0,PVBG_WIDTH,PVBG_HEIGHT);                           
                document.getElementById('label_pv_bg_filename').innerHTML=upload_pv_bg.value.replace(/^.*[\\\/]/, '');
                upload_pv_bg.value = ''
            }
        }
    }
});

function pv_rightKeyEvent(e){
    e.preventDefault();
    alert('pv_rightKeyEvent');
}
function pv_leaveEvent(e){
    // e.preventDefault();
    pvInfo.isMouseActive = false
    if (pvInfo.mouseBtn == 0)   
        pvInfo.mouseBtn = "None";
}

function pv_downEvent(e) {
    // recoverChannelColor

    pvInfo.isMouseActive = true;
    pvInfo.x1 = e.offsetX
    pvInfo.y1 = e.offsetY
    
    pvInfo[pvInfo.activeCH+'_CTX'].lineWidth = 5 // 線條寬度
    pvInfo[pvInfo.activeCH+'_CTX'].lineCap = 'round' // 線條兩端圓弧
    pvInfo[pvInfo.activeCH+'_CTX'].lineJoin = 'round' // 線條折角圓弧
}

function pv_upEvent (e) {
    // e.preventDefault();
    pvInfo.isMouseActive = false;
}

function pv_moveEvent(e) {
    if (!pvInfo.isMouseActive)  return;

    // 取得終點座標
    pvInfo.x2 = e.offsetX
    pvInfo.y2 = e.offsetY

    // 開始繪圖
    // pvInfo[pvInfo.activeCH+'_CTX'].beginPath()
    pvInfo[pvInfo.activeCH+'_CTX'].moveTo(pvInfo.x1, pvInfo.y1)
    pvInfo[pvInfo.activeCH+'_CTX'].lineTo(pvInfo.x2, pvInfo.y2)
    pvInfo[pvInfo.activeCH+'_CTX'].stroke()

    // 更新起始點座標
    pvInfo.x1 = pvInfo.x2
    pvInfo.y1 = pvInfo.y2
}

function drawXXXImage(ctx) {
    var img = new Image();
    img.onload = function(){
        ctx.drawImage(img,0,0,PVBG_WIDTH,PVBG_HEIGHT);
    };
    img.src = '/static/img/photoView.png';
}

function setActiveChannel(ch){       
    A='255' ;
    pvInfo[pvInfo.activeCH+'_id'].classList.remove("active");
    pvInfo.activeCH = ch;
    pvInfo[ch+'_id'].classList.add("active");

    if (ch == 'CH1') pvInfo[ch+'_CTX'].strokeStyle = 'rgba(' + '255' + ',' + '0' + ',' + '0' + ','+A + ')';               
    else if(ch == 'CH2') pvInfo[ch+'_CTX'].strokeStyle = 'rgba(' + '0' + ',' + '255' + ',' + '0' + ','+A + ')';         
    else if(ch == 'CH3') pvInfo[ch+'_CTX'].strokeStyle = 'rgba(' + '0' + ',' + '0' + ',' + '255' + ','+A + ')';         
    else if(ch == 'CH4') pvInfo[ch+'_CTX'].strokeStyle = 'rgba(' + '255' + ',' + '255' + ',' + '0' + ','+A + ')';         
    else if(ch == 'CH5') pvInfo[ch+'_CTX'].strokeStyle = 'rgba(' + '0' + ',' + '255' + ',' + '255' + ','+A + ')';         
    else if(ch == 'CH6') pvInfo[ch+'_CTX'].strokeStyle = 'rgba(' + '255' + ',' + '0' + ',' + '255' + ','+A + ')';    

    pvInfo[pvInfo.activeCH+'_Canvas'].style.visibility = "visible";  
    pvInfo[pvInfo.activeCH+'_CTX'].stroke();
    recoverChannelColor();
}
function pv_check_click(obj) {
    photoview_canvas_visible(obj.id.split("_")[0], pvInfo[obj.id].checked); 
}
function pv_btn_click(obj) {
    if ('Clear Active' == obj.innerHTML) {
        for(var i=1;i<=6;i++) {
            if(pvInfo['CH'+i+'_id'].classList.contains("active")) {
                pvInfo['CH'+i+'_CTX'].clearRect(0, 0, PVBG_WIDTH, PVBG_HEIGHT);
                pvInfo['CH'+i+'_CTX'].beginPath();
                pvInfo['CH'+i+'_CTX'].fillStyle = 'rgba(' + '0' + ',' + '0' + ',' + '0' + ','+'0' + ')'; 
                pvInfo['CH'+i+'_CTX'].fillRect(0, 0, PVBG_WIDTH, PVBG_HEIGHT);
            }          
        }
    }
    else if ('Clear Layer' == obj.innerHTML) {
        for(var i=1;i<=6;i++) {
            if(pvInfo['CH'+i+'_Check'].checked) {
                pvInfo['CH'+i+'_CTX'].clearRect(0, 0, PVBG_WIDTH, PVBG_HEIGHT);
                pvInfo['CH'+i+'_CTX'].beginPath();
                pvInfo['CH'+i+'_CTX'].fillStyle = 'rgba(' + '0' + ',' + '0' + ',' + '0' + ','+'0' + ')'; 
                pvInfo['CH'+i+'_CTX'].fillRect(0, 0, PVBG_WIDTH, PVBG_HEIGHT);
            }            
        }
    } else setActiveChannel(obj.innerHTML);   
}

function recoverChannelColor(){
    for(var i=1;i<=6;i++){  
        try {
            if(typeof pvInfo['CH'+i+'_Canvas'] === "undefined"){
                console.log("This property is not defined.");
            }else{
                pvInfo['CH'+i+'_Canvas'].style.opacity = 1.0;
            }            
        }catch(err) {
            console.log(err);
        }
    }
}

function runPhotoView(ch,colorR,colorG,colorB){
    pvInfo['CH'+ch+'_Canvas'].style.visibility = "visible";
    Gray = (colorR*0.299 + colorG*0.587 + colorB*0.114)/255;
    if(Gray < 0.1) Gray = 0.1;
    pvInfo['CH'+ch+'_Canvas'].style.opacity = Gray;  
}

function initphotoViewCanvas() {
    pvInfo = new Object();
    pvInfo.mouseBtn = 'None';
    pvInfo.isMouseActive=false;
        
    pvInfo.x1 = 0;
    pvInfo.y1 = 0;
    pvInfo.x2 = 0;
    pvInfo.y2 = 0;    
    pvInfo.bgCanvas = document.getElementById('photoView_BGCanvas');
    pvInfo.bgCanvas_ctx = pvInfo.bgCanvas.getContext('2d');
    pvInfo.bgCanvas.width = PVBG_WIDTH;
    pvInfo.bgCanvas.height = PVBG_HEIGHT;
    drawXXXImage(pvInfo.bgCanvas_ctx);

    for(var i=1;i<=6;i++) {
        pvInfo['CH'+i+'_Canvas'] = document.getElementById('photoView_CH'+i+'_Canvas');
        pvInfo['CH'+i+'_CTX'] = document.getElementById('photoView_CH'+i+'_Canvas').getContext('2d');
        pvInfo['CH'+i+'_Canvas'].width = PVBG_WIDTH;
        pvInfo['CH'+i+'_Canvas'].height = PVBG_HEIGHT;
        pvInfo['CH'+i+'_selected'] = '';
        pvInfo['CH'+i+'_id'] = document.getElementById('pv_btn'+i);
        pvInfo['CH'+i+'_Check'] = document.getElementById('CH'+i+'_Check');
        pvInfo['CH'+i+'_Check'].checked = true;
    }
    pvInfo['CH1'+'_selected'] = 'selected';

    pvInfo.fgCanvas = document.getElementById('photoView_FGCanvas');
    pvInfo.fgCanvas_ctx = pvInfo.fgCanvas.getContext('2d');
    pvInfo.fgCanvas.width = PVBG_WIDTH;
    pvInfo.fgCanvas.height = PVBG_HEIGHT;

    pvInfo.fgCanvas.addEventListener(rightKeyEvent,pv_rightKeyEvent);
    pvInfo.fgCanvas.addEventListener(leaveEvent,pv_leaveEvent);
    pvInfo.fgCanvas.addEventListener(downEvent,pv_downEvent);
    pvInfo.fgCanvas.addEventListener(upEvent,pv_upEvent);
    pvInfo.fgCanvas.addEventListener(moveEvent,pv_moveEvent);
    pvInfo.activeCH = 'CH1';
    setActiveChannel('CH1');
    pvInfo[pvInfo.activeCH+'_Canvas'].style.visibility = "visible";    
    pvInfo[pvInfo.activeCH+'_CTX'].stroke() 
    recoverChannelColor();
}

function all_photoview_canvas_visible(on){    
    for(var i=1;i<=6;i++) {
        if (on) pvInfo['CH'+i+'_Canvas'].style.visibility = "visible";    
        else pvInfo['CH'+i+'_Canvas'].style.visibility = "hidden";          
    }
}

function photoview_canvas_visible(ch,on){        
    if (on) pvInfo[ch+'_Canvas'].style.visibility = "visible";    
    else pvInfo[ch+'_Canvas'].style.visibility = "hidden";          
    
}