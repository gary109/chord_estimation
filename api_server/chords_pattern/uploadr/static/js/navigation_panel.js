function nv_btn_click(t) {    
    var e = document.getElementById(t.id);
    if('File' == e.innerHTML || 'Edit' == e.innerHTML || 'Control' == e.innerHTML || 'View' == e.innerHTML || 'Info' == e.innerHTML) {
        var active_zIndex = 100;
        for (var i = 0; i < toolbox_name_list.length; i++) {
            if (toolbox_name_list[i].class == e.innerHTML){
                document.getElementById(toolbox_name_list[i].id).style.zIndex = active_zIndex;
                active_zIndex++;
                var _ = document.getElementById(toolbox_name_list[i].content);
                document.getElementById(toolbox_name_list[i].id).style.height = toolbox_name_list[i]['h-']+'px';
                document.getElementById(toolbox_name_list[i].id).style.width = toolbox_name_list[i]['w-']+'px';
                if('visible' == _.style.visibility) {
                    _.style.visibility = "hidden";
                    if(toolbox_name_list[i].id == 'box_photo_view') all_photoview_canvas_visible(false);
                } else {
                    document.getElementById(toolbox_name_list[i].id).style.height = toolbox_name_list[i]['h+']+'px';
                    document.getElementById(toolbox_name_list[i].id).style.width = toolbox_name_list[i]['w+']+'px';
                    if(toolbox_name_list[i].id == 'box_photo_view') all_photoview_canvas_visible(true);
                    _.style.visibility = "visible";
                }
            } else document.getElementById(toolbox_name_list[i].id).style.zIndex = i;    
        }
    }
    else if('-' == e.innerHTML || '+' == e.innerHTML) {
        for (var i = 0; i < toolbox_name_list.length; i++) {
            var _ = document.getElementById(toolbox_name_list[i].content);
            document.getElementById(toolbox_name_list[i].id).style.height = toolbox_name_list[i]['h'+e.innerHTML]+'px';
            document.getElementById(toolbox_name_list[i].id).style.width = toolbox_name_list[i]['w'+e.innerHTML]+'px';
            if (e.innerHTML == '+') _.style.visibility = "visible";
            else {
                _.style.visibility = "hidden";
                all_photoview_canvas_visible(false);
            }
        }
    }
    else if('Reset' == e.innerHTML){
        for (var i = 0; i < toolbox_name_list.length; i++) {
            document.getElementById(toolbox_name_list[i].id).style.zIndex = i;
            var _ = document.getElementById(toolbox_name_list[i].content);
            document.getElementById(toolbox_name_list[i].id).style.height = toolbox_name_list[i]['h+']+'px';
            document.getElementById(toolbox_name_list[i].id).style.width = toolbox_name_list[i]['w+']+'px';
            _.style.visibility = "visible";                
        }
        initToolBox()
    }
}