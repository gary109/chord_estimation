const select_chords = document.getElementById('select_chords');

function initChordPanel() {
    var obj = chordsVar[songs_index];
    select_chords.innerHTML="";
    df = document.createDocumentFragment(); // create a document fragment to hold the options while we create them
    for (var i = 0; i < obj.length; i++) {
        var option = document.createElement('option'); // create the option element
        option.style.color = 'black';
        option.value = obj[i]; // set the value property
        option.appendChild(document.createTextNode("["+ i +"] \t\t"+ obj[i])); // set the textContent in a safe way.
        df.appendChild(option); // append the option to the document fragment
    }
    select_chords.appendChild(df); // append the document fragment to the DOM. this is the better way rather than setting innerHTML a bunch of times (or even once with a long string)    
    select_chords.selectedIndex = "0";
}


function setChordIndex(){
    var obj_onset = onsetsVar[songs_index]
    var obj_offset = offsetsVar[songs_index]            
    for (var i = 0; i < obj_onset.length; i++) {
        if((parseFloat(obj_onset[i]) <= currentTimeMs/1000) && (parseFloat(obj_offset[i]) > currentTimeMs/1000)) {
            select_chords.selectedIndex = String(i);
            break;
        }
    }
}

function onchange_chords() {
    var obj_onsets = onsetsVar[songs_index];
    onset_time = obj_onsets[parseInt(document.getElementById('select_chords').selectedIndex)]
    document.getElementById("track").currentTime = onset_time;
}