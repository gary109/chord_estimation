function initLoadingPanel(){
    document.getElementById("loading").style.visibility = "hidden";
    document.getElementById("loading").width = clientWidth+"px";
    document.getElementById("loading").height = clientHeight/2+"px";
}

function visibleLoadingPanel(){
    document.getElementById("loading").style.visibility = "visible";
    document.getElementById("loading").style.marginLeft = document.documentElement.clientWidth/2 - 200 + "px";
    document.getElementById("loading").style.marginTop = document.documentElement.clientHeight/2 -150 + "px";
    document.getElementById("loading").style.zIndex = 1000;
}

function hiddenLoadingPanel(){
    document.getElementById("loading").style.zIndex = 0;
    document.getElementById("loading").style.visibility = "hidden";
}