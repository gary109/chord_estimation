const bpmInput = document.getElementById('bpm');
let gridMode = "YES";
let fillMode = "YES";    
let alignMode = "YES";  

function updateTextInput(val) {            
    beats = val // 一分鐘六十拍
    beat_W = (60 / beats) / pixelTimeS;
    penWidth=beat_W;
    initGridCanvas();
}

function initGridForm() {
    var rad = document.gridForm.grid;
    var prev = null;
    for (var i = 0; i < rad.length; i++) {
        rad[i].addEventListener('change', function() {
            if (this !== prev) {
                prev = this;
            }
            gridMode = this.value;
            initGridCanvas();
        });
    }    
}

function initFillForm() {
    var rad = document.fillForm.fill;
    var prev = null;
    for (var i = 0; i < rad.length; i++) {
        rad[i].addEventListener('change', function() {
            if (this !== prev) {
                prev = this;
            }
            fillMode = this.value;
        });
    }
}

function initAlignForm() {
    var rad = document.alignForm.align;
    var prev = null;
    for (var i = 0; i < rad.length; i++) {
        rad[i].addEventListener('change', function() {
            if (this !== prev) {
                prev = this;
            }
            alignMode = this.value;
            initGridCanvas();
        });
    }
}