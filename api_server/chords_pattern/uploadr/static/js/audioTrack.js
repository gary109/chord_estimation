const audioTrack = document.getElementById('track');

audioTrack.addEventListener("error", function (e) {
    alert("COULD NOT LOAD AUDIO");
    document.getElementById("loading").style.zIndex = 0;
    document.getElementById("loading").style.visibility = "hidden";
});

audioTrack.addEventListener("play", function (e) {  
    document.getElementById("play_img").src = '/static/img/pause.png?t='+Math.random();
    bPlay = true;
    drawCursorRight(false);
    cursorRight_ctx.drawImage(lightCanvas, 0, 0);
    startAnimation('UpdateCanvas');
});

audioTrack.addEventListener("pause", function (e) {
    bPlay = false;
    light_ctx.drawImage(cursorRightCanvas, 0,0);
    startAnimation('UpdateRCursor');
    $("#play_img").attr('src','/static/img/play.png?t='+Math.random()); 
    
});

audioTrack.addEventListener('loadedmetadata', function (e) {
    document.getElementById("loading").style.zIndex = 0;
    document.getElementById("loading").style.visibility = "hidden";
    startAnimation('UpdateRCursor');
});

audioTrack.addEventListener("loadstart", function (e) {
    document.getElementById("loading").style.visibility = "visible";
    document.getElementById("loading").style.marginLeft = document.documentElement.clientWidth/2 - 200 + "px";
    document.getElementById("loading").style.marginTop = document.documentElement.clientHeight/2 -150 + "px";
    document.getElementById("loading").style.zIndex = 1000;
},false);

function initAudioController() {
    var track = document.getElementById('track');
    var filename = selected_song.split('.')[0]
    var extname = selected_song.split('.')[1]
    track.style.width = document.documentElement.clientWidth + "px";
    document.getElementById("track").setAttribute('src', root_songs + '/' + filename + '/' + selected_song);

    // check audio ready
    var counts = 10
    while (counts <= 0 && track.readyState == 0) {
        setTimeout(function () { alert("After 1 seconds!"); }, 1000);
        counts -= 1;
    }


    track.onseeking = function() {
        UpdateCanvas();
    };
}