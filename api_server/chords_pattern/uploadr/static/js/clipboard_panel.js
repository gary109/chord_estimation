let pasteMode = "Chord";

function initPasteForm() {
    var rad = document.pasteForm.paste;
    var prev = null;
    for (var i = 0; i < rad.length; i++) {
        rad[i].addEventListener('change', function() {
            if (this !== prev) {
                prev = this;
            }
            pasteMode = this.value;
        });
    }    
}
