var cPushArray1 = new Array();
var cStep1 = -1;

var cPushArray = new Array();
var cStep = -1;

const ch1keyinput = document.getElementById('ch1keyinput');
const ch2keyinput = document.getElementById('ch2keyinput');
const ch3keyinput = document.getElementById('ch3keyinput');
const ch4keyinput = document.getElementById('ch4keyinput');
const ch5keyinput = document.getElementById('ch5keyinput');
const ch6keyinput = document.getElementById('ch6keyinput');

let customer_keymap = 
    {
        'ch1keyinput':{
            'obj':ch1keyinput,
            'name':'ch1',
            'code': 0x01,
            'rcode':0xFE,
        },'ch2keyinput':{
            'obj':ch2keyinput,
            'name':'ch2',
            'code': 0x02,
            'rcode':0xFD,
        }, 'ch3keyinput':{
            'obj':ch3keyinput,
            'name':'ch3',
            'code': 0x04,
            'rcode':0xFB,
        }, 'ch4keyinput':{
            'obj':ch4keyinput,
            'name':'ch4',
            'code': 0x08,
            'rcode':0xF7,
        }, 'ch5keyinput':{
            'obj':ch5keyinput,
            'name':'ch5',
            'code': 0x10,
            'rcode':0xEF,
        }, 'ch6keyinput':{
            'obj':ch6keyinput,
            'name':'ch6',
            'code': 0x20,
            'rcode':0xDF,
        }
};
function check_chkey_ready() {
    document.getElementById('label_chkey').innerHTML='Wait 3s ...';
    var timeoutID = window.setInterval(( () => {
           window.clearInterval(timeoutID);
        if(KeyCodeInfo['code'] == 63)
            document.getElementById('label_chkey').innerHTML='PASS';
        else
            document.getElementById('label_chkey').innerHTML='FAIL';
    }), 3000);
}
function initHotKeyChannel(){
    for(var i in customer_keymap){
        customer_keymap[i]['obj'].addEventListener("keyup", function(event) {
            event.preventDefault();            
            customer_keymap[this.id]['obj'].focus = false;
            var key= (event || window.event).keyCode;
            KeyCodeInfo[key] = { 
                'name': event.key,
                'code': customer_keymap[this.id]['code'],
                'rcode':customer_keymap[this.id]['rcode'],
            }
        });
    } 
}
function cut(){
    if(x2_right == x1_right || y2_right == y1_right) return;
    cPush();
    let src = cv.imread(document.getElementById('lightCanvas'));
    let dst = new cv.Mat();
    w=Math.abs(x2_right-x1_right);
    h=Math.abs(y2_right-y1_right);
    let rect = new cv.Rect(x1_right, y1_right, w, h);
    dst = src.roi(rect);
    cv.imshow(document.getElementById('canvasOutput'), dst);
    lightCanvas=document.getElementById('lightCanvas');
    light_ctx=lightCanvas.getContext('2d')
    light_ctx.clearRect(x1_right, y1_right, w, h);
    src.delete();
    dst.delete();
    clipboard_active = 2;
    x1_right = x2_right;
    y1_right = y2_right;
    mouseBtn = 'None';
}
function copy(){
    if(x2_right == x1_right || y2_right == y1_right) return;
    let src = cv.imread(document.getElementById('lightCanvas'));
    let dst = new cv.Mat();
    w=Math.abs(x2_right-x1_right);
    h=Math.abs(y2_right-y1_right);
    let rect = new cv.Rect(x1_right, y1_right, w, h);
    dst = src.roi(rect);
    cv.imshow(document.getElementById('canvasOutput'), dst);
    src.delete();
    dst.delete();
    // cPush();
    clipboard_active = 2;
    x1_right = x2_right;
    y1_right = y2_right;
    mouseBtn = 'None';
}
function paste(){    
    actionClipboard('downEvent');
    cPush();
}
function cPush1() {
    cStep1++;
    if (cStep1 < cPushArray1.length) { cPushArray1.length = cStep1; }
    cPushArray1.push(document.getElementById('lightCanvas').toDataURL());
}
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('lightCanvas').toDataURL());
}  
function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            cPush1();
            lightCanvas = document.getElementById('lightCanvas');
            light_ctx = lightCanvas.getContext('2d');
            light_ctx.drawImage(canvasPic, 0, 0);
        }
    }
}     
function cRedo() {
    if (cStep1 > 0) {        
        var canvasPic = new Image();
        canvasPic.src = cPushArray1[cStep1];
        canvasPic.onload = function () { 
            lightCanvas = document.getElementById('lightCanvas');
            light_ctx = lightCanvas.getContext('2d');
            light_ctx.drawImage(canvasPic, 0, 0);            
            cPush();
        }
        cStep1--;
    }
}  
function enter(){}
function undo() { cUndo(); }
function redo() { cRedo(); }
function del(){
    if(x2_right == x1_right || y2_right == y1_right) return;
    cPush();
    w=Math.abs(x2_right-x1_right);
    h=Math.abs(y2_right-y1_right);
    lightCanvas=document.getElementById('lightCanvas');
    light_ctx=lightCanvas.getContext('2d')
    light_ctx.clearRect(x1_right, y1_right, w, h);
}
function esc(){
    clearRightCursor();
    removePatternActive();
}
