import warnings
def ignore_warn(*args, **kwargs):
    pass
warnings.warn = ignore_warn

import requests
import numpy as np
import pandas as pd
import time
import urllib.request
import sys
import os
from urllib.request import urlopen as url_open
import json #javascript object notation
from PIL import Image #Python Imaging Library
import cv2
from glob import glob
import contextlib

SERVER_URL = 'https://taptapapi.asantegenie.com'

#### APP to Could ###
API_Login = '/api/v0/taptap/app/login'
API_UploadMusic = '/api/v0/taptap/app/music_lib/'
API_GetMusicLibrary = '/api/v0/taptap/app/music_lib'
API_SaveProject = '/api/v0/taptap/app/user/project'
API_RemoveProject = '/api/v0/taptap/app/user/project'
API_GetProjectList = '/api/v0/taptap/app/user/projects'
API_DeleteMusic = '/api/v0/taptap/app/music_lib'
API_GetDeviceInfo = '/api/v0/taptap/app/user/dev_info'

#### APP to FW ###
API_hello = '/app/hello'
API_addProj = '/app/addProj'
API_chkAddProjProgress = '/app/chkAddProjProgress'
API_getBoxInfo = '/app/getBoxInfo' 
API_saveToPlayList = '/app/saveToPlayList'
API_setPlayerPlaying = '/app/setPlayerPlaying'
API_setPlayerPrev = '/app/setPlayerPrev'
API_setPlayerNext = '/app/setPlayerNext'
API_setPlayerVolume = '/app/setPlayerVolume'

class Box():
    def __init__(self, account, pwd, dataroot, projectName=[]):
        self.projectName = projectName
        self.msg = []
        self.code = []
        self.sessionId = []
        self.songId = []
        self.projectId = []
        self.account = account
        self.pwd = pwd
        self.dataroot = dataroot
        self.deviceIntIP = []
        self.progress = 0
        
    def login(self):
        self.progress = 5  
        self.msg = 'login ...'
        self.code = []
        self.sessionID = []
        data = {
            "loginId":self.account,
            "loginPwd":self.pwd,
            "loginType":"email",
            "pnId":"1234567890",
            "imei":"f702be25-9724-4233-925e-e1a1901b1541",
            "devName":"Nexus 5",
            "devModel":"Nexus 5",
            "devOs":"Android",
            "devOsVer":"23",
            "devMfr":"LGE",
            "appVer":"1.2.11"
        }

        headers = {'Content-Type': 'application/json'}
        print('登入 Cloud Server: {} {} ... '.format(self.account, self.pwd))
        with contextlib.closing(requests.post(SERVER_URL+API_Login, data = json.dumps(data), headers = headers)) as r: 
            if r.status_code == requests.codes.ok:
                try:
                    self.code = r.json()['status']['code']
                    # self.msg = r.json()['status']['message']
                    self.sessionId = r.json()['sessionId']
                    self.getBoxIP()                    
                    self.msg = 'login ... SUCCESS'
                except:
                    print('login fail')
                    self.code = -9000
                    self.msg = 'login fail'
                finally:
                    pass                
            else:
                self.code = -9001
                self.msg = 'login fail(%d)'%(r.status_code)
                print('Error Code:',r.status_code)     
        self.progress = 10  
        return self.sessionId
    
    def getMusicLibrary(self):
        uicMusicList = []
        message = []
        status = []
        code = []
        headers = { 
            'sessionId':self.sessionId,
            'Content-Type' : 'application/json',
            'Charset' : 'UTF-8',
            'Connection' : 'Keep-Alive',
        }
        with contextlib.closing(requests.get(SERVER_URL+API_GetMusicLibrary, headers = headers)) as r: 
            if r.status_code == requests.codes.ok: 
                code = r.json()['status']['code']
                message = r.json()['status']['message']
                if code == 1:                
                    uicMusicList = r.json()['uicMusicList']
                else:                
                    print('Code:{} Message:{}'.format(code, message))
                
                self.code = 1
                self.msg = 'getSongID ... SUCCESS'
            else:
                print('Error Code:',r.status_code)
                self.code = -9002
                self.msg = 'getMusicLibrary'%(r.status_code)
        return code,uicMusicList
    
    def getProjectList(self):
        self.msg = 'getProjectList ... '
        headers = { 
            'sessionId':self.sessionId,
            'Content-Type': 'application/json',
            'Charset' : 'UTF-8',
            'Connection' : 'Keep-Alive',
        }

        with contextlib.closing(requests.get(SERVER_URL+API_GetProjectList, headers = headers)) as r: 
            if r.status_code == requests.codes.ok: 
                code = r.json()['status']['code']
                message = r.json()['status']['message']
                if code == 1:                
                    projects = r.json()['projects']
                    self.code = 1
                    self.msg = 'getProjectList ... SUCCESS'
                else:                
                    print('Code:{} Message:{}'.format(code, message))
                    self.code = code
                    self.msg = 'getProjectList ... %s'%(message)                
            else:
                print('Error Code:',r.status_code)
                self.code = -9003
                self.msg = 'getProjectList'%(r.status_code)
        return code,projects

    def getProjectIdByName(self,ProjectName):
        self.projectId = []
        code,projects = self.getProjectList()
        if code == 1:
            for l in  projects:
                if l['projectName'] == ProjectName:
                    self.projectId = l['projectId']
        else:
            print ('Unknow Error! Code:',code)
        return self.projectId

    def getProjectId(self):
        self.msg = 'getProjectId ... '
        self.projectId = []
        code,projects = self.getProjectList()
        if code == 1:
            for l in  projects:
                if l['songId'] == self.songId and l['projectName'] == self.projectName:
                    self.projectId = l['projectId']
        else:
            print ('Unknow Error! Code:',code)
        self.msg = 'getProjectId ... SUCCESS'
        return self.projectId

    def getSongID(self, songName):
        self.msg = 'getSongID ... '
        self.songId = []
        code,uicMusicList = self.getMusicLibrary()
        if code == 1:
            for l in  uicMusicList:
                if l['musicTitle'] == songName:
                    self.songId = l['songId']
        else:
            print ('Unknow Error!!!')
        self.msg = 'getSongID ... SUCCESS'
        return self.songId
    
    def removeProject(self):
        self.msg = 'removeProject ... '
        status = []
        code = []
        headers = { 
            'sessionId':self.sessionId,
            'Content-Type': 'application/json',
            'Charset' : 'UTF-8',
            'Connection' : 'Keep-Alive',
        }

        body = { 
            'projectId':self.projectId
        }

        with contextlib.closing(requests.delete('{}{}/{}'.format(SERVER_URL,API_RemoveProject,self.projectId), 
                                              data = json.dumps(body), 
                                              headers = headers)) as r: 
            if r.status_code == requests.codes.ok: 
                code = r.json()['status']['code']
                message = r.json()['status']['message']
            else:
                message = r.status_code
            self.msg = 'removeProject ... SUCCESS'
            return code,message
    def deleteMusic(self):
        self.msg = 'deleteMusic ...'
        status = []
        code = []
        headers = { 
            'sessionId':self.sessionId,
            'Content-Type': 'application/json',
            'Charset' : 'UTF-8',
            'Connection' : 'Keep-Alive',
        }

        body = { 
            'songId':self.songId
        }

        with contextlib.closing(requests.delete('{}{}/{}'.format(SERVER_URL,API_DeleteMusic,self.songId), 
                                              data = json.dumps(body), 
                                              headers = headers)) as r: 
            if r.status_code == requests.codes.ok: 
                code = r.json()['status']['code']
                message = r.json()['status']['message']
            else:
                message = r.status_code
            self.msg = 'deleteMusic ... SUCCESS'
            return code,message
    
    def uploadMusic(self,songPath):
        self.msg = 'uploadMusic ...'
        import progressbar
        musicTitle = []
        musicLength = []  
        message = []
        status = []
        code = []
        baseName = os.path.basename(songPath)
        headers = { 
            'sessionId':self.sessionId,
            'Content-Type' : 'audio/mp3',
            'Charset' : 'UTF-8',
            'Connection' : 'Keep-Alive',
        }

        def read_in_chunks(file_object, blocksize=1024, chunks=-1):
            """Lazy function (generator) to read a file piece by piece.
            Default chunk size: 1k."""
            f_size = os.fstat(file_object.fileno()).st_size
            with progressbar.ProgressBar(max_value=f_size//1024) as bar:
                try:
                    i = 0
                    while chunks:
                        data = file_object.read(blocksize)
                        if not data:
                            break
                        yield data
                        chunks -= 1      
                        bar.update(i)
                        i += 1
                except:
                    print('Read File Error')
                finally:
                    file_object.close()

        with contextlib.closing(requests.post(SERVER_URL+API_UploadMusic+baseName, data = read_in_chunks(open(songPath, "rb")), headers = headers)) as r: 
            if r.status_code == requests.codes.ok: 
                code = r.json()['status']['code']
                message = r.json()['status']['message']
                if code == 1:                
                    self.songId = r.json()['songId']
                    musicTitle = r.json()['musicTitle']
                    musicLength = r.json()['musicLength']
                else:                
                    print('Code:{} Message:{}'.format(code, message))
            else:
                print('Error Code:',r.status_code)
        self.msg = 'uploadMusic ... SUCCESS'
        return code,self.songId,musicTitle,musicLength
    
   

    
    def saveProject(self, projName, showPath):
        self.msg = 'saveProject ...'
        import progressbar
        from time import gmtime, strftime 
        json_decoded = {}
        self.projectId = '{}_{}'.format(self.songId,strftime("%Y%m%d%H%M%S", gmtime()))
        editedCh = 6
        
     
        basename = os.path.basename(showPath)
        extension = os.path.splitext(basename)[1]
        
        if extension == '.npz':
            chs = np.load(showPath)
            ch_len = len(chs['ch1'])
            showDict = {"tapInfo": [{"info" : [],"channelId": 1, "iconId": 1},
                                    {"info" : [],"channelId": 2, "iconId": 2},
                                    {"info" : [],"channelId": 3, "iconId": 3},
                                    {"info" : [],"channelId": 4, "iconId": 4},
                                    {"info" : [],"channelId": 5, "iconId": 5},
                                    {"info" : [],"channelId": 6, "iconId": 6}],
                        "tagInfo": [], 
                        "rawTapInfo": [{"level":chs['ch1'].tolist(), 'effect':[0 for i in range(ch_len)], "channelId": 1, "iconId": 1}, 
                                       {"level":chs['ch2'].tolist(), 'effect':[0 for i in range(ch_len)], "channelId": 2, "iconId": 2}, 
                                       {"level":chs['ch3'].tolist(), 'effect':[0 for i in range(ch_len)], "channelId": 3, "iconId": 3}, 
                                       {"level":chs['ch4'].tolist(), 'effect':[0 for i in range(ch_len)], "channelId": 4, "iconId": 4}, 
                                       {"level":chs['ch5'].tolist(), 'effect':[0 for i in range(ch_len)], "channelId": 5, "iconId": 5}, 
                                       {"level":chs['ch6'].tolist(), 'effect':[0 for i in range(ch_len)], "channelId": 6, "iconId": 6}], 
                        "projectId": self.projectId, 
                        "projName": projName, 
                        "songId": self.songId, 
                        "editedCh": editedCh}
            with open('%s/temp.json'%(self.dataroot), 'w+') as json_file:
                json.dump(showDict, json_file)
            showPath = '%s/temp.json'%(self.dataroot)   
        elif extension == '.json':
            with open(showPath) as json_file:
                json_decoded = json.load(json_file)
            json_decoded['projectId'] = self.projectId
            json_decoded['projName'] = projName
            json_decoded['songId'] = self.songId
            json_decoded['editedCh'] = editedCh
            json_decoded['tagInfo'] = []
            
            with open(showPath, 'w+') as json_file:
                json.dump(json_decoded, json_file)
        else:
            self.msg = 'saveProject ... 輸入格式錯誤!'
            return code,'輸入格式錯誤! 只能吃 .npz or json ...'
        
        status = []
        code = []
        headers = { 
            'sessionId':self.sessionId,
            'Content-Type' : 'binary/octet-stream',
            'Charset' : 'UTF-8',
            'Connection' : 'Keep-Alive',
        }

        def read_in_chunks(file_object, blocksize=1024, chunks=-1):
            """Lazy function (generator) to read a file piece by piece.
            Default chunk size: 1k."""
            f_size = os.fstat(file_object.fileno()).st_size
            with progressbar.ProgressBar(max_value=f_size//1024) as bar:
                try:
                    i = 0
                    while chunks:
                        data = file_object.read(blocksize)
                        if not data:
                            break
                        yield data
                        chunks -= 1      
                        bar.update(i)
                        i += 1
                except:
                    print('Read File Error')
                finally:
                    file_object.close()

        with contextlib.closing(requests.post('{}{}/{}/{}/{}/{}'.format(SERVER_URL,API_SaveProject,self.projectId,projName,self.songId,editedCh), 
                                              data = read_in_chunks(open(showPath, "rb")), 
                                              headers = headers)) as r: 
            if r.status_code == requests.codes.ok: 
                code = r.json()['status']['code']
                message = r.json()['status']['message']
            else:
                message = r.status_code
            self.msg = 'saveProject ... '+message
            self.code = code
            return code,message


    def getDeviceInfo(self):
        taptapList = []
        headers = { 
            'sessionId':self.sessionId,
            'Content-Type': 'application/json',
            'Charset' : 'UTF-8',
            'Connection' : 'Keep-Alive',
        }

        with contextlib.closing(requests.get(SERVER_URL+API_GetDeviceInfo, headers = headers)) as r: 
            if r.status_code == requests.codes.ok: 
                code = r.json()['status']['code']
                message = r.json()['status']['message']
                if code == 1:                
                    taptapList = r.json()['taptapList']
                else:                
                    print('Code:{} Message:{}'.format(code, message))
            else:
                print('Error Code:',r.status_code)
        return code,taptapList

    def getBoxIP(self):
        boxip = []
        code,taptapList = self.getDeviceInfo()
        if code == 1:
            for l in  taptapList:
                if l['deviceIntIP']:
                    self.deviceIntIP = l['deviceIntIP']
                    break
        else:
            print ('Unknow Error! Code:',code)
        return self.deviceIntIP
    
    def hello(self, debugLevel = 1):  
        try:
            code = 0
            url = 'http://{}{}'.format(self.deviceIntIP,API_hello)
            with contextlib.closing(requests.get(url,timeout=(3, 5))) as r: 
                if r.status_code == requests.codes.ok: 
                    code = r.json()['code']
                    if debugLevel == 1:
                        print('{} ... [Success]'.format(url))
                else:
                    print('{} [{}]'.format(url,r.status_code))
        except requests.exceptions.RequestException as e:
            print(e)
        return code
            
    def chkAddProjProgress(self):
        try:
            code = 0
            data = {
                "ProjId":self.projectId,
            }
            headers = {'Content-Type': 'application/json'}
            url = 'http://{}{}'.format(self.deviceIntIP,API_chkAddProjProgress)            
            with contextlib.closing(requests.post(url, data = json.dumps(data), headers = headers, timeout=(3, 5))) as r: 
                if r.status_code == requests.codes.ok: 
                    code = r.json()['code']
                    if code == 1:                
                        PercentComplete = r.json()['PercentComplete']
                    else:                
                        print('Code:{}'.format(code))
                else:
                    print('Error Code:',r.status_code)
        except requests.exceptions.RequestException as e:
            print(e)
        return code,PercentComplete

    def getAddProjProgress(self):
        try:
            Progress = 0
            while Progress < 100:
                code, Progress = self.chkAddProjProgress()
                sys.stdout.write('\rBox下載專案進度:{}% '.format(Progress))
                sys.stdout.flush()
                if code < 0:
                    break
        except requests.exceptions.RequestException as e:
            print(e)
        return code
            
    def addProj(self): 
        try:
            code = 0
            data = {
                "ProjId":self.projectId,
            }

            headers = {'Content-Type': 'application/json'}
            url = 'http://{}{}'.format(self.deviceIntIP,API_addProj)
            with contextlib.closing(requests.post(url, data = json.dumps(data), headers = headers, timeout=(3, 5))) as r: 
                if r.status_code == requests.codes.ok: 
                    code = r.json()['code']
                else:
                    print('... Error Code:',r.status_code)
        except requests.exceptions.RequestException as e:
            print(e)
        return code
    
    def getBoxInfo(self):  
        try:
            code = 0
            url = 'http://{}{}'.format(self.deviceIntIP,API_getBoxInfo)
            print(url)
            with contextlib.closing(requests.get(url,timeout=(3, 5))) as r: 
                if r.status_code == requests.codes.ok: 
                    code = r.json()['code']
                    if code == 1:
                        ProjList = r.json()['ProjList']
                    else:                
                        print('Code:{}'.format(code))
                else:
                    print('... Error Code:',r.status_code)
        except requests.exceptions.RequestException as e:
            print(e)
        return code,ProjList
    
    def saveToPlayList(self):
        try:
            code = 0
            data = {
                "playerProjIdList":self.projectId,
            }
            headers = {'Content-Type': 'application/json'}
            url = 'http://{}{}'.format(self.deviceIntIP,API_saveToPlayList)            
            with contextlib.closing(requests.post(url, data = json.dumps(data), headers = headers, timeout=(3, 5))) as r: 
                if r.status_code == requests.codes.ok: 
                    code = r.json()['code']
                    print('Code:{}'.format(code))                
                else:
                    print('Error Code:',r.status_code)
        except requests.exceptions.RequestException as e:
            print(e)
        return code
    
    def setPlayerPlaying(self,PlayMode):
        try:
            code = 0
            data = {
                'PlayMode':PlayMode, # 0:STOP, 1:PLAY, 2:PAUSE
                'ProjId':self.projectId
            }
            headers = {'Content-Type': 'application/json'}
            url = 'http://{}{}'.format(self.deviceIntIP,API_setPlayerPlaying)            
            with contextlib.closing(requests.post(url, data = json.dumps(data), headers = headers, timeout=(3, 5))) as r: 
                if r.status_code == requests.codes.ok: 
                    code = r.json()['code']
                    if code == 1:
                        CurPlayProjId = r.json()['CurPlayProjId']
                    else:
                        print('Code:{}'.format(code))                
                else:
                    print('Error Code:',r.status_code)
        except requests.exceptions.RequestException as e:
            print(e)
        return code,CurPlayProjId
    
    def setPlayerPrev(self):
        try:
            code = 0
            url = 'http://{}{}'.format(self.deviceIntIP,API_setPlayerPrev)            
            with contextlib.closing(requests.post(url,timeout=(3, 5))) as r: 
                if r.status_code == requests.codes.ok: 
                    code = r.json()['code']
                    if code == 1:
                        CurPlayProjId = r.json()['CurPlayProjId']
                    else:
                        print('Code:{}'.format(code))                
                else:
                    print('Error Code:',r.status_code)
        except requests.exceptions.RequestException as e:
            print(e)
        return code,CurPlayProjId
    
    def setPlayerNext(self):
        try:
            code = 0
            url = 'http://{}{}'.format(self.deviceIntIP,API_setPlayerNext)            
            with contextlib.closing(requests.post(url,timeout=(3, 5))) as r: 
                if r.status_code == requests.codes.ok: 
                    code = r.json()['code']
                    if code == 1:
                        CurPlayProjId = r.json()['CurPlayProjId']
                    else:
                        print('Code:{}'.format(code))                
                else:
                    print('Error Code:',r.status_code)
        except requests.exceptions.RequestException as e:
            print(e)
        return code,CurPlayProjId
    
    def setPlayerVolume(self,Volume):
        try:
            code = 0
            data = {
                'Volume':Volume
            }
            headers = {'Content-Type': 'application/json'}
            url = 'http://{}{}'.format(self.deviceIntIP,API_setPlayerVolume)            
            with contextlib.closing(requests.post(url, data = json.dumps(data), headers = headers, timeout=(3, 5))) as r: 
                if r.status_code == requests.codes.ok: 
                    code = r.json()['code']                
                else:
                    print('Error Code:',r.status_code)
        except requests.exceptions.RequestException as e:
            print(e)
        return code
    
    def sendProjectToCloud(self,ProjectName, songPath, showPath):
        self.progress = 0
        self.login()
        self.progress = 15  
        if self.sessionId == []:
            print('登入 Cloud Server  ... [Fail]'.format())            
            return self.code,self.msg
        else:
            print('登入 Cloud Server {} ... [Success]'.format(self.sessionId))
        
        self.progress = 30  
        # 取得歌曲ID
        songName = os.path.basename(songPath)
        print('取得歌曲ID({}): {}'.format(songName, self.getSongID(songName)))
        self.progress = 50  
        if self.songId == []:
            # 上傳歌曲
            print('上傳歌曲: ', self.uploadMusic(songPath))

        self.progress = 80  
        # 取得秀ID
        print('取得Project ID:', self.getProjectId())
        if self.projectId != []:
            # 刪除燈光秀
            print('刪除燈光秀({}): '.format(self.projectId), self.removeProject())

            # # 刪除歌曲
            # print('刪除歌曲({}): '.format(self.songId), self.deleteMusic())

            # # 上傳歌曲
            # print('上傳歌曲: ', self.uploadMusic(songPath))

        self.progress = 90  
        # 上傳秀並且與歌曲配對成專案
        code,msg = self.saveProject(ProjectName,showPath)
        print('上傳秀({})並且與歌曲({})配對成專案({}): {}'.format(showPath,songName,ProjectName,(code,msg)))        
        print('Done')
        self.progress = 100  
      
        return self.code,self.msg

    def notifyBoxToDownloadProject(self, ProjectName):
        if self.hello(debugLevel=0) == 1:
            if not self.deviceIntIP:
                print('取得 Box IP: {}'.format(self.getBoxIP()))

            # 通知 BOX 去下載專案（歌＋秀）
            print('通知 BOX 去下載專案({})'.format(ProjectName))
            self.addProj()
            self.getAddProjProgress()

            # 查看 Box 專案（歌＋秀）清單
            # print('查看 Box 專案（歌＋秀）清單：\n', box.getBoxInfo())

            # 通知 Box 將專案儲存為播放清單
            print('通知 Box 將專案({})儲存為播放清單, Code:{}'.format(ProjectName, self.saveToPlayList()))     
        else:
            print('Box 不存在！')
            
    def play(self, ProjectName):
        if self.hello(debugLevel=0) == 1:
            self.getProjectIdByName(ProjectName = ProjectName)
            print('播放({})'.format(ProjectName)) 
            self.setPlayerPlaying(PlayMode = 1) # 0:STOP, 1:PLAY, 2:PAUSE
        else:
            print('Box 不存在！')
            
    def pause(self, ProjectName):
        if self.hello(debugLevel=0) == 1:
            print('暫停播放({})'.format(ProjectName))
            self.setPlayerPlaying(PlayMode = 2) # 0:STOP, 1:PLAY, 2:PAUSE
        else:
            print('Box 不存在！')
            
    def stop(self, ProjectName):
        if self.hello(debugLevel=0) == 1:
            print('停止播放({})'.format(ProjectName)) # 0:STOP, 1:PLAY, 2:PAUSE
            self.setPlayerPlaying(PlayMode = 0) # 0:STOP, 1:PLAY, 2:PAUSE
        else:
            print('Box 不存在！')
            
    def next(self):
        if self.hello(debugLevel=0) == 1:
            print('播放下一首({})：{}'.format(self.setPlayerNext()))
        else:
            print('Box 不存在！')
            
    def prev(self):
        if self.hello(debugLevel=0) == 1:
            print('播放上一首({})：{}'.format(self.setPlayerPrev()))
        else:
            print('Box 不存在！')
        
    def volume(self,volume):
        if self.hello(debugLevel=0) == 1:
            print('調整音量: {} Code: {}'.format(volume, self.setPlayerVolume(volume)))
        else:
            print('Box 不存在！')
