import secrets
from bitstring import BitArray
import progressbar
import librosa, librosa.display
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import IPython.display as ipd
import numpy as np
import pandas as pd
import sklearn
from sklearn import preprocessing
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from skimage.transform import rescale, resize, downscale_local_mean
import cv2
from glob import glob
# from time import time
import time
import shutil
import os
import scipy
from scipy import signal
from scipy import stats
from scipy.misc import imsave
import datetime
import math
from pathlib import Path
import youtube_dl
import argparse
import sys
import mido
import json
import os.path
from os import path
import seaborn as sns
import copy

import PIL
from scipy import misc

import random

from madmom.audio.filters import LogarithmicFilterbank
from madmom.features.onsets import SpectralOnsetProcessor
from madmom.audio.signal import normalize

import parselmouth

from midiutil.MidiFile import MIDIFile
from pandas.core.frame import DataFrame

from multiprocessing import Pool
import multiprocessing as mp

sns.set() # Use seaborn's default style to make attractive graphs
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
class Utils:        
    def __init__(self):
        self.midi_clist  = [
            (0, "red"), (1/12, "red"), 
            (2/12, "orange"),(3/12, "orange"), 
            (4/12, "yellow"), 
            (5/12, "green"), (6/12, "green"), 
            (7/12, "blue"), (8/12, "blue"),
            (9/12, "cyan"), (10/12, "cyan"), 
            (11/12, "purple"), (12/12, "white")]
        self.midiColor = mcolors.LinearSegmentedColormap.from_list("", self.midi_clist)
        self.midiColor.set_bad(color="k")
        
        self.song_structure = ['chorus_symbol_nokey','verse_symbol_nokey','intro_symbol_nokey',
                      'pre-chorus_symbol_nokey','bridge_symbol_nokey','intro-and-verse_symbol_nokey',
                      'instrumental_symbol_nokey','verse-and-pre-chorus_symbol_nokey','pre-chorus-and-chorus_symbol_nokey',
                      'outro_symbol_nokey']

        self.song_info_dict = {
            'song_path':[],  
            'chorus_symbol_nokey':[],                             # 5690
            'verse_symbol_nokey':[],                              # 4102
            'intro_symbol_nokey':[],                              # 2224
            'pre-chorus_symbol_nokey':[],                         # 1027
            'bridge_symbol_nokey':[],                             # 758
            'intro-and-verse_symbol_nokey':[],                    # 754
            'instrumental_symbol_nokey':[],                       # 593
            'verse-and-pre-chorus_symbol_nokey':[],               # 507
            'pre-chorus-and-chorus_symbol_nokey':[],              # 365
            'outro_symbol_nokey':[],                              # 333
        }
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################         
    def init_theorytab(self,datasetPath='../../lead-sheet-dataset/datasets/', csv='theorytab.csv'):
        self.genCSV_theorytab(datasetPath=datasetPath, csv=csv)
        self.genCSV_theorytab_symble_structure(csv=csv)

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################    
    def init_datasets(self,output_root):
#         self.color_Do = (0,0,255)    # 紅色
#         self.color_Re = (0,97,255)   # 橙色
#         self.color_Mi = (0,255,255)　# 黃色
#         self.color_Fa = (0,255,0)　  # 綠色
#         self.color_Sol = (255,0,0)　 # 藍色
#         self.color_La = (84,46,8)　  # 靛色
#         self.color_Si = (240,32,160) # 紫色
        
#         self.color_C = (0,0,255)    # 紅色
#         self.color_D = (0,97,255)   # 橙色
#         self.color_E = (0,255,255)　# 黃色
#         self.color_F = (0,255,0)　  # 綠色
#         self.color_G = (255,0,0)　 # 藍色
#         self.color_A = (84,46,8)　  # 靛色
#         self.color_B = (240,32,160) # 紫色
        self.colorMap = ['red', 'green', 'blue', 'yellow', 'pink', 'orange', 'cyan', 'saddlebrown', 'maroon', 'tomato']
        self.original_dir = 'original'
        self.MIR_ST500 = 'MIR-ST500'
        self.AIcup_testset_ok = 'AIcup_testset_ok'
        self.train_dir = 'train'
        self.val_dir = 'val'
        self.test_dir = 'test'
        self.features_dir = 'features'  
        self.output_root = output_root
        self.source_data_dir_path = './datasets/%s/%s'%(self.original_dir,self.MIR_ST500) 
        self.source_testdata_dir_path = './datasets/%s/%s'%(self.original_dir,self.AIcup_testset_ok)         
        self.feature_file_path = '/'.join((output_root, self.features_dir)) 
        self.train_file_path = '/'.join((output_root, self.train_dir))
        self.val_file_path = '/'.join((output_root, self.val_dir))
        self.test_file_path = '/'.join((output_root, self.test_dir))                                                                                                   
        self.delete_ipynb_checkpoints()
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################         
    def init_vars(self, shift=True, hop_length=512, sample_rate=None,
                  time_steps=1.024, pixel_interval=0.016, isTrain=True, renew_all=True, light_pixel_interval=0.025,verbose=False,total_channels=6):
        self.init_patterns(total_channels)
        self.hop_length = hop_length
        self.sample_rate = sample_rate
        self.time_steps = time_steps
        self.shift = shift
        self.pixel_interval=pixel_interval
        self.time_interval=self.pixel_interval*256
        self.isTrain = isTrain
        self.renew_all = renew_all
        self.light_pixel_interval = light_pixel_interval
        self.verbose = verbose
        print("hop_length=%d, sample_rate=%s, time_steps=%f, shift=%d, pixel_interval=%f, time_interval=%f, isTrain=%d, renew_all=%d, verbose=%d" 
              %(self.hop_length,
                self.sample_rate,
                self.time_steps,
                self.shift,
                self.pixel_interval,
                self.time_interval,
                self.isTrain,
                self.renew_all,self.verbose))    
                
        # df_theorytab_symble = pd.read_csv('theorytab_symble.csv')
        # df_theorytab_structure = pd.read_csv('theorytab_structure.csv')
        # self.theorytab_symble_dict = {}
        # for index, row in df_theorytab_symble.iterrows():
        #     self.theorytab_symble_dict['%s'%(row['chord'])] = row['code']
            
        # self.theorytab_structure_dict = {}
        # for index, row in df_theorytab_structure.iterrows():
        #     self.theorytab_structure_dict['%s'%(row['structure'])] = row['code']
###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def rgb2gray(self, rgb):
        return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])  
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################        
    def api_result2note(self, dataroot, basename, uuid, pitch_freq_th=.5, shift_ms=0, toMIDI=True, filter_noise_ms=60):   
        ###########################################################################
        # Step1. 將一首歌曲的預測結果圖，水平合併成一張 256(長)x 歌曲長度(寬) x 3(RGB)
        ###########################################################################
        import time as ts
        startTime = ts.time()
        img_index = 1
        data = []
        print('[GARY TEST]',dataroot,' ==== ',uuid)
        while True:
            file_path = '%s/results/predict_test/images/input_%s_%d_fake_B.png'%(dataroot,uuid,img_index)
            if path.exists(file_path) == True:
                inputs = cv2.imread(file_path)
                if img_index == 1:
                    data = inputs
                else:
                    data = np.hstack((data, inputs))
            elif path.exists(file_path) == False and img_index == 1:
                print('uuid:%s not exist!!!'%(uuid))
                return []
            else:
                break
            img_index += 1
        ########################################################
        # Step2. 處理 RGB -> 1 channel
        ########################################################
        # b, g, r = cv2.split(data)
        gray_data = self.rgb2gray(data) / 2      
        outmidi_img_path = '%s/%s_midi.png'%(dataroot,basename)
        cv2.imwrite(outmidi_img_path, gray_data)

        ########################################################
        # Step3. 壓縮長(256) ---> 計算出現次數的頻率
        ########################################################
        df = DataFrame(gray_data)
        pitch_list = []
        for i in range(df.shape[1]):
            frequency_df = df[i].value_counts(normalize=True, ascending=False)
            norm_list = frequency_df.tolist()
            frequency_list = frequency_df.index.tolist()
            pitch = math.ceil(frequency_list[0])
            pitch_list.append(pitch)

        # Step 3.1 Shift pitch onset/offset
        if shift_ms != 0:
            shift_pixel = int(math.fabs(shift_ms)/1000/self.pixel_interval)
            for i in range(shift_pixel):
                if shift_ms > 0:
                    pitch_list.insert(0, np.int(0))
                    pitch_list.pop(-1)
                else:
                    pitch_list.pop(0)
                    pitch_list.append(np.int(0))
        
        #====================================================      
        # Step 4. 產出　onset, offset, pitch data format list
        #====================================================
        
        #=========================
        # create your MIDI object
        #=========================
        volume = 100
        midi_note = []
        track = 0   # the only track
        channel = 0     # add some notes
        miditime = 0    # start at the beginning
        mf = MIDIFile(1)     # only 1 track            
        mf.addTrackName(track, miditime, "Sample Track")            
        mf.addTempo(track, miditime, int(1/self.pixel_interval)*60) # mf.addTempo(track, time, 120)            
        
        #=======================================
        # outputing raw_note and midi_note list
        #=======================================
        time = 0
        note_list = []
        prev_pitch = 0
        now_pitch = 0
        onset=0
        offset=0
        index = 0
        for _pitch in pitch_list:
            now_pitch = np.int(_pitch)
            if self.verbose:
                print('[%d] now_pitch:%d, prev_pitch:%d'%(index,now_pitch,prev_pitch))
            index += 1
            if now_pitch != prev_pitch:
                note_list.append((onset, time, prev_pitch))
                                
                if toMIDI:
        #             print(_note)
                    _onset = onset
                    _offset = time
                    duration = _offset - _onset
                    time_onset = round(_onset*self.pixel_interval, 3)  
                    time_offset = round(_offset*self.pixel_interval, 3) 
                    time_duration = round(duration*self.pixel_interval, 3)*1000
                    if prev_pitch >= 21:       
                        if time_duration >= filter_noise_ms: # About 64ms = 0.016*
                            mf.addNote(track, channel, prev_pitch, _onset, duration, volume) 
                            midi_note.append([time_onset, time_offset, prev_pitch])
#                         else:
#                             mf.addNote(track, channel, 0, _onset, duration, volume)
#                             midi_note.append([time_onset, time_offset, 0])
#                     else:            
#                         mf.addNote(track, channel, 0, _onset, duration, volume)
#                         midi_note.append([time_onset, time_offset, 0])
                onset = time+1
            prev_pitch = now_pitch
            time += 1                
            
        if toMIDI:
            # write it to disk            
            # os.makedirs('./midi/', exist_ok=True)    
            outmidi_file = '%s/%s.mid'%(dataroot,basename)
            with open(outmidi_file, 'wb') as outf:
                mf.writeFile(outf)
            print('=== note2midi Done (%s)==='%(outmidi_file))
                  

        np.savez('%s/%s_midi_note.npz'%(dataroot,basename),midi_note=midi_note)
        print('[result2note] Done ... %.3fs'%(ts.time() - startTime))
        return note_list,midi_note
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def note2midi(self, song_id, note_list, filter_noise_ms, volume=100):
        import time as ts
        startTime = ts.time()
        # create your MIDI object
        mf = MIDIFile(1)     # only 1 track
        track = 0   # the only track

        time = 0    # start at the beginning
        mf.addTrackName(track, time, "Sample Track")
        # mf.addTempo(track, time, 120)
        mf.addTempo(track, time, int(1/self.pixel_interval)*60)

        # add some notes
        channel = 0
        new_note = []
        for _note in note_list:
#             print(_note)
            _onset = _note[0]
            _offset = _note[1]
            _pitch = _note[2]

            if _pitch >= 21:
                pitch = _pitch         
                duration = 1
                if (((_offset - _onset)+1)*self.pixel_interval*1000) >= filter_noise_ms: # About 64ms = 0.016*4
                    duration = (_offset - _onset)+1         # 1 beat long
                    time = _onset
                    mf.addNote(track, channel, pitch, time, duration, volume)
                    time_onset = round(time*self.pixel_interval, 3)  
                    time_offset = round((time+duration)*self.pixel_interval, 3)  
                    new_note.append([time_onset, time_offset, pitch])
        
        # write it to disk            
        os.makedirs('./midi/', exist_ok=True)    
        outmidi_file = './midi/%s.mid'%(song_id)
        with open(outmidi_file, 'wb') as outf:
            mf.writeFile(outf)
      
        print('[note2midi] [MIDI] %s '%(outmidi_file))
#         print('=== note2midi Done (%fs) ==='%(time.time() - startTime))
        print('[note2midi] Done ... %.3fs'%(ts.time() - startTime))
        return new_note
 
###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def init_patterns(self, total_channels=6, pattern_path = './model/pattern'):
        self.NOTES_FLAT = ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B']     
        self.secretsGenerator = secrets.SystemRandom()   
        self.pattern_path = pattern_path
        self.total_channels = total_channels
        self.channel_max_code = 2 ** self.total_channels - 1
        # self.direction_list = ['Down_0','Down_1','Up_0','Up_1']
        self.direction_list = ['Down_0','Up_0']
        self.effects_dict = {}
        for i in range(self.total_channels):
            self.effects_dict['ch%d'%(i)] = {
                # 'direction' : self.secretsGenerator.sample(self.direction_list, k=1)[0],
                # 'step' : int(secrets.token_hex(1),16) & 3,
                'code' : 1 << i,
            }
        self.effects_dict['pattern_code'] = 0 # int(secrets.token_hex(1),16) & self.channel_max_code
        self.effects_dict['pre_pattern_code'] = 0
        self.effects_dict['direction'] = self.secretsGenerator.sample(self.direction_list, k=1)[0]
        self.effects_dict['step'] = 1 # int(secrets.token_hex(1),16) & 3
        self.effects_dict['pattern_name'] = []
        self.effects_dict['pre_pattern_name'] = []
        self.effects_dict['decreasing'] = float(0.4)
        self.effects_dict['random'] = 1
        self.effects_dict['pitch'] = 0
        self.effects_dict['pre_pitch'] = 0
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def reverseBits(self,num,bitSize): 
        # convert number into binary representation 
        # output will be like bin(10) = '0b10101' 
        binary = bin(num) 
    
        # skip first two characters of binary 
        # representation string and reverse 
        # remaining string and then append zeros 
        # after it. binary[-1:1:-1]  --> start 
        # from last character and reverse it until 
        # second last character from left 
        reverse = binary[-1:1:-1] 
        reverse = reverse + (bitSize - len(reverse))*'0'
    
        # converts reversed binary string into integer 
    #     print (int(reverse,2))
        # print(reverse)
        return int(reverse,2)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def shift_pattern_code(self,direction,shift=True):
        self.effects_dict['pattern_code'] = 0
        if shift == False:
            return
        for i in range(self.total_channels):
            if direction == 'Down_0': # shift left pad 0                           
                self.effects_dict['ch%d'%(i)]['code'] = (self.effects_dict['ch%d'%(i)]['code'] << self.effects_dict['step']) & self.channel_max_code
            #==================================================================
            elif direction == 'Up_0': # shift right pad 0 
                self.effects_dict['ch%d'%(i)]['code'] = (self.effects_dict['ch%d'%(i)]['code'] >> self.effects_dict['step']) & self.channel_max_code
            #==================================================================
            elif direction == 'Down_1': # shift left pad 1
                self.effects_dict['ch%d'%(i)]['code'] = (self.effects_dict['ch%d'%(i)]['code'] << self.effects_dict['step']) & self.channel_max_code
                self.effects_dict['ch%d'%(i)]['code'] |= 1
            #==================================================================
            elif direction == 'Up_1': # shift right pad 1 
                self.effects_dict['ch%d'%(i)]['code'] = (self.effects_dict['ch%d'%(i)]['code'] >> self.effects_dict['step']) & self.channel_max_code
                self.effects_dict['ch%d'%(i)]['code'] |= (1<<(self.total_channels-1))
            #==================================================================
            self.effects_dict['pattern_code'] |= self.effects_dict['ch%d'%(i)]['code']

###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def set_pattern_code(self, code, direction, step):
        self.effects_dict['direction'] = direction
        self.effects_dict['step'] = step 
        self.effects_dict['pattern_code'] = code
        for i in range(self.total_channels):
            self.effects_dict['ch%d'%(i)]['code'] = code & (1 << i)
        self.effects_dict['pre_pattern_code'] = code
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def random_pattern_code(self,):
        while True:
            new_code = int(secrets.token_hex(1),16) & self.channel_max_code
            if new_code != 0:
                self.set_pattern_code(code=new_code, direction=self.secretsGenerator.sample(self.direction_list, k=1)[0], step=1)
                return new_code
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def init_pattern_code(self, code):
        if code == 0:# or code == self.channel_max_code:
            while True:
                new_code = int(secrets.token_hex(1),16) & self.channel_max_code                
                if new_code != 0:
                    direction = []
                    if new_code == 1:
                        direction = 'Down_0'
                    elif new_code == (1 << (self.channel_max_code-1)):    
                        direction = 'Up_0'
                    else:
                        direction=self.secretsGenerator.sample(self.direction_list, k=1)[0]
                    self.set_pattern_code(code=new_code, direction=direction, step=1)
                    return new_code
        self.effects_dict['pre_pattern_code'] = code
        return code
###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def select_patterns(self,total_channels,unit_steps,pattern_type,shift=True):
        self.effects_dict['pattern_name'] = []
        ###########################################################################################################
        if pattern_type == 'L':
            new_code = self.init_pattern_code(self.effects_dict['pre_pattern_code'])         
            datasetsPath = Path(self.pattern_path)
            links_path = datasetsPath.glob('**/S%02X_E*_L*.png'%(new_code))
            L_list = []
            for link in links_path:
                L_list.append(link)
            L_list = shuffle(L_list)
            pattern_basename = os.path.basename(self.secretsGenerator.sample(L_list, k=1)[0])
            self.effects_dict['pattern_name'] = os.path.splitext(pattern_basename)[0]
            END_STR = self.effects_dict['pattern_name'].split('_', 2)[1][1:]
            self.set_pattern_code(code=int(END_STR, 16), direction=self.secretsGenerator.sample(self.direction_list, k=1)[0], step=1)
            self.effects_dict['decreasing'] = 1#0.9
            
        ###########################################################################################################
        elif pattern_type == 'normal':
            code = self.init_pattern_code(code = self.effects_dict['pattern_code'])
            if self.effects_dict['pre_pitch'] == self.effects_dict['pitch']:    
                print(" [Bling before] {0:0>6b}".format(code))
                code = self.reverseBits(code ,bitSize=self.total_channels)
                print(" [Bling after] {0:0>6b}".format(code))
                self.effects_dict['pattern_name'] = 'S%02X_E%02X_M'%(code, code)
                self.shift_pattern_code(shift=shift, direction='ReverseBits')
                self.effects_dict['decreasing'] = 0
            # elif (self.effects_dict['pitch'] - self.effects_dict['pre_pitch']) <= -4: # when pitch over downer 5
            #     print("### pitch over downer ###")
            #     self.effects_dict['pattern_name'] = 'S06_E06_M'
            #     # self.effects_dict['pattern_code'] = self.effects_dict['pre_pattern_code']
            #     # code = self.init_pattern_code(self.effects_dict['pre_pattern_code'])
            #     # self.set_pattern_code(code=code, direction=self.effects_dict['direction'], step=1)
            #     self.shift_pattern_code(shift=shift, direction=self.effects_dict['direction'])
            elif abs(self.effects_dict['pitch'] - self.effects_dict['pre_pitch']) >= 5: # when pitch over downer 5
                print("### pitch over upper ###")
                self.effects_dict['pattern_name'] = 'S3F_E3F_M'
                # self.effects_dict['pattern_code'] = self.effects_dict['pre_pattern_code']
                # code = self.init_pattern_code(self.effects_dict['pre_pattern_code'])
                # self.set_pattern_code(code=code, direction=self.effects_dict['direction'], step=1)
                self.shift_pattern_code(shift=shift, direction=self.effects_dict['direction'])
                self.effects_dict['decreasing'] = 0
            else:
                self.effects_dict['pattern_name'] = 'S%02X_E%02X_M'%(code, code)
                self.shift_pattern_code(shift=shift, direction=self.effects_dict['direction'])
                self.effects_dict['decreasing'] = 0
            
        ###########################################################################################################
        elif pattern_type == 'S':            
            # self.effects_dict['pre_pattern_code'] = self.reverseBits(self.effects_dict['pre_pattern_code'] ,bitSize=self.total_channels)
            self.effects_dict['pattern_code'] = self.effects_dict['pre_pattern_code']
            code = self.init_pattern_code(self.effects_dict['pre_pattern_code'])
            self.set_pattern_code(code=code, direction=self.effects_dict['direction'], step=1)
            self.effects_dict['pattern_name'] = 'S%02X_E%02X_M'%(code, code)
           
            # self.shift_pattern_code(shift=shift, direction=self.effects_dict['direction'])
            
            self.effects_dict['decreasing'] = 0#0.1
        ##########################################################################################################    
        elif pattern_type == 'party':
            pass
        
        if self.verbose:
            print(' [pattern info] [%s] [now:%s] [pre:%s] [direction:%s] [step:%d]'%(pattern_type, 
                self.effects_dict['pattern_name'], self.effects_dict['pre_pattern_name'], self.effects_dict['direction'], self.effects_dict['step']))
        
        an_image = PIL.Image.open('%s/%s.png'%(self.pattern_path, self.effects_dict['pattern_name']))
        an_image_sz = an_image.resize( (unit_steps, total_channels),  PIL.Image.NEAREST )
        new_grayscale_image_sz = an_image_sz.convert("L")
        new_grayscale_array_sz = np.asarray(new_grayscale_image_sz).astype('float32')
        new_grayscale_array_sz = new_grayscale_array_sz // 100
        if self.verbose:
            self.format_display(new_grayscale_array_sz)

        self.effects_dict['pre_pattern_name'] = self.effects_dict['pattern_name']
        return new_grayscale_array_sz                 
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def api_note2light(self, basename, note_list, dataroot='./light', filter_noise_ms=60, 
                        pattern_mode='normal', total_channels=6,
                        pitch_time_25 = 0.1, pitch_time_50 = 0.3, pitch_time_75 = 0.5): 
        import time as ts
        startTime = ts.time()
        saveName = '%s/%s.npz'%(dataroot, basename)        
        channels = dict()
        for i in range(total_channels):
            channels['ch%d'%(i+1)] = []      
        self.total_channels = total_channels
        self.channel_max_code = 2 ** self.total_channels - 1      
        light_start_time = 0.0
        light_end_time = note_list[-1][1]
        vocal_start_time = note_list[0][0]
        vocal_end_time = note_list[-1][1]
        note_index = 0
        note_counts = 0
        nT = np.array(note_list).T        
        max_pitch = np.max(nT[2])
        min_pitch = np.min(nT[2])

        df_pitch_info = pd.DataFrame({'pitch':nT[2],'time':np.array(nT[1])-np.array(nT[0])}) 
        pitch_time_25 = df_pitch_info.describe().loc[['25%']]['time'][0]
        pitch_time_50 = df_pitch_info.describe().loc[['50%']]['time'][0]
        pitch_time_75 = df_pitch_info.describe().loc[['75%']]['time'][0]

        print(' ================= api_note2light info ================ ')
        print(' Pattern Mode: %s'%(pattern_mode))
        print(' Light total time: %.3f <-> %.3f'%(light_start_time,light_end_time))
        print(' Vocal total time: %.3f <-> %.3f'%(vocal_start_time,vocal_end_time))      
        print(' Pitch: %.3f <-> %.3f'%(min_pitch,max_pitch))     
#         print(df_pitch_info.describe())
        print(' 25%%:%.3f 50%%:%.3f 75%%:%.3f'%(pitch_time_25, pitch_time_50, pitch_time_75))
        print(' ================================================== ')     
        ##################################################################################### 
        #################################### Normal ######################################### 
        ##################################################################################### 
        if pattern_mode == 'normal' or pattern_mode == 'party':
            """
            ==========================================
            vocal   : 一般模式
            pattern : 藉由音長區間，自動搭配合適的圖驣
            ==========================================
            0. 音長 > 500ms(10*0.025), 進入隨機挑選Chorus圖驣，恒亮。
            1. 音長 > 250ms(10*0.025), 進入隨機挑選Chorus圖驣，恒亮。
            2. 音長 <= 250ms(10*0.025), 進入隨機挑選verse圖驣，急速下墜模式。
            """       
            bSel = True
            light_time = light_start_time
            new_level = 0
            bFirst = True
            self.effects_dict['pitch'] = round(note_list[note_index][2],3)
            self.effects_dict['pre_pitch'] = round(note_list[note_index][2],3)-1

            while True:
                
                onset_time = note_list[note_index][0]   # sec
                offset_time = note_list[note_index][1]  # sec
                duration_time = offset_time-onset_time  # sec

                # print(note_index,light_time,onset_time,offset_time)

                # print(offset_time, onset_time)
                decreasing_step = duration_time/self.light_pixel_interval
                unit_steps = int(math.ceil(duration_time/self.light_pixel_interval))
                self.effects_dict['pitch'] = round(note_list[note_index][2],3)
                NOTES_INDEX = int(self.effects_dict['pitch']-60)%12

                KeyOctave = self.effects_dict['pitch']//12 - 1
                if bSel:
                    if self.verbose:
                        print('[light info] [pattern:%s] prev_pitch:%.3f(%s%d) pitch:%.3f(%s%d) duration_time:%.3f'%(
                            self.effects_dict['pattern_name'],
                            self.effects_dict['pre_pitch'],self.NOTES_FLAT[NOTES_INDEX],KeyOctave,
                            self.effects_dict['pitch'],self.NOTES_FLAT[NOTES_INDEX],KeyOctave,duration_time))                    
                    bSel = False                  
                    pattern_array = []
                    #=======================================================
                    # print('[duration_time]',duration_time)
                    if duration_time >= pitch_time_75:
                        pattern_array = self.select_patterns(total_channels, unit_steps, pattern_type='L') 
                    elif duration_time >= pitch_time_50:     
                        pattern_array = self.select_patterns(total_channels, unit_steps, pattern_type=pattern_mode)  
                    elif duration_time >= pitch_time_25:     
                        pattern_array = self.select_patterns(total_channels, unit_steps, pattern_type=pattern_mode)             
                    else:                                          
                        pattern_array = self.select_patterns(total_channels, unit_steps, pattern_type='S')
                
                self.effects_dict['pre_pitch'] = self.effects_dict['pitch']
        
                if light_time >= onset_time and light_time <= offset_time:
                    # print('1')
                    new_level = 10                
                    # if (offset_time-onset_time)*1000 >= filter_noise_ms:
                    #     new_level = 10
                    # else:
                    #     new_level = 0
                    #     decreasing_step = 'noise'
                    
                    for ch in range(total_channels):   
                        _new_level = new_level
                        _new_level = _new_level * pattern_array[ch][note_counts]                            
                        if _new_level > 10:
                            _new_level = 10
                        elif _new_level < 0:
                            _new_level = 0
                        channels['ch%d'%(ch+1)].append(int(_new_level))
                        # channels['ch%d'%(ch+1)].append(_new_level)
                    
                    note_counts += 1
                    
                    if self.verbose:
                        print('[Channels info] t:%.3f note:[%.3f,%.3f,%.3f] ===> light:[%d,%d,%d,%d,%d,%d] %.3fs  unit_steps:%d'%
                              (light_time,onset_time,offset_time,self.effects_dict['pitch'],
                               channels['ch1'][-1],
                               channels['ch2'][-1],
                               channels['ch3'][-1],
                               channels['ch4'][-1],
                               channels['ch5'][-1],
                               channels['ch6'][-1],
                               duration_time,unit_steps))
                elif note_counts == 0:
                    # print('2')
                    if bFirst:
                        # print('3')
                        bFirst = False
                        for ch in range(total_channels):
                            channels['ch%d'%(ch+1)].append(int(new_level))
                    else:       
                        # print('4')                 
                        for ch in range(total_channels):
                            _new_level = channels['ch%d'%(ch+1)][-1]
                            _new_level = _new_level - self.effects_dict['decreasing'] if _new_level > 1 else 0
                            channels['ch%d'%(ch+1)].append(int(_new_level))
                else:               
                    # print('5') 
                    note_counts = 0
                    note_index += 1
                    light_time -= self.light_pixel_interval
                    bSel = True
                        
                if note_index >= len(note_list):
                    note_index = len(note_list)-1
                    
                light_time += self.light_pixel_interval
                if light_time > light_end_time:
                    break
                
            for ch in range(total_channels):
                channels['ch%d'%(ch+1)].append(0.0) 
        
        ##################
        #   Output NPZ   #
        ##################
        
        np.savez(saveName, 
                 ch1=[round(x) for x in channels['ch1']], 
                 ch2=[round(x) for x in channels['ch2']], 
                 ch3=[round(x) for x in channels['ch3']], 
                 ch4=[round(x) for x in channels['ch4']], 
                 ch5=[round(x) for x in channels['ch5']], 
                 ch6=[round(x) for x in channels['ch6']]) 

        ###########################
        #   Output Channels PNG   #
        ###########################
        self.genChannelsPNG(dataroot, basename, channels)

#         print('=== generate npz done ===')
        print('[api_note2light] Done ... %.3fs'%(ts.time() - startTime))
        return channels
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def genChannelsPNG(self,dataroot,basename,channels):        
        saveName = '%s/%s_channels.png'%(dataroot, basename)    
        data = []
        for i,ch in enumerate(channels):
            ch_data = np.array(channels[ch]).reshape(1,-1)
            if i == 0:
                data = copy.deepcopy(ch_data)
            else:
                data = np.vstack((data,copy.deepcopy(ch_data)))  
        data *= 25
        cv2.imwrite(saveName, data)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################
    def format_display(self,array):
        h,w = array.shape
        for _h in range(h):
            _str = ''
            for _w in range(w):
                _str += '%3d,'%(array[_h][_w])
            print('%s'%(_str))
###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def run_output_AB_features_datasets(self, imgs_MP3_Note):
        start_time = datetime.datetime.now()
        imgs_MP3 = imgs_MP3_Note[0]
        imgs_Note = imgs_MP3_Note[1]
        counts = self.run_cut_full_image(imgs_MP3, imgs_Note)        
        elapsed_time = datetime.datetime.now() - start_time
        print('A:%s B:%s counts:%d ---> [%s]' % (os.path.basename(imgs_MP3), os.path.basename(imgs_Note), counts, elapsed_time))
        
###############################################################################################
###############################################################################################    
###############################################################################################
############################################################################################### 
    def run_output_AB_features_datasets_theorytab(self, imgs_AB):
        start_time = datetime.datetime.now()
        imgs_A = imgs_AB[0]
        imgs_B = imgs_AB[1]
        counts = self.run_cut_full_image(imgs_A, imgs_B)        
        elapsed_time = datetime.datetime.now() - start_time
        print('[%d] A:%s B:%s counts:%d ---> [%s]' % (imgs_AB[2], os.path.basename(imgs_A), os.path.basename(imgs_B), counts, elapsed_time))
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################                 
    def output_AB_features_datasets(self, csvFile):
        train = pd.read_csv(csvFile)
        imgs_MP3 = []
        imgs_Note = []
        for _path, _groundtruth, _songs in zip(train['paths'],train['groundtruth'],train['songs']):
            mp3_path = os.path.join(_path, _songs)
            basename = os.path.basename(mp3_path)
            imgname = '%s_feature_full.png'%(os.path.splitext(basename)[0])
            mp3_img_path = os.path.join(_path, imgname)

            groundtruth_path = os.path.join(_path, _groundtruth)
            basename = os.path.basename(groundtruth_path)
            imgname = '%s.png'%(os.path.splitext(basename)[0])
            groundtruth_img_path = os.path.join(_path, imgname)

            if path.exists(mp3_img_path) == True and path.exists(groundtruth_img_path) == True:  
                imgs_MP3.append(mp3_img_path)
                imgs_Note.append(groundtruth_img_path)        
                print(mp3_img_path, groundtruth_img_path)
            else:
                print(" Not found MP3(%s) or groundtruth(%s)"%(mp3_img_path,groundtruth_img_path))
        try:
            shutil.rmtree(self.feature_file_path)
        except OSError as e:
            print(e)
        else:
            print("The %s directory is deleted successfully"%(self.feature_file_path))
            os.makedirs('%s' % (self.feature_file_path), exist_ok=True)    
            self.delete_ipynb_checkpoints()
            self.cut_full_image(imgs_MP3, imgs_Note)
        print("=== Done ===")
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################         
    def output_test_datasets(self, csvFile, who=None):
        test = pd.read_csv(csvFile)
        imgs_MP3 = []
        count = 1
        for _path, _songs in zip(test['paths'],test['songs']):
            mp3_path = os.path.join(_path, _songs)
            dirname = os.path.dirname(mp3_path)
            features_path = '%s/features'%(os.path.dirname(mp3_path))
            basename = os.path.basename(mp3_path)
            imgname = '%s_feature_full.png'%(os.path.splitext(basename)[0])
            mp3_img_path = os.path.join(_path, imgname)
            if path.exists(mp3_img_path) == True and self.renew_all == True:
                print('[%d] %s'%(count, mp3_img_path))
                imgs_MP3.append(mp3_img_path)
                count += 1
            elif path.exists(mp3_img_path) == True and self.renew_all == False and path.exists(features_path) == False:
                print('[%d] %s'%(count, mp3_img_path))
                imgs_MP3.append(mp3_img_path)
                count += 1
            else:
                print(" Not found MP3 %s"%(mp3_img_path))   
        if who == None:
            self.cut_features_image(imgs_MP3)
        else:
            self.cut_features_image([imgs_MP3[who]])
        print("=== Done ===")
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################          
    def run_copy_train_file(self, __path):
        basename = os.path.basename(__path)
        save_path = '/'.join((self.train_file_path, basename))
        shutil.copy(__path, save_path)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
    def run_copy_val_file(self, __path):
        basename = os.path.basename(__path)
        save_path = '/'.join((self.val_file_path, basename))
        shutil.copy(__path, save_path)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
    def run_copy_test_file(self, __path):
        basename = os.path.basename(__path)
        save_path = '/'.join((self.test_file_path, basename))
        shutil.copy(__path, save_path)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################          
    def run_copy_train_file_theorytab(self, __path):
        desPath = '/'.join((self.output_root, self.train_dir))
        print('[COPY Train Datasets] %s -> %s '%(__path, desPath))
        shutil.copy(__path, desPath)
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
    def run_copy_val_file_theorytab(self, __path):
        desPath = '/'.join((self.output_root, self.val_dir))
        print('[COPY Val Datasets] %s -> %s '%(__path, desPath))
        shutil.copy(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
    def run_copy_test_file_theorytab(self, __path):
        desPath = '/'.join((self.output_root, self.test_dir))
        print('[COPY Test Datasets] %s -> %s '%(__path, desPath))
        shutil.copy(__path, desPath )
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
    def output_train_datasets_theorytab(self, test_size=0.1, total_size=10000):
        import random
        startTime = time.time()
        image_path_list = []
        
        for file_path in glob('%s/*.png'%(self.feature_file_path)):
            image_path_list.append(file_path)
    
        if len(image_path_list) < total_size:
            total_size = len(image_path_list)
            
        data_list = shuffle(image_path_list)  
        
        _train_size = int(total_size*(1-test_size))
        _test_size = int(total_size*(test_size)/2)
        _val_size = int(total_size*(test_size)/2)
        
        train_list = data_list[0:_train_size]
        test_list = data_list[_train_size:_train_size+_test_size]
        val_list = data_list[_train_size+_test_size:_train_size+_test_size+_val_size]                            

        path_list = [self.train_file_path , self.val_file_path, self.test_file_path]
        for pa in path_list:
            try:
                shutil.rmtree(pa)
            except OSError as e:
                print(e)
            else:
                print("The %s directory is deleted successfully"%(pa))
            os.makedirs('%s' % (pa), exist_ok=True)
   
        self.delete_ipynb_checkpoints()
        
        
        # Muti-processing
        print('### [%s] Creating Train/Val/Test Datasets For Pix2Pix ###'%(self.output_root))  
        # pool_size = mp.cpu_count()
        # print('pool size:%d'%(pool_size))
        # pool = Pool(processes=pool_size) # Pool() 不放參數則默認使用電腦核的數量    
        pool_size = 5
        print('pool size:%d'%(pool_size))
        pool = Pool(processes=pool_size) # Pool() 不放參數則默認使用電腦核的數量  
        pool.map(self.run_copy_train_file_theorytab, train_list)
        pool.map(self.run_copy_val_file_theorytab, val_list)
        pool.map(self.run_copy_test_file_theorytab, test_list)
        pool.close()  
        pool.join()     

        print("[%s] Row image size:"%(self.feature_file_path), len(image_path_list))
        print("[%s] train:%d test:%d val:%d"%(self.output_root, len(train_list), len(test_list), len(val_list)))
        print('=== Finishied %fs ==='%(time.time() - startTime))  
###############################################################################################
###############################################################################################    
###############################################################################################
###############################################################################################           
    def output_train_datasets(self, test_size=0.1, total_size=10000):
        import random
        startTime = time.time()
        image_path_list = []
        for file_path in glob('{}/*.png'.format(self.feature_file_path)):
            image_path_list.append(file_path)
            
        if len(image_path_list) < total_size:
            total_size = len(image_path_list)
            
        data_list = shuffle(image_path_list)  
        
        _train_size = int(total_size*(1-test_size))
        _test_size = int(total_size*(test_size)/2)
        _val_size = int(total_size*(test_size)/2)
        
        train_list = data_list[0:_train_size]
        test_list = data_list[_train_size:_train_size+_test_size]
        val_list = data_list[_train_size+_test_size:_train_size+_test_size+_val_size]
        
        print("train:%d test:%d val:%d"%(len(train_list), len(test_list), len(val_list)))

        path_list = [self.train_file_path, self.val_file_path, self.test_file_path]
        for pa in path_list:
            try:
                shutil.rmtree(pa)
            except OSError as e:
                print(e)
            else:
                print("The %s directory is deleted successfully"%(pa))
            os.makedirs('%s' % (pa), exist_ok=True)
            
        self.delete_ipynb_checkpoints()
        
        
        # Muti-processing
        print('### Creating Train/Val/Test Datasets For Pix2Pix ###')  
        pool_size = mp.cpu_count()
        print('pool size:%d'%(pool_size))
        pool = Pool(processes=pool_size) # Pool() 不放參數則默認使用電腦核的數量   
        pool.map(self.run_copy_train_file, train_list)
        pool.map(self.run_copy_val_file, val_list)
        pool.map(self.run_copy_test_file, test_list)
        pool.close()  
        pool.join()     
        print('=== Finishied %fs ==='%(time.time() - startTime))  
   
    def run_note2img(self, song_groundtruth_path):
        start_time = datetime.datetime.now()
        ##############################################################
        _path = song_groundtruth_path[0]
        mp3_path = song_groundtruth_path[1]
        groundtruth_path = song_groundtruth_path[2]
        ##############################################################
        y, sr = librosa.load(mp3_path,offset=0.0,sr=self.sample_rate)
        total_sec = librosa.get_duration(y=y, sr=sr)
#         print("[%d] %s - %s [%fs]"%(count,os.path.basename(mp3_path),os.path.basename(groundtruth_path),total_sec))  
        ##############################################################
        zero_pitch = 0
        time_index = 0
        notes = []
        with open(groundtruth_path,'r', newline=None) as fp:
            for line in fp.readlines():
                line = line.strip().split(' ',2)
                g_pitch_start_time = np.float64(line[0])
                g_pitch_end_time = np.float64(line[1])
                g_pitch = np.int64(line[2])
                while time_index <= g_pitch_end_time:
                    if time_index >= g_pitch_start_time:
                        notes.append(g_pitch)
                    else:
                        notes.append(zero_pitch)
                        
                    time_index += self.pixel_interval

        while time_index <= total_sec:
            notes.append(zero_pitch)
            time_index += self.pixel_interval
                        
        note_image = np.zeros((256,len(notes),3), np.uint8)

        x = 0
        for i in notes:
            c = int(i*2)
            cv2.line(note_image, (x, 0), (x, 255), (c,c,c), 1)
            x += 1

        basename_groundtruth = os.path.splitext(os.path.basename(groundtruth_path))[0]
        save_path = '%s/%s.png'%(_path,basename_groundtruth)
        try:
            os.remove(save_path)
        except OSError as e:
            print(e)
        else: 
            pass
        cv2.imwrite(save_path, note_image)
        elapsed_time = datetime.datetime.now() - start_time
        print('########################## %s #############################' % (os.path.basename(mp3_path)))
        print(" %s - %s [%fs]"%(os.path.basename(mp3_path),os.path.basename(groundtruth_path),total_sec)) 
        print(' elapsed_time: %s '%(elapsed_time))
        
    def note2img(self,csvFile):
        train = pd.read_csv(csvFile)
        count = 1
        for _path, _groundtruth, _songs in zip(train['paths'],train['groundtruth'],train['songs']):
            mp3_path = os.path.join(_path, _songs)
            groundtruth_path = os.path.join(_path, _groundtruth)
            if path.exists(mp3_path) == True:
                if path.exists(groundtruth_path) == True:            
                    ##############################################################
                    y, sr = librosa.load(mp3_path,offset=0.0,sr=self.sample_rate)
                    total_sec = librosa.get_duration(y=y, sr=sr)
                    print("[%d] %s - %s [%fs]"%(count,os.path.basename(mp3_path),os.path.basename(groundtruth_path),total_sec))  
                    count += 1    
                    ##############################################################
                    zero_pitch = 0
                    time_index = 0
                    notes = []
                    with open(groundtruth_path,'r', newline=None) as fp:
                        for line in fp.readlines():
                            line = line.strip().split(' ',2)
                            g_pitch_start_time = np.float64(line[0])
                            g_pitch_end_time = np.float64(line[1])
                            g_pitch = np.int64(line[2])
#                             print('pitch %s start:%s ~ end:%s'%(g_pitch,g_pitch_start_time,g_pitch_end_time))
                            while time_index <= g_pitch_end_time:
                                if time_index >= g_pitch_start_time:
                                    notes.append(g_pitch)
                                else:
                                    notes.append(zero_pitch)

                                time_index += self.pixel_interval

                    while time_index <= total_sec:
                        notes.append(zero_pitch)
                        time_index += self.pixel_interval

                    note_image = np.zeros((256,len(notes),3), np.uint8)

                    x = 0
                    for i in notes:
                        c = int(i*2)
                        cv2.line(note_image, (x, 0), (x, 255), (c,c,c), 1)
                        x += 1


                    basename_groundtruth = os.path.splitext(os.path.basename(groundtruth_path))[0]
                    cv2.imwrite('%s/%s.png'%(_path,basename_groundtruth), note_image)
                else:
                    print(" Not found Groundtruth %s"%(groundtruth_path))
            else:
                print(" Not found MP3 %s"%(mp3_path))
        print("=== Done ===")
        
    def parser_mp3datasets(self, datasetPath, toCsvName):
        datasetsPath = Path(datasetPath)

        # mp3 path
        mp3_path_list = []
        links_path = datasetsPath.glob('**/*.mp3')
        for link in links_path:
            mp3_path_list.append(link)
            
            
         # gen csv    
        data = pd.DataFrame()
        data['paths'] = mp3_path_list
        data.to_csv(os.path.join('./', toCsvName), index=False)        
        return mp3_path_list
        
        
    def parser_datasets2csv(self, datasetPath, toCsvName):
        datasetsPath = Path(datasetPath)

        # video_links
        links_path = datasetsPath.glob('*/*_link.txt')
        video_links = []
        for link in links_path:
            with open(link,'r', newline=None) as fp:
                for line in fp.readlines():
                    line = line.strip()
                    video_links.append(line)
                    break

        songs = []
        paths = []
        groundtruth = []
        features = []
        vocals = []

        links_path = datasetsPath.glob('*/*_link.txt')
        for link in links_path:
            dirname  = os.path.dirname(link)
            basename = os.path.basename(dirname)
            groundtruth_name = '%s_groundtruth.txt'%(basename)
            if self.isTrain:
                groundtruth.append(groundtruth_name)
            else:
                groundtruth.append('None')
            paths.append(dirname)
            songs.append('%s.mp3'%(basename))
            features.append('%s_feature.json'%(basename))
            vocals.append('%s_vocal.json'%(basename))

        # gen csv    
        data = pd.DataFrame()
        data['paths'] = paths
        data['songs'] = songs
        data['features'] = features
        data['vocals'] = vocals
        data['groundtruth'] = groundtruth
        data['video_links'] = video_links

        data.to_csv(os.path.join('./', toCsvName), index=False)
        print('=== %s Done ==='%('Train' if self.isTrain == True else 'Test'))
    
    def download_song2mp3(self,csvPath, count=1):
        data = pd.read_csv(csvPath)
        i = count
        for path,video_link,song in zip(data['paths'][i:],data['video_links'][i:],data['songs'][i:]):
            print('=== %d ==='%(count))
            download_video('%s/%s'%(path,song.split(".")[0]), video_link)
            count += 1
    
    def download_video(self,title, video_url):
        ydl_opts = {
            'outtmpl': '{}.%(ext)s'.format(title),
            'format': 'bestaudio/best',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
            }],
        }

        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            try:
                ydl.download([video_url])
            except ValueError:
                print("Oops!  That was no valid number.  Try again...")

        return {
            'audio': open('{}.mp3'.format(title), 'rb'),
            'title': title,
        } 
    
    
    
    def run_cut_full_image(self, song_path, show_path, feature_file_path=None):
#         dirname = os.path.dirname(song_path)
#         basename = os.path.basename(dirname)  
        number = os.path.splitext(os.path.basename(song_path))[0]
        count = 1

        songImg = cv2.imread(song_path)
        showImg = cv2.imread(show_path)
        w, h ,c = showImg.shape
               
        # check loop length must to be min (song_h, show_h)
        song_w, song_h ,song_c = songImg.shape 
        if song_h < h:
            w = song_w
            h = song_h
            c = song_c
        
        if self.shift:
            shift_window = self.time_steps / self.pixel_interval
        else:
            shift_window = 256
        window_width = 256

        for offset_i in (np.arange(0,window_width,shift_window)):  
            window_size = int((h-offset_i)/window_width) 
            for ind in range(window_size):
                start = 256 * (ind) + int(offset_i) 
                end = 256 * (ind + 1) + int(offset_i)
                diff = end - start
                newSongImg = songImg[:,start:end,:]
                newShowImg = showImg[:,start:end,:]
                save_path = '/'.join((self.feature_file_path, '{}_{}.png'.format(number,count)))
                data = np.hstack((newSongImg,newShowImg)) # Domain A & B
                cv2.imwrite(save_path, data)
                count = count + 1
        return count

    def cut_full_image(self, song_full_path, show_full_path, feature_file_path=None):
        file_count = 1
        for song_path, show_path in (zip(song_full_path, show_full_path)):
#             dirname = os.path.dirname(song_path)
#             basename = os.path.basename(dirname)
            number = os.path.splitext(os.path.basename(song_path))[0]
            count = 1
            print('====== [{}/{}] ======'.format(file_count,len(song_full_path)))
            print('====== {}_{} ======'.format(number,count))
            print('SONG ===> {} '.format(song_path))
            print('NOTE ===> {} '.format(show_path))

            songImg = cv2.imread(song_path)
            showImg = cv2.imread(show_path)
            w, h ,c = showImg.shape

            # check loop length must to be min (song_h, show_h)
            song_w, song_h ,song_c = songImg.shape 
            if song_h < h:
                w = song_w
                h = song_h
                c = song_c

            print(w, h ,c)
            if self.shift:
                shift_window = self.time_steps / self.pixel_interval
            else:
                shift_window = 256
            window_width = 256

            for offset_i in (np.arange(0,window_width,shift_window)):  
                window_size = int((h-offset_i)/window_width) 
#                 print('<<offset {} , window_size {}>>'.format(round(offset_i, 1), window_size))
                for ind in range(window_size):
                    start = 256 * (ind) + int(offset_i) 
                    end = 256 * (ind + 1) + int(offset_i)
                    diff = end - start
                    newSongImg = songImg[:,start:end,:]
                    newShowImg = showImg[:,start:end,:]
                    save_path = '/'.join((self.feature_file_path, '{}_{}.png'.format(number,count)))
                    data = np.hstack((newSongImg,newShowImg)) # Domain A & B
                    cv2.imwrite(save_path, data)                  
                    print(f'{count} -> {start}-{end} : {diff}')
                    count = count + 1
            file_count += 1
            
    def api_cut_features_image(self, song_full_path):
        self.delete_ipynb_checkpoints()
        file_count = 1
        for song_path in (song_full_path):
            dirname = os.path.dirname(song_path)
            output_features_path = '%s/features/' % (dirname)
            print('[GARY]',output_features_path)
            ###################################################################################################
            try:
                shutil.rmtree(output_features_path)
            except OSError as e:
                print(e)                
            else:
                print("The %s directory is deleted successfully"%(output_features_path))
            os.makedirs(output_features_path, exist_ok=True)  
            print('The %s directory is created successfully'%(output_features_path))
            ###################################################################################################
            number = os.path.basename(dirname)
            count = 1
            print('====== [{}/{}] ======'.format(file_count,len(song_full_path)))
            print('====== {}_{} ======'.format(number,count))
            print('SONG ===> {} '.format(song_path))
            songImg = cv2.imread(song_path)
            w, h ,c = songImg.shape
            print(w, h ,c)
 
            if self.shift:
                shift_window = self.time_steps / self.pixel_interval
            else:
                shift_window = 256
            window_width = 256

            for offset_i in (np.arange(0,window_width,shift_window)):  
                window_size = int((h-offset_i)/window_width) 
                print('<<offset {} , window_size {}>>'.format(round(offset_i, 1), window_size))
                for ind in range(window_size):
                    start = 256 * (ind) + int(offset_i) 
                    end = 256 * (ind + 1) + int(offset_i)
                    diff = end - start
                    newSongImg = songImg[:,start:end,:]
                    save_path = '/'.join((output_features_path, '{}_{}.png'.format(number,count))) 
                    cv2.imwrite(save_path, newSongImg)                  
                    print(f'{count} -> {start}-{end} : {diff}')
                    count = count + 1
            file_count += 1

    def cut_features_image(self, song_full_path):
        self.delete_ipynb_checkpoints()
        file_count = 1
        for song_path in (song_full_path):
            dirname = os.path.dirname(song_path)
            output_features_path = '%s/features/' % (dirname)
            print('[GARY]',output_features_path)
            ###################################################################################################
            try:
                shutil.rmtree(output_features_path)
            except OSError as e:
                print(e)                
            else:
                print("The %s directory is deleted successfully"%(output_features_path))
            os.makedirs(output_features_path, exist_ok=True)  
            print('The %s directory is created successfully'%(output_features_path))
            ###################################################################################################
            number = os.path.basename(dirname)
            count = 1
            print('====== [{}/{}] ======'.format(file_count,len(song_full_path)))
            print('====== {}_{} ======'.format(number,count))
            print('SONG ===> {} '.format(song_path))
            songImg = cv2.imread(song_path)
            w, h ,c = songImg.shape
            print(w, h ,c)
 
            if self.shift:
                shift_window = self.time_steps / self.pixel_interval
            else:
                shift_window = 256
            window_width = 256

            for offset_i in (np.arange(0,window_width,shift_window)):  
                window_size = int((h-offset_i)/window_width) 
                print('<<offset {} , window_size {}>>'.format(round(offset_i, 1), window_size))
                for ind in range(window_size):
                    start = 256 * (ind) + int(offset_i) 
                    end = 256 * (ind + 1) + int(offset_i)
                    diff = end - start
                    newSongImg = songImg[:,start:end,:]
                    save_path = '/'.join((output_features_path, '{}_{}.png'.format(number,count))) 
                    cv2.imwrite(save_path, newSongImg)                  
                    print(f'{count} -> {start}-{end} : {diff}')
                    count = count + 1
            file_count += 1
           
    def delete_ipynb_checkpoints(self):
        # delete all .ipynb_checkpoints dir
        for filename in Path(os.getcwd()).glob('**/*.ipynb_checkpoints'):
            try:
                shutil.rmtree(filename)
            except OSError as e:
                print(e)
            else: 
                print("The %s is deleted successfully" % (filename))

    def normalize(self,x, axis=0):
        return sklearn.preprocessing.minmax_scale(x, axis=axis)
    
    def drawWavefrom(self, y, sr, song_num):
        librosa.display.waveplot(y, sr=sr, linewidth=0.01, alpha=0.5, x_axis='ms')
    
    def drawCentroids(self,y, sr, song_num):
        # 計算頻譜質心
        spectral_centroids = librosa.feature.spectral_centroid(y, sr=sr)[0]
        frames = range(len(spectral_centroids))
        t = librosa.frames_to_time(frames)

        # 節奏 onset strength # Irene
        onset_env = librosa.onset.onset_strength(y=y, sr=sr)
        plt.plot(t, 1+self.normalize(onset_env), color='g', linewidth=0.01, alpha=0.5)
            
        # Median aggregation, and custom mel options
        onset_env = librosa.onset.onset_strength(y=y, sr=sr, aggregate=np.median, fmax=8000, n_mels=256)
        times = librosa.frames_to_time(np.arange(len(onset_env)), sr=sr)
        plt.plot(times, onset_env / onset_env.max(), color='c', linewidth=0.01, alpha=0.5)
            

    def draw_pitch(self, pitch, ylim_min=0, ylim_max=600):
        # Extract selected pitch contour, and
        # replace unvoiced samples by NaN to not plot
        pitch_values = pitch.selected_array['frequency']        
        print(pitch_values.shape)
        pitch_values[pitch_values==0] = np.nan
        midi_number=librosa.hz_to_midi(pitch_values)
        midi_number=midi_number%12/12
#         midi_number[midi_number == np.nan] = 0
#         midi_number[midi_number == -np.inf] = 0
        
#         l = [x for x in midi_number if ~np.isnan(x)]
#         print(l)
        
        count = 0
        l = []
        for x in midi_number:
            if np.isnan(x):
                l.append(1.0)
            else:
                l.append(x)
            count += 1
#         print(l)
#         l = np.array(l)
        
#         midi_number = librosa.hz_to_midi(pitch_values) if pitch_values != np.nan else 0
#         print(pitch.xs())
        
#         midi_number[midi_number == -np.inf] = 0
#         midi_number[midi_number == np.nan] = 0
#         print(aaa)
#         midi_number[midi_number == -np.inf] = 0
        
#         print(librosa.hz_to_midi(pitch_values))
#         pitch_values[pitch_values<ylim_min] = np.nan
#         pitch_values[pitch_values>ylim_max] = np.nan
#         plt.plot(pitch.xs(), pitch_values, '-.',linewidth=1, markersize=1, color='orange')
#         plt.plot(pitch.xs(), pitch_values, '.', markersize=0.05, color='r')
#         plt.plot(pitch.xs(), pitch_values, 'o', markersize=0.01, linewidth=0.01, color='r')
#         plt.scatter(pitch.xs(),pitch_values, s=.01, color='r')
#         plt.plot(pitch.xs(), pitch_values, 'o-',linewidth=.01, markersize=0.01, color='orange')
#         plt.grid(False)
#         plt.bar(pitch.xs(),pitch_values,linewidth=0.01, align='edge', width = 0.01, edgecolor='b', color='b', alpha=0.5, cmap='rainbow')       
        plt.bar(pitch.xs(), pitch_values, linewidth=0.01, align='edge', width = 0.01,  alpha=.5,
                edgecolor=self.midiColor(l), color=self.midiColor(l))
     
#         plt.plot(pitch.xs(),pitch_values, '-.',linewidth=.5, alpha=0.5, color=self.midiColor(l) )
        plt.ylim(ylim_min, ylim_max)
#         plt.ylabel("fundamental frequency [Hz]")

        plt.xticks([])
        plt.yticks([])
 
        
            
    def draw_spectrogram(self,spectrogram,dynamic_range=70):
        X, Y = spectrogram.x_grid(), spectrogram.y_grid()
        sg_db = 10 * np.log10(spectrogram.values)
#         plt.pcolormesh(X, Y, sg_db, vmin=sg_db.max() - dynamic_range, cmap='afmhot')
        plt.pcolormesh(X, Y, sg_db, vmin=sg_db.max() - dynamic_range)

        plt.ylim([spectrogram.ymin, spectrogram.ymax])
#         plt.xlabel("time [s]")
#         plt.ylabel("frequency [Hz]")
        plt.xticks([])
        plt.yticks([])

    def draw_intensity(self, intensity, db_min=0, db_max=100):
#         plt.plot(intensity.xs(), intensity.values.T, linewidth=3, color='w')
#         plt.plot(intensity.xs(), intensity.values.T, linewidth=1)
#         plt.plot(intensity.xs(), intensity.values.T, linewidth=1, color='w')
        plt.plot(intensity.xs(), intensity.values.T, linewidth=.5, color='r', alpha=0.5)
        plt.grid(False)
        plt.ylim(db_min,db_max)
#         plt.ylabel("intensity [dB]")
        plt.xticks([])
        plt.yticks([])
            
    def drawMFCC(self, y, sr):
        mfccs = librosa.feature.mfcc(y=y, sr=sr, hop_length=self.hop_length, n_mfcc=40)
#         _mfccs = mfccs.astype('float64')
        _mfccs = np.array(mfccs).astype('float64')
        _mfccs = sklearn.preprocessing.scale(_mfccs, axis=1)
#         librosa.display.specshow(mfccs, sr=sr, x_axis='time',y_axis='linear',  linewidth=0.01)  
#         librosa.display.specshow(mfccs, sr=fs, x_axis='ms',  y_axis='hz', linewidth=0.01)
        librosa.display.specshow(_mfccs, sr=sr, x_axis='ms',  y_axis='mel', linewidth=0.01)
        plt.ylim([0, 8192])
        plt.xticks([])
        plt.yticks([])
        return mfccs
        
    def drawMFCC_delta(self, mfcc, sr):
#         _mfccs = mfcc.astype('float32')
        _mfccs = np.array(mfcc).astype('float64')
        _mfccs = sklearn.preprocessing.scale(_mfccs, axis=1)
        mfcc_delta = librosa.feature.delta(_mfccs)
  
        librosa.display.specshow(mfcc_delta, sr=sr, x_axis='ms',  y_axis='mel', linewidth=0.01)
        plt.ylim([0, 8192])
        plt.xticks([])
        plt.yticks([])
        
    def drawSTFT(self, y, sr):
        # STFT 語音頻譜分析圖
        D = librosa.amplitude_to_db(abs(librosa.stft(y)), ref=np.max)
#         librosa.display.specshow(D, x_axis='time', y_axis='linear',sr=sr,  cmap='coolwarm', linewidth=0.01)
        librosa.display.specshow(D, x_axis='ms', y_axis='log',sr=sr, linewidth=0.01, fmin=0.0, fmax=5000.0)
        plt.xticks([])
        plt.yticks([])

    def drawChroma_cqt(self,y, sr):
        chromagram = librosa.feature.chroma_cqt(y=y, sr=sr, bins_per_octave=12*3)
        librosa.display.specshow(chromagram, x_axis='ms', y_axis='chroma', hop_length=self.hop_length, sr=sr, linewidth=0.01)
        
    def drawChroma_cens(self,y, sr):
        chromagram = librosa.feature.chroma_cens(y, sr=sr, hop_length=self.hop_length, n_chroma=12*2)
        librosa.display.specshow(chromagram, x_axis='ms', y_axis='chroma', sr=sr, hop_length=self.hop_length, cmap='coolwarm')
        
    def drawChroma_harmonic_cens(self,y, sr):
         # 計算 CQT+Chroma                
        y_harm = librosa.effects.harmonic(y=y, margin=8)
        chroma_os_harm = librosa.feature.chroma_cqt(y=y_harm, sr=sr, bins_per_octave=12*3)
        chromagram = librosa.feature.chroma_cens(y,sr=sr,C=chroma_os_harm, bins_per_octave=12*3, n_chroma=12)
        librosa.display.specshow(chromagram, x_axis='ms', y_axis='chroma',sr=sr, linewidth=0.01)
        
    def drawChroma_smooth(self,y, sr):
#         chroma_orig = librosa.feature.chroma_cqt(y=y, sr=sr)
#         C = np.abs(librosa.cqt(y=y, sr=sr, bins_per_octave=12*3, n_bins=7*12*3))
#         librosa.display.specshow(librosa.amplitude_to_db(C, ref=np.max)[idx], y_axis='cqt_note', bins_per_octave=12*3)
  
        y_harm = librosa.effects.harmonic(y=y, margin=8)
        chroma_os_harm = librosa.feature.chroma_cqt(y=y_harm, sr=sr, bins_per_octave=12*3)
        chroma_filter = np.minimum(chroma_os_harm,
                           librosa.decompose.nn_filter(chroma_os_harm,
                                                       aggregate=np.median,
                                                       metric='cosine'))
        chroma_smooth = scipy.ndimage.median_filter(chroma_filter, size=(1, 9))
        librosa.display.specshow(chroma_smooth, x_axis='ms', y_axis='chroma',sr=sr, linewidth=0.01)
        
    def drawChroma_stft(self, y, sr, N, H):
#         X = librosa.stft(y, n_fft=N, hop_length=H, win_length=N, window='hanning')
#         Y = np.abs(X)
# #         C = librosa.feature.chroma_stft(sr=sr, S=Y, norm=None)
#         C = librosa.feature.chroma_stft(sr=sr, S=Y, n_chroma=12)
#         librosa.display.specshow(C, y_axis='chroma', x_axis='time', sr=sr, hop_length=H,cmap='coolwarm')
    
        chromagram = librosa.feature.chroma_stft(y, sr=sr, hop_length=self.hop_length)
        librosa.display.specshow(chromagram, x_axis='ms', y_axis='chroma', hop_length=self.hop_length, cmap='coolwarm')
    
    def run_Creating_Datasets_ALLINONE(self, song_path, offset=0.0, duration=None, prefix_name=None): 
        start_time = datetime.datetime.now()
        dirname = os.path.dirname(song_path)
        basename = os.path.basename(song_path)
        songname = os.path.splitext(basename)[0]
        if prefix_name is None:
            save_path = '%s/%s_feature_full.png'%(dirname,songname)
        else:
            save_path = '%s/%s_%s_feature_full.png'%(dirname,songname,prefix_name)
        ax_total = 2
        ax_num = 1
        w = 256
        h = 256
        y, sr = librosa.load(song_path,sr=self.sample_rate, offset=offset, duration=duration)
        snd = parselmouth.Sound(song_path)
        total_sec = librosa.get_duration(y=y, sr=sr)
        w_ratio = int(math.ceil(total_sec / self.time_interval))
        w = w * w_ratio
#         print('=== width : {} , total_sec: {}, wanted_sec: {}==='.format(w, total_sec, w_ratio*self.time_interval))
        num_pad = round(w_ratio * self.time_interval * sr - y.shape[0])
        y = np.pad(y, (0, num_pad), 'constant', constant_values=(0, 0))            
        plt.style.use('dark_background')
        fig = plt.figure(0)
        ############
        #### AX ####
        ############
        # 原波形圖
        ax1 = plt.subplot(ax_total,1,ax_num)
        ax1.axis('off')
        ax_num += 1            
# #             self.drawWavefrom(y=y, sr=sr, song_num=song_num)
# #             self.drawSTFT(y=y, sr=sr)
#             intensity = snd.to_intensity(time_step=self.pixel_interval)
# #             spectrogram = snd.to_spectrogram(time_step=pixel_interval)
#             pitch = snd.to_pitch(time_step=self.pixel_interval, pitch_ceiling=600.0)
#             self.draw_intensity(intensity)
#             plt.twinx()
#             self.draw_pitch(pitch, ylim_min=0, ylim_max=1000)
        self.drawMFCC(y=y, sr=sr)

#             ############
#             #### AX ####
#             ############
#             plt.xticks([])
#             plt.yticks([])
#             plt.axis('off')
#             ax2 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
#             ax2.axis('off')
#             ax_num += 1            
# #             self.drawCentroids(y=y, sr=sr, song_num=song_num)
# #             self.drawChroma_cqt(y=y, sr=sr)
            
#             self.draw_pitch(pitch, ylim_min=300, ylim_max=700)

            ############
            #### AX ####
            ############
#             plt.xticks([])
#             plt.yticks([])
#             ax2 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
#             ax2.axis('off')
#             ax_num += 1            
#             self.drawChroma_harmonic_cens(y=y, sr=sr)
            
            ############
            #### AX ####
            ############
#             plt.xticks([])
#             plt.yticks([])
#             ax3 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
#             ax3.axis('off')
#             ax_num += 1  
#             self.drawMFCC(y=y, sr=sr)
            
        ############
        #### AX ####
        ############
        plt.xticks([])
        plt.yticks([])
        ax4 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
        ax4.axis('off')
        ax_num += 1
#             self.drawChroma_smooth(y=y, sr=sr)
        self.drawChroma_cens(y=y, sr=sr)
#             self.drawChroma_stft(y=y, sr=sr, N=2048, H=1024)
#             self.drawChroma_cqt(y=y, sr=sr)
            
        ##################################################################################
        plt.axis('off')
        fig.set_size_inches((w/100.0)/3,(h/100.0)/3) #dpi = 300, output = 256*256 pixels
        plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, hspace = 0, wspace = 0)
        plt.margins(0,0)
        corp_song_name_path = '%s_crop_song.png'%(songname)  
        fig.savefig(corp_song_name_path, format='png', transparent=True, dpi=300, pad_inches = 0)
        inputs = cv2.imread(corp_song_name_path)
        data = inputs        
        
        try:
            os.remove(save_path)
        except OSError as e:
            # print(e)
            pass
        else: 
            pass
            
        try:
            os.remove(corp_song_name_path)
        except OSError as e:
            # print(e)
            pass
        else: 
            pass
                        
        cv2.imwrite(save_path, data)
        plt.close(fig)
        elapsed_time = datetime.datetime.now() - start_time

        if self.verbose:
            print('########################## song_path:%s #############################' % (song_path))
            print(' width: {} , total_sec: {}, wanted_sec: {}'.format(w, total_sec, w_ratio*self.time_interval))
            print(' elapsed_time: %s '%(elapsed_time))
      
    def Creating_Datasets_ALLINONE(self, files_MP3, files_note, start, end, offset=0.0, duration=None, prefix_name=None):   
        song_num = 1
        ax_total = 2
        for song_path,note_path in zip(files_MP3[start:end],files_note[start:end]):
            ax_num = 1
            w = 256
            h = 256
            start_time = datetime.datetime.now()

            dirname = os.path.dirname(song_path)
            basename = os.path.basename(song_path)
            songname = os.path.splitext(basename)[0]
            if prefix_name is None:
                save_path = '%s/%s_feature_full.png'%(dirname,songname)
            else:
                save_path = '%s/%s_%s_feature_full.png'%(dirname,songname,prefix_name)

            y, sr = librosa.load(song_path,sr=self.sample_rate, offset=offset, duration=duration)
            snd = parselmouth.Sound(song_path)
            total_sec = librosa.get_duration(y=y, sr=sr)
            w_ratio = int(math.ceil(total_sec / self.time_interval))
            w = w * w_ratio
            if self.verbose:
                print('=== width : {} , total_sec: {}, wanted_sec: {} ==='.format(w, total_sec, w_ratio*self.time_interval), song_path)
            num_pad = round(w_ratio * self.time_interval * sr - y.shape[0])
            y = np.pad(y, (0, num_pad), 'constant', constant_values=(0, 0))            
            plt.style.use('dark_background')
            fig = plt.figure(0)
            ############
            #### AX ####
            ############
            # 原波形圖
            ax1 = plt.subplot(ax_total,1,ax_num)
            ax1.axis('off')
            ax_num += 1            
# #             self.drawWavefrom(y=y, sr=sr, song_num=song_num)
# #             self.drawSTFT(y=y, sr=sr)
#             intensity = snd.to_intensity(time_step=self.pixel_interval)
# #             spectrogram = snd.to_spectrogram(time_step=pixel_interval)
#             pitch = snd.to_pitch(time_step=self.pixel_interval, pitch_ceiling=600.0)
#             self.draw_intensity(intensity)
#             plt.twinx()
#             self.draw_pitch(pitch, ylim_min=0, ylim_max=1000)
            self.drawMFCC(y=y, sr=sr)

#             ############
#             #### AX ####
#             ############
#             plt.xticks([])
#             plt.yticks([])
#             plt.axis('off')
#             ax2 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
#             ax2.axis('off')
#             ax_num += 1            
# #             self.drawCentroids(y=y, sr=sr, song_num=song_num)
# #             self.drawChroma_cqt(y=y, sr=sr)
            
#             self.draw_pitch(pitch, ylim_min=300, ylim_max=700)

            ############
            #### AX ####
            ############
#             plt.xticks([])
#             plt.yticks([])
#             ax2 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
#             ax2.axis('off')
#             ax_num += 1            
#             self.drawChroma_harmonic_cens(y=y, sr=sr)
            
            ############
            #### AX ####
            ############
#             plt.xticks([])
#             plt.yticks([])
#             ax3 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
#             ax3.axis('off')
#             ax_num += 1  
#             self.drawMFCC(y=y, sr=sr)
            
            ############
            #### AX ####
            ############
            plt.xticks([])
            plt.yticks([])
            ax4 = plt.subplot(ax_total,1,ax_num, sharex=ax1)
            ax4.axis('off')
            ax_num += 1
#             self.drawChroma_smooth(y=y, sr=sr)
            self.drawChroma_cens(y=y, sr=sr)
#             self.drawChroma_stft(y=y, sr=sr, N=2048, H=1024)
#             self.drawChroma_cqt(y=y, sr=sr)
            
            ##################################################################################
            plt.axis('off')
            fig.set_size_inches((w/100.0)/3,(h/100.0)/3) #dpi = 300, output = 256*256 pixels
            plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, hspace = 0, wspace = 0)
            plt.margins(0,0)
            corp_song_name_path = '%s_crop_song.png'%(songname) 
            fig.savefig(corp_song_name_path, format='png', transparent=True, dpi=300, pad_inches = 0)
            inputs = cv2.imread(corp_song_name_path)
            data = inputs
            # dirname = os.path.dirname(song_path)
            # basename = os.path.basename(song_path)
            # songname = os.path.splitext(basename)[0]
            
            # if prefix_name is None:
            #     save_path = '%s/%s_feature_full.png'%(dirname,songname)
            # else:
            #     save_path = '%s/%s_%s_feature_full.png'%(dirname,songname,prefix_name)
            
            # try:
            #     os.remove(save_path)
            # except OSError as e:
            #     if self.verbose:
            #         print(e)
            # else: 
            #     if self.verbose:
            #         print("The %s is deleted successfully" % (save_path))
            #     pass

            try:
                os.remove(save_path)
            except OSError as e:
                # print(e)
                pass
            else: 
                pass
                
            try:
                os.remove(corp_song_name_path)
            except OSError as e:
                # print(e)
                pass
            else: 
                pass

            cv2.imwrite(save_path, data)
            plt.close(fig)
            song_num += 1
            elapsed_time = datetime.datetime.now() - start_time
            if self.verbose:
                print('########################## total time:%s #############################' % (elapsed_time))
    
    def song2img4all(self, csvFile):
        data = pd.read_csv(csvFile)
        count = 1
        files_MP3 = []
        files_note = []
        for _path, _groundtruth, _songs in zip(data['paths'],data['groundtruth'],data['songs']):
            mp3_path = os.path.join(_path, _songs)
            groundtruth_path = os.path.join(_path, _groundtruth)
            basename = os.path.basename(groundtruth_path)
            imgname = '%s.png'%(os.path.splitext(basename)[0])
            groundtruth_img_path = os.path.join(_path, imgname)
            if self.isTrain == True and path.exists(mp3_path) == True and path.exists(groundtruth_img_path) == True:  
                files_MP3.append(mp3_path)
                files_note.append(groundtruth_img_path)              
            elif self.isTrain == False and path.exists(mp3_path) == True:
                files_MP3.append(mp3_path)
                files_note.append(mp3_path) 
        self.Creating_Datasets_ALLINONE(files_MP3, files_note, 0, len(files_MP3))
        
    def song2img4one(self, mp3_path, offset=0.0, duration=None, prefix_name=None):
        self.Creating_Datasets_ALLINONE([mp3_path], [mp3_path], 0, 1, offset=offset, duration=duration, prefix_name=prefix_name)

    def api_song2img4one(self, mp3_path, offset=0.0, duration=None, prefix_name=None):
        self.Creating_Datasets_ALLINONE([mp3_path], [mp3_path], 0, 1, offset=offset, duration=duration, prefix_name=prefix_name)
                
    def run(self, command):
        if self.verbose:
            print(command)
        exit_status = os.system(command)
        if exit_status > 0:
            exit(1)

    def api_predict(self, dataroot,epoch='latest', CLASS='singing_transcription', MODEL='pix2pix'):
        startTime = time.time()
        ##############################################
        NET_SIZE = 64 #128
        NORM='batch'
        EPOCH = epoch
        ##############################################
        NEF='%d'%(NET_SIZE)
        NGF='%d'%(NET_SIZE)
        NDF='%d'%(NET_SIZE)
        G_MODEL_NAME = '%s_net_G.pth'%(EPOCH)
        D_MODEL_NAME = '%s_net_D.pth'%(EPOCH)
        ##############################################
        CMD='./model/predict_pitch.py' 
       
        DIRECTION='AtoB' # from domain A to domain B
        LOAD_SIZE=256 # scale images to this size
        CROP_SIZE=256 # then crop to this size
        INPUT_NC=3  # number of channels in the input image
        NO_FLIP='--no_flip'
        DATAROOT=dataroot
        CHECKPOINTS_DIR='./model/pretrained_models/%s/'%(CLASS)
        # PHASE='--phase predict'
        PHASE='--phase predict_test'
        CPU='--gpu_ids -1'
        GPU_ID=0   # gpu id
        NUM_TEST=1000 # number of input images duirng test
        # number of samples per input images
        NUM_SAMPLES='--n_samples 1' 
        # USE_DROPOUT='--use_dropout'
        NAME='%s_%s'%(CLASS,MODEL)
        # RESULTS_DIR='./results/%s/'%(NAME)
        RESULTS_DIR='%s/results/'%(dataroot)
        NET_G='--netG unet_256'
        DATASET_MODE='--dataset_mode single'#'aligned'
        ##############################################
        os.environ['CMD']=str(CMD)
        os.environ['MODEL']=str(MODEL)
        os.environ['CLASS']=str(CLASS)
        os.environ['DIRECTION']=str(DIRECTION)
        os.environ['LOAD_SIZE']=str(LOAD_SIZE)
        os.environ['CROP_SIZE']=str(CROP_SIZE)
        os.environ['INPUT_NC']=str(INPUT_NC)
        os.environ['NO_FLIP']=str(NO_FLIP)
        os.environ['DATAROOT']=str(DATAROOT)
        os.environ['CHECKPOINTS_DIR']=str(CHECKPOINTS_DIR)
        os.environ['PHASE']=str(PHASE)
        os.environ['CPU']=str(CPU)
        os.environ['GPU_ID']=str(GPU_ID)
        os.environ['NUM_TEST']=str(NUM_TEST)
        os.environ['NUM_SAMPLES']=str(NUM_SAMPLES)
        os.environ['NAME']=str(NAME)
        os.environ['RESULTS_DIR']=str(RESULTS_DIR)
        os.environ['NET_G']=str(NET_G)
        # os.environ['USE_DROPOUT']=str(USE_DROPOUT)
        os.environ['DATASET_MODE']=str(DATASET_MODE)
        os.environ['G_MODEL_NAME']=str(G_MODEL_NAME)
        os.environ['D_MODEL_NAME']=str(D_MODEL_NAME)
        os.environ['EPOCH']=str(EPOCH)

        os.environ['NEF']=str(NEF)
        os.environ['NGF']=str(NGF)
        os.environ['NDF']=str(NDF)
        ##############################################
        self.run(' CUDA_VISIBLE_DEVICES=$GPU_ID python $CMD \
          --dataroot $DATAROOT \
          --results_dir $RESULTS_DIR \
          --checkpoints_dir $CHECKPOINTS_DIR \
          --name $NAME \
          --model $MODEL \
          --serial_batches \
          $PHASE \
          $DATASET_MODE  \
          --direction $DIRECTION \
          --load_size $LOAD_SIZE \
          --crop_size $CROP_SIZE \
          --input_nc $INPUT_NC \
          --num_test $NUM_TEST \
          $NUM_SAMPLES \
          --center_crop \
          --no_encode \
          $NO_FLIP \
          $USE_DROPOUT \
          $NET_G \
          $CPU')
        print('[Prediction] Done ... %fs'%(time.time() - startTime))

    def predict(self, epoch='latest', song_id=1, dataroot='./datasets/original/AIcup_testset_ok/'):
        startTime = time.time()
        ##############################################
        NET_SIZE = 64 #128
        NORM='batch'
        # NORM='instance'
        # EPOCH = 1680 # Not Bad

        EPOCH = epoch
        ##############################################
        NEF='%d'%(NET_SIZE)
        NGF='%d'%(NET_SIZE)
        NDF='%d'%(NET_SIZE)
        G_MODEL_NAME = '%s_net_G.pth'%(EPOCH)
        D_MODEL_NAME = '%s_net_D.pth'%(EPOCH)
        ##############################################
        CMD='./predict_pitch.py' 
        MODEL='pix2pix'
        CLASS='singing_transcription'
        DIRECTION='AtoB' # from domain A to domain B
        LOAD_SIZE=256 # scale images to this size
        CROP_SIZE=256 # then crop to this size
        INPUT_NC=3  # number of channels in the input image
        NO_FLIP='--no_flip'
        DATAROOT='%s/%d'%(dataroot,song_id)
        CHECKPOINTS_DIR='./pretrained_models/%s/'%(CLASS)
        # PHASE='--phase predict'
        PHASE='--phase predict_test'
        CPU='--gpu_ids -1'
        GPU_ID=0   # gpu id
        NUM_TEST=1000 # number of input images duirng test
        # number of samples per input images
        NUM_SAMPLES='--n_samples 1' 
        # USE_DROPOUT='--use_dropout'
        NAME='%s_%s'%(CLASS,MODEL)
        # RESULTS_DIR='./results/%s/'%(NAME)
        RESULTS_DIR='./results/'
        NET_G='--netG unet_256'
        DATASET_MODE='--dataset_mode single'#'aligned'
        ##############################################
        os.environ['CMD']=str(CMD)
        os.environ['MODEL']=str(MODEL)
        os.environ['CLASS']=str(CLASS)
        os.environ['DIRECTION']=str(DIRECTION)
        os.environ['LOAD_SIZE']=str(LOAD_SIZE)
        os.environ['CROP_SIZE']=str(CROP_SIZE)
        os.environ['INPUT_NC']=str(INPUT_NC)
        os.environ['NO_FLIP']=str(NO_FLIP)
        os.environ['DATAROOT']=str(DATAROOT)
        os.environ['CHECKPOINTS_DIR']=str(CHECKPOINTS_DIR)
        os.environ['PHASE']=str(PHASE)
        os.environ['CPU']=str(CPU)
        os.environ['GPU_ID']=str(GPU_ID)
        os.environ['NUM_TEST']=str(NUM_TEST)
        os.environ['NUM_SAMPLES']=str(NUM_SAMPLES)
        os.environ['NAME']=str(NAME)
        os.environ['RESULTS_DIR']=str(RESULTS_DIR)
        os.environ['NET_G']=str(NET_G)
        # os.environ['USE_DROPOUT']=str(USE_DROPOUT)
        os.environ['DATASET_MODE']=str(DATASET_MODE)
        os.environ['G_MODEL_NAME']=str(G_MODEL_NAME)
        os.environ['D_MODEL_NAME']=str(D_MODEL_NAME)
        os.environ['EPOCH']=str(EPOCH)

        os.environ['NEF']=str(NEF)
        os.environ['NGF']=str(NGF)
        os.environ['NDF']=str(NDF)
        ##############################################
#         self.original_dir = 'original'
#         self.MIR_ST500 = 'MIR-ST500'
#         self.AIcup_testset_ok = 'AIcup_testset_ok'
#         self.train_dir = 'train'
#         self.val_dir = 'val'
#         self.test_dir = 'test'
#         self.features_dir = 'features'
#         self.source_data_dir_path = './datasets/%s/%s'%(self.original_dir,self.MIR_ST500) 
#         self.source_testdata_dir_path = './datasets/%s/%s'%(self.original_dir,self.AIcup_testset_ok)       
#         self.feature_file_path = '/'.join((output_root, self.features_dir)) 
#         self.train_file_path = '/'.join((output_root, self.train_dir))
#         self.val_file_path = '/'.join((output_root, self.val_dir))
#         self.test_file_path = '/'.join((output_root, self.test_dir))
#         ##############################################
#         self.model_source_path = '../../BicycleGAN/checkpoints/%s/%s_%s/'%(CLASS,CLASS,MODEL)
#         self.pretrained_models_path = './pretrained_models/%s/%s/'
        ##############################################
        if NET_SIZE == 64 and NORM == 'batch':
            self.run('cp -rf ./checkpoints/singing_transcription/singing_transcription_pix2pix/$G_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')
            self.run('cp -rf ./checkpoints/singing_transcription/singing_transcription_pix2pix/$D_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')            
        elif NET_SIZE == 64 and NORM == 'instance':
            self.run('cp -rf ./checkpoints/singing_transcription_64_instance/singing_transcription_64_instance_pix2pix/$G_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')
            self.run('cp -rf ./checkpoints/singing_transcription_64_instance/singing_transcription_64_instance_pix2pix/$D_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')
        else: #'batch' 128
            self.run('cp -rf ./checkpoints/singing_transcription_128/singing_transcription_128_pix2pix/$G_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/')
            self.run('cp -rf ./checkpoints/singing_transcription_128/singing_transcription_128_pix2pix/$D_MODEL_NAME ./pretrained_models/singing_transcription/singing_transcription_pix2pix/') 
        ##############################################
        self.run(' CUDA_VISIBLE_DEVICES=$GPU_ID python $CMD \
          --dataroot $DATAROOT \
          --results_dir $RESULTS_DIR \
          --checkpoints_dir $CHECKPOINTS_DIR \
          --name $NAME \
          --model $MODEL \
          --serial_batches \
          $PHASE \
          $DATASET_MODE  \
          --direction $DIRECTION \
          --load_size $LOAD_SIZE \
          --crop_size $CROP_SIZE \
          --input_nc $INPUT_NC \
          --num_test $NUM_TEST \
          $NUM_SAMPLES \
          --center_crop \
          --no_encode \
          $NO_FLIP \
          $USE_DROPOUT \
          $NET_G \
          $CPU')
        print('[Prediction] Done ... %fs'%(time.time() - startTime))
    
    def genCSV_theorytab_symble_structure(self,csv):
        df_theorytab = pd.read_csv(csv)
        symbol_list = []
        structure_list = []
        with progressbar.ProgressBar(max_value=df_theorytab.shape[0]) as bar:
            i = 0
            for index, row in df_theorytab.iterrows():
#                 print(row["song_path"])
                for name in self.song_structure:             
                    if os.path.exists(row[name]):
#                         print(row[name])
                        with open(row[name] , 'r') as reader:
                            jf = json.loads(reader.read())
                            for chord in jf['tracks']['chord']:
                                if chord:
                                    symbol_list.append(chord['symbol'])
                                    structure_list.append(name)
                bar.update(i)
                i += 1

        print(len(symbol_list),len(structure_list))
        dat = {'symbol':symbol_list, 'structure':structure_list}
        df = pd.DataFrame(dat) 
        print(df.describe())
#         print(df['symbol'].value_counts())
#         print(df['structure'].value_counts())
        
        # 賦予**[音樂結構]**一個代碼，範圍 0~0xFF
        frequency_df = df['structure'].value_counts()
        structure_list = frequency_df.index.tolist()
        structure_code = 0xFFF
        code_list = []
        structure_dict = {}
        for i in structure_list:    
            code_list.append(structure_code)
            structure_code -= 409

        chord_dict = {'structure':structure_list, 'code':code_list}
        data = pd.DataFrame(chord_dict)
        data.to_csv(os.path.join('./', 'theorytab_structure.csv'), index=False)  
        
        
        # 賦予**[和弦]**一個代碼，範圍 0~0xFFF
        frequency_df = df['symbol'].value_counts()
        symbol_list = frequency_df.index.tolist()
        code_list = []
        chord_code = 0xFFF
        for i in symbol_list:    
            code_list.append(chord_code)
            chord_code -= 6

        chord_dict = {'chord':symbol_list, 'code':code_list}
        data = pd.DataFrame(chord_dict)
        data.to_csv(os.path.join('./', 'theorytab_symble.csv'), index=False)  

    
    def genCSV_theorytab(self, datasetPath='../../lead-sheet-dataset/datasets/', csv='theorytab.csv'):
        datasetsPath = Path(datasetPath)
        links_path = datasetsPath.glob('**/*.mp3')
        for link in links_path:   
            self.song_info_dict['song_path'].append(link)
            
        with progressbar.ProgressBar(max_value=len(self.song_info_dict['song_path'])) as bar:
            i = 0
            for song_path in self.song_info_dict['song_path']:
                ground_truth_dirname = os.path.dirname(song_path).replace('/song/','/event/')    
                for name in self.song_structure:
                    ground_truth_path = ground_truth_dirname+'/%s.json'%(name)
                    if os.path.exists(ground_truth_path):
                        self.song_info_dict[name].append(ground_truth_path)
                    else:
                        self.song_info_dict[name].append('None')
                bar.update(i)
                i += 1

        # gen csv    
        data = pd.DataFrame(self.song_info_dict)
        data.to_csv(os.path.join('./', csv), index=False)  
        
        
    def genTrain_AB_CSV4TheoryTab(self, datasetPath='../../lead-sheet-dataset/datasets/', csv='theorytab_AB'):    
        feature_full_dict = {}
        feature_full_dict['feature_full'] = []

        melody_data_dict = {}    
        melody_data_dict['feature_full'] = []
        melody_data_dict['groundtruth'] = []

        chord_data_dict = {}
        chord_data_dict['feature_full'] = []
        chord_data_dict['groundtruth'] = []

        datasetsPath = Path(datasetPath)
        links_path = datasetsPath.glob('**/*_feature_full.png')
        for link in links_path:  
            feature_full_dict['feature_full'].append(link)

        with progressbar.ProgressBar(max_value=len(feature_full_dict['feature_full'])) as bar:
            i = 0
            for feature_full_path in feature_full_dict['feature_full']:
                groundtruth_melody_path = str(feature_full_path).replace('feature_full.png','groundtruth_melody.png')  
                if os.path.exists(groundtruth_melody_path): 
                    melody_data_dict['feature_full'].append(str(feature_full_path))
                    melody_data_dict['groundtruth'].append(groundtruth_melody_path)

                groundtruth_chord_path = str(feature_full_path).replace('feature_full.png','groundtruth_chord.png')  
                if os.path.exists(groundtruth_chord_path):  
                    chord_data_dict['feature_full'].append(str(feature_full_path))
                    chord_data_dict['groundtruth'].append(groundtruth_chord_path)

                bar.update(i)
                i += 1

        # gen melody & chord train csv        
        melody_data = pd.DataFrame(melody_data_dict)
        melody_data.to_csv('./%s_melody.csv'%(csv), index=False)  

        chord_data = pd.DataFrame(chord_data_dict)
        chord_data.to_csv('./%s_chord.csv'%(csv), index=False) 
        
    def song2img_theorytab(self, csv, startIndex=-1, endIndex=-1):
        df_theorytab = pd.read_csv(csv)
        FAIL_LIST = []
        for index, row in df_theorytab.iterrows():                
            if index < startIndex:
                if self.verbose:
                    print('[SKIP1_%d] %s'%(index,row['song_path']))                
                continue
            
            if endIndex>0 and index>endIndex:
                if self.verbose:
                    print('[SKIP2_%d] %s'%(index,row['song_path']))  
                break                
                                
            for name in self.song_structure: 
                if os.path.exists(row[name]):
                    with open(row[name] , 'r') as reader:
                        jf = json.loads(reader.read())
                        try:
                            global_start = float(jf["metadata"]['global_start'])
                            active_start = float(jf["metadata"]['active_start'])
                            active_stop = float(jf["metadata"]['active_stop'])
                            duration = 0.0
                            if jf["metadata"]['duration'] == 'nan' or jf["metadata"]['duration'] == 'NaN' or jf["metadata"]['duration'] == 0 or (active_stop-active_start) <= 0:
                                continue
                            duration = float(jf["metadata"]['duration'])
        #                   print(row['song_path'],active_start, active_stop,name)
                        #------------------------------------------------------------------
                            # duration = active_stop-active_start
                            if duration != 0 and (active_stop-active_start) > 0:
                                print('[%d] [%s] %s [global_start:%.3f active_start:%.3f active_stop:%.3f duration:%.3f]'%(index,name,row['song_path'],global_start,active_start,active_stop,duration))
                                self.song2img4one(mp3_path=row['song_path'], offset=active_start+global_start, duration=duration, prefix_name=name) 
                            else:
                                fail_string = '[FAIL_%d] [%s] %s [global_start:%.3f active_start:%.3f active_stop:%.3f]'%(index,name,row['song_path'],global_start,active_start,active_stop)
                                print(fail_string) 
                                FAIL_LIST.append(fail_string)       
                        except OSError as e:
                            print(e)
                            FAIL_LIST.append(e)  
                        else:
                            continue
        for i in FAIL_LIST:
            print(i)

#                 bar.update(i)
#                 i += 1

    def melody_note2img(self, jf, savePath, name, toMIDI=True):
        dirname_groundtruth = os.path.dirname(savePath)
        basename_groundtruth = os.path.basename(savePath)
        filename_groundtruth = '%s_%s_groundtruth_melody'%(os.path.splitext(basename_groundtruth)[0],name)
        ############################################
        try:            
            if os.path.exists('%s/%s.png'%(dirname_groundtruth,filename_groundtruth)):
                os.remove('%s/%s.png'%(dirname_groundtruth,filename_groundtruth))
        except OSError as e:
            if self.verbose:
                print(e)
        else: 
            if self.verbose:
                print("The %s.png is deleted successfully" % (filename_groundtruth))
            pass
        ############################################
        BPM = float(jf["metadata"]['BPM'])
        key = jf["metadata"]['key']
        duration = float(jf["metadata"]['duration'])
        global_start = float(jf["metadata"]['global_start'])
        active_start = float(jf["metadata"]['active_start'])
        active_stop = float(jf["metadata"]['active_stop'])
        beat_time = 60/float(BPM)
        ################### melody ##################
        zero_pitch = 0
        time_index = 0
        melody_index = 0  
        melody_notes = []                 
        while melody_index < len(jf['tracks']['melody']):    
            if jf['tracks']['melody'][melody_index]:
                g_pitch_start_time = float(jf['tracks']['melody'][melody_index]['event_on'])*beat_time
                g_pitch_end_time = float(jf['tracks']['melody'][melody_index]['event_off'])*beat_time
                g_pitch = jf['tracks']['melody'][melody_index]['pitch']+60.0
                if self.verbose:
                    print(g_pitch,g_pitch_start_time,g_pitch_end_time,round(time_index, 3))                
                if time_index < g_pitch_end_time:
                    if time_index >= g_pitch_start_time:
                        melody_notes.append(g_pitch)
                    else:
                        melody_notes.append(zero_pitch)
                    time_index += self.pixel_interval
                else:
                    melody_index += 1                            
            else:
                melody_index += 1 
                
        if len(melody_notes) != 0:
            melody_note_image = np.zeros((256,len(melody_notes),3), np.uint8)
            x = 0
            for ii in melody_notes:
                c = int(ii*2) # Gray image rule is pitch*2
                cv2.line(melody_note_image, (x, 0), (x, 255), (c,c,c), 1)
                x += 1

            cv2.imwrite('%s/%s.png'%(dirname_groundtruth,filename_groundtruth), melody_note_image)   
            ################### melody To midi ##################
            if toMIDI:
                ############################################
                outmidi_file = '%s/%s.mid'%(dirname_groundtruth,filename_groundtruth)
                ############################################
                try:
                    if os.path.exists("%s"%(outmidi_file)):
                        os.remove("%s"%(outmidi_file))
                except OSError as e:
                    if self.verbose:
                        print(e)
                else: 
                    if self.verbose:
                        print("The %s is deleted successfully" % (outmidi_file))
                    pass
                ############################################
                # create your MIDI object
                mf = MIDIFile(1)     # only 1 track
                channel = 0
                time = 0
                track = 0   # the only track
                mf.addTrackName(track, time, "Sample Track")
                mf.addTempo(track, 0, BPM)
                # add some notes
                new_note = []
                for _note in jf['tracks']['melody']:
#                     print(_note)
                    if _note:
                        onset = _note['event_on']
                        offset = _note['event_off']
                        duration = _note['event_duration']
                        pitch = _note['pitch']+60.0
                        mf.addNote(track, channel, int(pitch), onset, duration, 100)         
                
                with open(outmidi_file, 'wb') as outf:
                    mf.writeFile(outf)
                
    def chord_note2img(self, jf, savePath,name):
        ################### chord ##################
        dirname_groundtruth = os.path.dirname(savePath)
        basename_groundtruth = os.path.basename(savePath)
        filename_groundtruth = '%s_%s_groundtruth_chord'%(os.path.splitext(basename_groundtruth)[0],name)                               
        ############################################
        try:
            if os.path.exists('%s/%s.png'%(dirname_groundtruth,filename_groundtruth)):
                os.remove('%s/%s.png'%(dirname_groundtruth,filename_groundtruth))
        except OSError as e:
            if self.verbose:
                print(e)
        else: 
            if self.verbose:
                print("The %s.png is deleted successfully" % (filename_groundtruth))
            pass
        ############################################
        BPM = float(jf["metadata"]['BPM'])
        key = jf["metadata"]['key']
        duration = float(jf["metadata"]['duration'])
        global_start = float(jf["metadata"]['global_start'])
        active_start = float(jf["metadata"]['active_start'])
        active_stop = float(jf["metadata"]['active_stop'])
        beat_time = 60/float(BPM)

        zero_symbol = 'none'
        time_index = 0
        chord_index = 0  
        chord_notes = []                                 
        while chord_index < len(jf['tracks']['chord']):    
            if jf['tracks']['chord'][chord_index]:
                g_symbol_start_time = float(jf['tracks']['chord'][chord_index]['event_on'])*beat_time
                g_symbol_end_time = float(jf['tracks']['chord'][chord_index]['event_off'])*beat_time
                g_symbol_duration = float(jf['tracks']['chord'][chord_index]['event_duration'])*beat_time
                g_symbol = jf['tracks']['chord'][chord_index]['symbol']
                if self.verbose:
                    print(g_symbol,g_symbol_start_time,g_symbol_end_time,round(time_index, 3))                
                if time_index < g_symbol_end_time:
                    if time_index >= g_symbol_start_time:
                        chord_notes.append(g_symbol)
                    else:
                        chord_notes.append(zero_symbol)
                    time_index += self.pixel_interval
                else:
                    chord_index += 1                            
            else:
                chord_index += 1 
        if len(chord_notes) != 0:
            chord_note_image = np.zeros((256,len(chord_notes),3), np.uint8)
            x = 0
            for ii in chord_notes:
                if ii != 'none':
                    symble_code = self.theorytab_symble_dict[ii]
                    structure_code = self.theorytab_structure_dict[name]
                    symble_bit_string = BitArray(hex=hex(symble_code))                                            
                    structure_bit_string = BitArray(hex=hex(structure_code))                            
    #                 print(symble_bit_string,structure_bit_string)
                    r = int(symble_bit_string.bin[0:4]+structure_bit_string.bin[0:4], 2)
                    g = int(symble_bit_string.bin[4:8]+structure_bit_string.bin[4:8], 2)
                    b = int(symble_bit_string.bin[8:12]+structure_bit_string.bin[8:12], 2)
    #                 print(r,g,b)
                    cv2.line(chord_note_image, (x, 0), (x, 255), (r,g,b), 1)
                x += 1
            cv2.imwrite('%s/%s.png'%(dirname_groundtruth,filename_groundtruth), chord_note_image) 
                    
    def note2img_theorytab(self, csv, startIndex=-1, endIndex=-1):
        df_theorytab = pd.read_csv(csv)
        for index, row in df_theorytab.iterrows():
            if index < startIndex:
                if self.verbose:
                    print('[SKIP1_%d] %s'%(index,row['song_path']))                
                continue

            if endIndex>0 and index>endIndex:
                if self.verbose:
                    print('[SKIP2_%d] %s'%(index,row['song_path']))  
                break 
            if os.path.exists(row['song_path']):            
                for name in self.song_structure:             
                    if os.path.exists(row[name]):
                        with open(row[name] , 'r') as reader:
                            jf = json.loads(reader.read())
        #                     print(json.dumps(jf["metadata"]))      
                            print('[%d] %s %s'%(index,name,row['song_path']))
                            try:
                                self.melody_note2img(jf, savePath=row["song_path"], name=name)
                            except OSError as e:
                                print('[FAIL] [melody_note2img] [%d] %s %s'%(index,name,row['song_path']))
                            else: 
                                pass

                            try:
                                self.chord_note2img(jf, savePath=row["song_path"],name=name)
                            except OSError as e:
                                print('[FAIL] [chord_note2img] [%d] %s %s'%(index,name,row['song_path']))
                            else: 
                                pass  
    def run_note2img_theorytab(self, song_path, json_path, name, index):
        with open(json_path , 'r') as reader:
            jf = json.loads(reader.read())
            try:
                self.melody_note2img(jf, savePath=song_path, name=name)
            except OSError as e:
                print('[FAIL] [melody_note2img] [%d] [%s] %s -> %s'%(index,name,song_path,json_path))
            else: 
                pass

            try:
                self.chord_note2img(jf, savePath=song_path,name=name)
            except OSError as e:
                print('[FAIL] [chord_note2img] [%d] [%s] %s -> %s'%(index,name,song_path,json_path))
            else: 
                pass                             