# from flask import Flask, request, redirect, url_for, render_template, Response, flash,session
import os
# import json
import glob
import time
# from uuid import uuid4
from model.utils import Utils
from model.box import Box
import copy
# import random
# import threading
from pydub import AudioSegment
import youtube_dl

class API:        
    def download_video(self, title, video_url):
        ydl_opts = {
            'outtmpl': '{}.%(ext)s'.format(title),
            'format': 'bestaudio/best',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
            }],
        }

        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            try:
                ydl.download([video_url])
            except ValueError:
                print("Oops!  That was no valid number.  Try again...")

        return {
            'audio': open('{}.mp3'.format(title), 'rb'),
            'title': title,
        } 

    def __init__(self):
        self.utils = Utils()
        self.utils.init_vars(verbose=False,shift=False)
        self.utils.init_datasets(output_root = './datasets')

    def song2img4one(self, infodict):
        infodict['progress'] = 0

        # if infodict['youtube'] != "":
        #     print(infodict['youtube'])
        #     self.download_video(os.path.join(infodict['target'], infodict['uuid']),infodict['youtube'])
        #     infodict['files'] = []
        #     for _ in glob.glob("{}/*.mp3".format(infodict['target'])):
        #         print(_)
        #         infodict['files'].append(_)
            
        for file in infodict['files']:
            dirname = os.path.dirname(file)
            basename = os.path.basename(file)

            if os.path.splitext(basename)[1] == '.mp4' or os.path.splitext(basename)[1] == '.m4a':
                infodict['message'] = 'Convert song format to MP3 ... '
                mp3_filename = '%s/%s.mp3'%(dirname, os.path.splitext(basename)[0]) 
                print('[song2img4one] mp3_filename ---> ',mp3_filename)
                AudioSegment.from_file(file).export(mp3_filename, format='mp3')
                infodict['message'] = 'Convert song format to MP3 ... success'
                infodict['code'] = 1000
                file = mp3_filename
            print('[song2img4one]',infodict['message'],file)
            infodict['message'] = 'Extract Song Features ... '
            self.utils.api_song2img4one(mp3_path=file)
        
            imgname = '%s/%s_feature_full.png'%(dirname, os.path.splitext(basename)[0])        
            self.utils.api_cut_features_image([imgname])
            infodict['message'] = 'Extract Song Features ... success'
            infodict['code'] = 1001
            print('[song2img4one]',infodict['message'])
        return True

    def predict(self, infodict):
        infodict['progress'] = 20
        for file in infodict['files']:
            dirname = os.path.dirname(file)
            basename = os.path.basename(file)
            # print(dirname, basename)
            infodict['message'] = 'Prediction Note ... '
            try:         
                if infodict['ai_model'] == 'vocal':
                    CLASS='singing_transcription'
                elif infodict['ai_model'] == 'chord':
                    CLASS='chord_transcription'
                elif infodict['ai_model'] == 'melody':
                    CLASS='melody_transcription'
                else:
                    CLASS='singing_transcription'  
                self.utils.api_predict(epoch=infodict['epoch'], dataroot=dirname,CLASS=CLASS,MODEL='pix2pix')
            except:
                self.utils.api_predict(dataroot=dirname)
            
            infodict['message'] = 'Prediction Note ... success'
            infodict['code'] = 1002
        return True

    def result2note(self, infodict):
        infodict['progress'] = 40
        for file in infodict['files']:        
            dirname = os.path.dirname(file)
            basename = os.path.basename(file)
            # print(dirname, basename)
    
            infodict['message'] = 'Note to MIDI ... '
            # utils.verbose = True
            # shift_ms = 150 # For TapTap needed
            shift_ms = 100 # For TapTap needed
            # shift_ms = -150 # For iPhone needed

            raw_note_list, midi_note_list = self.utils.api_result2note(dataroot=dirname, basename=os.path.splitext(basename)[0], uuid=infodict['uuid'], shift_ms=shift_ms)

            infodict['%s_midi_note_list'%(os.path.splitext(basename)[0])] = copy.deepcopy(midi_note_list)
            infodict['message'] = 'Note to MIDI ... success'
            infodict['code'] = 1003
        return True

    def note2light(self, infodict):
        infodict['progress'] = 60
        for file in infodict['files']:        
            dirname = os.path.dirname(file)
            basename = os.path.basename(file)
            # print(dirname, basename)
        
            infodict['message'] = 'MIDI To Light Show ... '
            try:            
                light_modal = infodict['light_modal']
                total_channels = int(infodict['total_channels'])
                pitch_time_25 = float(infodict['low_pitch_length'])
                pitch_time_50 = float(infodict['middle_pitch_length'])
                pitch_time_75 = float(infodict['long_pitch_length'])
                new_light_list = self.utils.api_note2light(
                    dataroot=dirname, basename=os.path.splitext(basename)[0],
                    note_list=infodict['%s_midi_note_list'%(os.path.splitext(basename)[0])], 
                    pattern_mode=light_modal,
                    total_channels=total_channels,
                    pitch_time_25 = pitch_time_25,
                    pitch_time_50 = pitch_time_50,
                    pitch_time_75 = pitch_time_75,
                )
            except:
                new_light_list = self.utils.api_note2light(
                    dataroot=dirname, basename=os.path.splitext(basename)[0],
                    note_list=infodict['%s_midi_note_list'%(os.path.splitext(basename)[0])])
                   
            infodict['message'] = 'MIDI To Light Show ... success'
            infodict['code'] = 1004
        return True

    def sendProjectToCloud(self, infodict):
        infodict['progress'] = 80
        for file in infodict['files']:        
            dirname = os.path.dirname(file)
            basename = os.path.basename(file)
            filename = os.path.splitext(basename)[0]
            print('[sendProjectToCloud]',dirname, basename)
            showPath = '%s/%s.npz'%(dirname,filename)
            ProjectName = filename
            infodict['message'] = 'Upload Light Show(%s) to cloud ...'%(ProjectName)
            box = Box(account=infodict['account'] , pwd=infodict['pwd'], dataroot=dirname)
            if os.path.splitext(basename)[1] == '.mp4' or os.path.splitext(basename)[1] == '.m4a':
                file = '%s/%s.mp3'%(dirname, os.path.splitext(basename)[0]) 
            code,msg = box.sendProjectToCloud(ProjectName=ProjectName, songPath=file, showPath=showPath)
            infodict['message'] = msg
            infodict['code'] = code
        infodict['progress'] = 100                
        return True


