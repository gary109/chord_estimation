#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sys
import argparse

from uploadr.app import app
# from uploadr.app import socketio


parser = argparse.ArgumentParser(description="Uploadr")
parser.add_argument(
    "--port", "-p",
    type=int,
    help="Port to listen on",
    default=12345,
)
args = parser.parse_args()

if __name__ == '__main__':
    context = ('./certs/server.crt','./certs/server.key')
    flask_options = dict(
        host='0.0.0.0',
        debug=True,
        port=args.port,        
        # threaded=False, 
        # processes=10,
        # threaded=True,
        # ssl_context='adhoc',
        # ssl_context=context,
    )
    # socketio.run(app,**flask_options)
    app.run(**flask_options)

    
    

